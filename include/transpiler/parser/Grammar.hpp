#include "abnf_full.h"
/*
 *
<number>	     
<characters>
<string> 
<letter>          ::= A|B|C ... |Z|a|b|c ... |z
<break character> ::= _ | @ | # | $
<replace>                  ::= <-
<relation>                 ::= ==
                             | <
                             | >
                             | !=
                             | < =
                 		     | > =
<type>                  ::= FIXED
                          | CHARACTER
                          | LABEL
						  
<id character>    ::= <letter> | <break character>
<identifier>      ::= <id character>
                    | <identifier> <id character>
                    | <identifier> <decimal digit>

<constant>        ::= <string>
                    | <number>
					
<logical factor>           ::= <logical secondary>
                             | <logical factor> & <logical secondary>

<logical secondary>        ::= <logical primary>
                             | ¬ <logical primary>

<logical primary>          ::= <string expression>
                             | <string expression> <relation> <string expression>


<string expression>        ::= <arithmetic expression>
			     | <string expression> ||  <arithmetic expression>

<arithmetic expression>    ::= <term>
                             | <arithmetic expression> + <term>
                             | <arithmetic expression> - <term>
                             | + <term>
                             | - <term>

<term>                     ::= <primary>
                             | <term> * <primary>
                             | <term> / <primary>
                             | <term> MOD <primary>
							 
<primary>         ::= <constant>
                    | <variable>
                    | ( <expression> )		
					
<expression>               ::= <logical factor>
                             | <expression> | <logical factor>
							 

<variable>        ::= <identifier>
                    					
<left part>                ::= <variable> ,	

<assignment>               ::= <variable> <replace> <expression>
						 | <left part> <assignment>			

<if statement>       	::= <if clause> <statement> 
			  | <if clause> <true part> <statement>

<if clause>		::= IF <expression> THEN 

<true part>	        ::= <basic statement> ELSE

<statement>  		::= <basic statement>
			  | <if statement>
			  
<statement list>	::= <statement>
			  | <statement list> <statement>	
			  
<while clause>	        ::= WHILE <expression>
<case selector>         ::= CASE <expression>

<basic statement>  	::= <assignment> ;
| <group> ;
| <procedure definition> ;
| <return statement> ;
| <call statement> ;
| <declaration statement> ;
		  | ;
| <label definition> <basic statement>

<group>                 ::= <group head> <ending>

<group head>            ::= DO ;
                          | DO <step definition> ;
                          | DO <while clause> ;
                          | DO <case selector> ;
                          | <group head> <statement>

<step definition>       ::= <variable> <replace> <expression> <iteration control>

<iteration control>     ::= TO <expression>
                          | TO <expression> BY <expression>

<procedure definition>  ::= <procedure head> <statement list> <ending>

<procedure head>        ::= <procedure name> ;
                          | <procedure name> <type> ;
                          | <procedure name> <parameter list> ;
                          | <procedure name> <parameter list> <type> ;

<procedure name>        ::=  <label definition> PROCEDURE

<parameter list>        ::= <parameter head> <identifier> )

<parameter head>        ::=  (
                          | <parameter head> <identifier> ,

<ending>                ::= END
                          | END <identifier>
                          | <label definition> <ending>
						  
<return statement>      ::= RETURN
                          | RETURN <expression>

<call statement>        ::= CALL <variable>

<declaration statement> ::= DECLARE <declaration element>
		          | <declaration statement> , <declaration element>

<declaration element>   ::= <type declaration>
                          | <identifier> LITERALLY <string>

<type declaration>      ::= <identifier specification> <type>
                          | <bound head> <number> ) <type>
                          | <type declaration> <initial list>


<bound head>            ::= <identifier specification> (

<identifier specification> ::= <identifier>
                             | <identifier list> <identifier> )

<identifier list>          ::= (
                             | <identifier list> <identifier> ,
 
<initial list>             ::= <initial head> <constant> )

<initial head>             ::= INITIAL (
                             | <initial head> <constant> ,
<program>		::= <statement list> EOF


using Assign_sign = abnf::Alternatives<
                        abnf::String<ABNF_STATIC_STRING("<-")>   
                        >;                         
using Delimeter_sign = abnf::Alternatives<
                        abnf::String<ABNF_STATIC_STRING(";")>   
                        >;                         
using Rule_relation = abnf::Alternatives<
                        abnf::String<ABNF_STATIC_STRING("<")>,   
                        abnf::String<ABNF_STATIC_STRING(">")>,   
                        abnf::String<ABNF_STATIC_STRING("<=")>,   
                        abnf::String<ABNF_STATIC_STRING(">=")>,   
                        abnf::String<ABNF_STATIC_STRING("==")>,   
                        abnf::String<ABNF_STATIC_STRING("!=")>   
                        >;                         
using Rule_type = abnf::Alternatives<
                        abnf::String<ABNF_STATIC_STRING("FIXED")>
                      >;
using Rule_digit = abnf::Rule_dec_val;
using Rule_string = abnf::Rule_quoted_string;
using Rule_id_char = abnf::Repeat<
		abnf::Alternatives<
			abnf::CodeRange<0x20, 0x21>,
			abnf::CodeRange<0x23, 0x7E>
		>
	>;
using Rule_identifier_helper = Rule_id_char;
using Rule_identifier = abnf::Alternatives<
                            Rule_id_char,
                            abnf::Concat<Rule_identifier_helper, Rule_id_char>,
                            abnf::Concat<Rule_identifier_helper, Rule_digit>
                            >;
using Rule_constant = abnf::Alternatives<
                        Rule_string,
                        Rule_digit,
                        abnf::Concat<abnf::String<ABNF_STATIC_STRING("(")>,
                        Rule_expression,
                        abnf::Concat<abnf::String<ABNF_STATIC_STRING(")")>>
                        >;
using Rule_string_expression = abnf::Alternatives<Rule_string>; //TODO
using Rule_term = abnf::Alternatives<
                    Rule_primary,
                    abnf::Concat<Rule_term, abnf::ABNF_STATIC_STRING("*"), Rule_primary>,
                    abnf::Concat<Rule_term, abnf::ABNF_STATIC_STRING("/"), Rule_primary>,
                    abnf::Concat<Rule_term, abnf::ABNF_STATIC_STRING("MOD"), Rule_primary>,
                    >;
using Rule_arithmetic = abnf::Alternatives<
                            Rule_term,
                            abnf::Concat<Rule_arithmetic, abnf::ABNF_STATIC_STRING("+"), Rule_term>,
                            abnf::Concat<Rule_arithmetic, abnf::ABNF_STATIC_STRING("-"), Rule_term>,
                            abnf::Concat<abnf::ABNF_STATIC_STRING("+"), Rule_term>,
                            abnf::Concat<abnf::ABNF_STATIC_STRING("-"), Rule_term>
                            >; 
using Rule_logic_primary = abnf::Alternatives<Rule_string_expression,
                                abnf::Concat<Rule_string, Rule_arithmetic>
                                >;
using Rule_login_secondary = abnf::Alternatives<Rule_logic_primary,
                                abnf::Concat<abnf::ABNF_STATIC_STRING("!"), Rule_logic_primary>
                                >;
using Rule_logic_factor =  abnf::Alternatives<Rule_logic_secondary,
                                abnf::Concat<Rule_logic_factor, abnf::ABNF_STATIC_STRING("&"), Rule_logic_secondary>,
                                abnf::Concat<Rule_logic_factor, abnf::ABNF_STATIC_STRING("|"), Rule_logic_secondary>
                                >;
using Rule_expression_helper = Rule_logic_factor;
using Rule_expression = abnf::Alternatives<Rule_expression_helper, 
                        abnf::Alternatives<Rule_expression_helper, Rule_logic_factor>>;
using Rule_primary = abnf::Alternatives<
                        Rule_constant,
                        Rule_variable,
                        Rule_expression
                        >;
using Assign_sign = abnf::Alternatives<
                        abnf::String<ABNF_STATIC_STRING("<-")>   
                        >;                         
using Rule_variable = Rule_identifier;
using Rule_left_part = abnf::Concat<Rule_variable, abnf::ABNF_STATIC_STRING(",")>;
using Rule_assignment = abnf::Alternatives<
                        abnf::Concat<Rule_variable, Assign_sign, Rule_expression>,
                        abnf::Concat<Rule_left_part, Rule_assignment>
                        >;
using Rule_ending = abnf::Alternatives<abnf::String::ABNF_STATIC_STRING("end"),
                    abnf::Concat<abnf::String::ABNF_STATIC_STRING("end"), Rule_identifier>>;
using Rule_basic_statement = abnf::Alternatives<
                            
                            >;
using Rule_if_clause = abnf::Concat<abnf::String::ABNF_STATIC_STRING("if"),
                         Rule_expression, abnf::String::ABNF_STATIC_STRING("then")>;
using Rule_true = abnf::Concat<Rule_basic_statement, abnf::String::ABNF_STATIC_STRING("else")>;
using Rule_if_statement = abnf::Alternatives<abnf::Concat<Rule_if_clause, Rule_statement>,
                            abnf::Concat<Rule_if_clause, Rule_true, Rule_statement>>;
using Rule_statement = abnf::Alternatives<Rule_basic_statement, Rule_if_statement>;
using Rule_statement_list = abnf::Alternatives<Rule_statement, 
                                abnf::Concat<Rule_statement_list, Rule_statement>>;
using Rule_while = abnf::Concat<abnf::String::ABNF_STATIC_STRING("while"), Rule_expression>; 
using Rule_case = abnf::Concat<abnf::String::ABNF_STATIC_STRING("case"), Rule_expression>; 
using Rule_group = abnf::Concat<Rule_group_head, Rule_ending>;
using Rule_iteration_control =  abnf::Alternatives<abnf::Concat<abnf::String::ABNF_STATIC_STRING("to"), Rule_expression>,
                                    abnf::Concat<abnf::String::ABNF_STATIC_STRING("to"), Rule_expression, 
                                    abnf::String::ABNF_STATIC_STRING("by"), Rule_expression>
                                >;  
using Rule_step_definition = abnf::Concat<Rule_variable, Assign_sign, Rule_expression, Rule_iteration_control>;        
using Rule_group_head = abnf::Alternatives<abnf::Concat<abnf::String::ABNF_STATIC_STRING("do"), abnf::String::ABNF_STATIC_STRING(";")>,
                                            abnf::Concat<abnf::String::ABNF_STATIC_STRING("do"), Rule_step_definition, abnf::String::ABNF_STATIC_STRING(";")>,
                                            abnf::Concat<abnf::String::ABNF_STATIC_STRING("do"), Rule_while, abnf::String::ABNF_STATIC_STRING(";")>,
                                            abnf::Concat<abnf::String::ABNF_STATIC_STRING("do"), Rule_case, abnf::String::ABNF_STATIC_STRING(";")>,
                                            abnf::Concat<Rule_group_head, Rule_statement>
                                            >; 
using Rule_program = Rule_statement_list;
*/


/*
 1Я) <программа> :: = ’begin’ {(объявление> ’;’} {(команда) ’;’} ’end’
 2Н) <объявление> :: = (’var’ | ’агг’) (имя) {’;’ (имя)}

Зя) (команда) :: = (присваивание) | (развилка) (цикл while) | (цикл repeat) | ’skip’ | ’trap’

    4Я) <присваивание>:: = (переменная) ’:=’ (выражение)
    5Я) (переменная) :: = (имя) [(индекс)]
    6Я) (индекс) :: = ’[’ (выражение) ’]’
    7Н) (выражение) :: = (переменная) | (число) (функция)
    8Я) (функция) :: = <1шя> ’(’ (выражение) (выражение)} ’)’
    9Я) (развилка) :: = ’if’ (условие) ’then’

[(команда) {’;’ (команда)}]

[’else’ (команда) {’;’ (команда)}] ’fi’

10я) (цикл while) :: = ’while’ (условие) ’do’

[(команда) {’;’ (команда)}] ’od’

11я) (цикл repeat):: = ’repeat’ [(команда) {’;’ (команда)}]

’until’ (условие)

    12я) (условие) :: = (выражение) (’<’ | ’<’ | ’=’ | V’ | ’>’ | ’>’)
    (выражение)
    13я) (нжя) :: = (буква) {(буква) | (цифра)}
    14я) (число) :: = (цифра) {(цифра)}
    15я) (буква) :: = ’а’ | ’b’ | ’с’ | ’d’ | ’е’ | ’/ | ’g’ | ’h’ | ’i’ | ’/ | ’к’ | ’Г |

’m’ | V | ’o’ | у | V | ’5’ | V | V | V | V | У |

У | У

16н) (цифра) :: = ’O’ | ’1’ | ’2’ | ’3’ | 4’ | ’5’ | ’6’ | 7’ | ’8’ | ’9’ +++++++++
 */
using Ignorable = abnf::Optional<abnf::Repeat<abnf::String<ABNF_STATIC_STRING(" ")>, 1>>;
using Assign_sign = abnf::String<ABNF_STATIC_STRING("<-")>;                         
using Delimeter_sign = abnf::String<ABNF_STATIC_STRING(";")>;  
using Rule_relation = abnf::Alternatives<abnf::String<ABNF_STATIC_STRING("<")>, abnf::String<ABNF_STATIC_STRING(">")>, abnf::String<ABNF_STATIC_STRING("<=")>,   
abnf::String<ABNF_STATIC_STRING(">=")>, abnf::String<ABNF_STATIC_STRING("==")>, abnf::String<ABNF_STATIC_STRING("!=")>>;            

using Rule_types = abnf::Alternatives<abnf::String<ABNF_STATIC_STRING("FIXED")>>;
using Rule_digit = abnf::Rule_dec_val;
using Rule_string = abnf::Rule_quoted_string;
using Rule_id_char = abnf::Repeat<abnf::Alternatives<abnf::CodeRange<0x20, 0x21>, abnf::CodeRange<0x23, 0x7E>>>;
using Rule_identificator = abnf::IdPredicate;//abnf::Concat<abnf::Rule_token, abnf::Optional<abnf::Rule_dec_val>>;
using Rule_variable = abnf::Concat<Rule_identificator, Ignorable, abnf::Optional<abnf::Concat<abnf::String<ABNF_STATIC_STRING("[")>, 
Ignorable, Rule_digit, Ignorable,  abnf::String<ABNF_STATIC_STRING("]")>, Ignorable>>>;
      
using Rule_assignment = abnf::Concat<Rule_identificator, Ignorable,  Assign_sign, Ignorable,
abnf::Alternatives<Rule_identificator, Rule_digit>, Ignorable, Delimeter_sign, Ignorable>;

using Rule_definition = abnf::Concat<abnf::Alternatives<abnf::String<ABNF_STATIC_STRING("var")>, abnf::String<ABNF_STATIC_STRING("arr")>>, abnf::Repeat<abnf::String<ABNF_STATIC_STRING(" ")>, 1>, Rule_variable,/* Ignorable, */Delimeter_sign>;
using Rule_command = abnf::Concat<abnf::Alternatives<Rule_assignment>, abnf::Repeat<abnf::String<ABNF_STATIC_STRING(" ")>>>;

#define Rule_function abnf::EmptyRule

using Rule_expression = abnf::Alternatives<Rule_variable, Rule_digit, Rule_function>;
#undef Rule_function

#define Rule_function abnf::Concat<Rule_identificator, abnf::Optional<abnf::Repeat<abnf::String<ABNF_STATIC_STRING(" ")>, 1>>,\
abnf::String<ABNF_STATIC_STRING("(")>,\
abnf::Optional<abnf::Repeat<abnf::String<ABNF_STATIC_STRING(" ")>, 1>>,\
abnf::Optional<abnf::Concat<abnf::Optional<abnf::Repeat<Rule_expression>, 1>>,\
abnf::Optional<abnf::Repeat<abnf::String<ABNF_STATIC_STRING(",")>, 1>>>>,\
abnf::Optional<abnf::Repeat<abnf::String<ABNF_STATIC_STRING(" ")>, 1>>,\
abnf::String<ABNF_STATIC_STRING(")")>>

using Rule_condition = abnf::Concat<Rule_expression, Rule_relation, Rule_expression>;

using Rule_if = abnf::Concat<abnf::String<ABNF_STATIC_STRING("if")>, Ignorable, abnf::String<ABNF_STATIC_STRING("(")>, Ignorable, 
Rule_condition, Ignorable, abnf::String<ABNF_STATIC_STRING(")")>, Ignorable, abnf::String<ABNF_STATIC_STRING("then")>, Ignorable,
abnf::String<ABNF_STATIC_STRING("{")>, Ignorable,

abnf::Optional<abnf::Repeat<Rule_command, 1>>,

Ignorable,
abnf::String<ABNF_STATIC_STRING("}")>
/*,
Ignorable,

abnf::Optional<abnf::Concat<
abnf::String<ABNF_STATIC_STRING("else")>, Ignorable,
abnf::Repeat<abnf::String<ABNF_STATIC_STRING("{")>, 1>, Ignorable,

abnf::Optional<abnf::Repeat<Rule_command, 1>>,

Ignorable,
abnf::Repeat<abnf::String<ABNF_STATIC_STRING("}")>, 1>>
>*/
>;

using Rule_while = abnf::Concat<abnf::String<ABNF_STATIC_STRING("while")>, Ignorable, 
abnf::String<ABNF_STATIC_STRING("(")>, Ignorable, Rule_condition, Ignorable, abnf::String<ABNF_STATIC_STRING(")")>, Ignorable,
abnf::Repeat<abnf::String<ABNF_STATIC_STRING("{")>, 1>, Ignorable,

abnf::Optional<abnf::Repeat<Rule_command, 1>>,

Ignorable,
abnf::Repeat<abnf::String<ABNF_STATIC_STRING("}")>, 1>, Ignorable
>;

using Rule_program = abnf::Concat<
abnf::String<ABNF_STATIC_STRING("begin")>, 
        /*Ignorable,*/ abnf::Repeat<abnf::Alternatives<Rule_command, Rule_definition, Rule_condition>>,/*abnf::Repeat<abnf::Rule_WSP, 1>,*/
           abnf::String<ABNF_STATIC_STRING("end.")>>;

