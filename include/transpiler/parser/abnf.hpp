#ifndef ABNF_HPP
#define ABNF_HPP

#include <vector>
#include <string>
#include <regex>
#include <iterator>
#include <utility>
#include <ranges>
#include <limits>

struct MatchInfo 
{
    bool matched = true;
    std::size_t end_pos = 0;
    std::size_t error_pos = std::string::npos;
};

struct RuleBase
{
    RuleBase() {}
    virtual MatchInfo get_match(std::string::iterator begin, std::string::iterator end) 
    {
        MatchInfo match_info { .matched = false };
        return match_info;
    }
    ~RuleBase() {}
};

class RuleContainer
{
public: 
    RuleContainer() {}    
    RuleContainer(std::initializer_list<RuleBase*> l) : m_rules(l) {}

    using value_type = std::vector<RuleBase*>::value_type;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;
    using reference = std::vector<RuleBase*>::reference;
    using const_reference = std::vector<RuleBase*>::const_reference;
    using pointer = std::vector<RuleBase*>::pointer;
    using const_pointer = std::vector<RuleBase*>::const_pointer;
    using iterator = std::vector<RuleBase*>::iterator;
    using const_iterator = std::vector<RuleBase*>::const_iterator;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    iterator begin() noexcept 
    {
        return m_rules.begin();
    }
    const_iterator begin() const noexcept 
    {
        return m_rules.begin();
    }
    const_iterator cbegin() const noexcept
    {
        return m_rules.cbegin();
    }
    iterator end() noexcept 
    {
        return m_rules.end();
    }
    const_iterator end() const noexcept 
    {
        return m_rules.end();
    }
    const_iterator cend() const noexcept
    {
        return m_rules.cend();
    }
    void push_back(const_reference value)
    {
        m_rules.push_back(value);
    }
    iterator erase(const_iterator pos) 
    {
        return m_rules.erase(pos);
    }
    reference operator[](size_type pos) 
    {
        return m_rules[pos];
    }
private:
    std::vector<RuleBase*> m_rules;
};

class ConcatImpl : public RuleBase
{
public:
    ConcatImpl(){}
    ConcatImpl(std::initializer_list<RuleBase*> l) : m_rules_container(l) {}
    void push_back(RuleBase& value)
    {
        m_rules_container.push_back(&value);
    }
    void set_rule_at(std::size_t pos, RuleBase* rule)
    {
        m_rules_container[pos] = rule;
    }
    virtual MatchInfo get_match(std::string::iterator begin, std::string::iterator end) 
    {        
        MatchInfo match_info;
        for (auto rule_it = m_rules_container.begin(); rule_it != m_rules_container.end()
            && match_info.matched; ++rule_it)
        {
            auto rule_match = (*rule_it)->get_match(begin, end);
            match_info.matched &= rule_match.matched;

            begin += rule_match.end_pos;
            match_info.end_pos += rule_match.end_pos;
        }
        return match_info;
    }
    ~ConcatImpl() {}
private:
    RuleContainer m_rules_container;
};

class AlterImpl : public RuleBase
{
public:
    AlterImpl(){}
    AlterImpl(std::initializer_list<RuleBase*> l) : m_rules_container(l) {}
    void push_back(RuleBase& value)
    {
        m_rules_container.push_back(&value);
    }
    void set_rule_at(std::size_t pos, RuleBase* rule)
    {
        m_rules_container[pos] = rule;
    }
    virtual MatchInfo get_match(std::string::iterator begin, std::string::iterator end) 
    {
        MatchInfo match_info{ .matched = false };
        for (auto rule_it = m_rules_container.begin(); rule_it != m_rules_container.end()
            && !match_info.matched; ++rule_it)
        {
            match_info = (*rule_it)->get_match(begin, end);
        }
        return match_info;
    }
    ~AlterImpl() {}
private:
    RuleContainer m_rules_container; 
};

class RangeAlterImpl : public RuleBase
{
public:
    RangeAlterImpl(){} 
    RangeAlterImpl(char start_sym, char end_sym) : m_start(start_sym), m_end(end_sym) {}
    
    virtual MatchInfo get_match(std::string::iterator begin, std::string::iterator end) 
    {
        std::string_view str(begin, end);
        std::size_t pos = 1;
        auto range = std::ranges::iota_view{ m_start, m_end };
        for (auto ch_it = range.begin(); pos > 0 && ch_it != range.end(); ++ch_it)
        {
            pos = str.find(*ch_it);
        }

        return MatchInfo
        {
            .matched = !pos,
            .end_pos = !pos ? (pos + 1) : std::string::npos,
            .error_pos = pos ? std::string::npos : (pos + 1)
        };
    }
    ~RangeAlterImpl() {}
private:
    char m_start;
    char m_end;
};

class RepeatImpl : public RuleBase
{
public:
    RepeatImpl(){}
    RepeatImpl(RuleBase* rule, std::size_t min = 0, std::size_t max = std::numeric_limits<std::size_t>::max()) :
    m_rule(rule), m_min_cardinality(min), m_max_cardinality(max) {}

    virtual MatchInfo get_match(std::string::iterator begin, std::string::iterator end) 
    {
        MatchInfo match_info;
        MatchInfo rule_match = match_info;
        
        std::size_t repeats = 0;
        while (rule_match.matched)
        {
            rule_match = m_rule->get_match(begin, end);

            if (rule_match.matched)
            {
                ++repeats;
                begin += rule_match.end_pos;
                match_info.end_pos += rule_match.end_pos;
            }
            else
                match_info.error_pos = rule_match.end_pos;
        }

        if (repeats >= m_min_cardinality && repeats <= m_max_cardinality)
            match_info.matched = true;

        return match_info;
    }
    ~RepeatImpl() {}
private:
    RuleBase* m_rule = nullptr;
    std::size_t m_min_cardinality = 0;
    std::size_t m_max_cardinality = std::numeric_limits<std::size_t>::max();
};

class StaticStringImpl : public RuleBase
{
public:
    StaticStringImpl(){}
    StaticStringImpl(std::string str) : m_str(str) {}

    virtual MatchInfo get_match(std::string::iterator begin, std::string::iterator end) 
    {
        auto pos = std::string_view(begin, end).find(m_str);
        MatchInfo match_info{ .matched = pos == 0, 
            .end_pos = pos + m_str.size(),
            .error_pos = (pos != 0) ? std::string::npos : pos };
        return match_info;
    }
    ~StaticStringImpl() {}
private:
    std::string m_str;
};

class DynamicStringImpl : public RuleBase
{
public:
    DynamicStringImpl(){}
    DynamicStringImpl(std::string delimeters = " \n\r\t\0", std::string allowed_symbols = " \n\r\t\0") :
        m_delimeters(delimeters), m_allowed_symbols(allowed_symbols) {}
    DynamicStringImpl(std::regex regex) : m_regex(regex) {}
    
    virtual MatchInfo get_match(std::string::iterator begin, std::string::iterator end) 
    {
        MatchInfo match_info{ .matched = false, .end_pos = std::string::npos };
        auto str = std::string_view(begin, end);
        if (!m_allowed_symbols.empty())
        {
            auto delim_pos = get_delimeter_pos(str);
            std::size_t string_end_pos = 0;

            if (delim_pos != str.end())
            {
                for (auto ch_it = str.begin(); ch_it != delim_pos
                    && string_end_pos != std::string::npos; ++ch_it)
                {
                    string_end_pos = m_allowed_symbols.find(*ch_it);
                }
            }
            match_info.matched = string_end_pos != std::string::npos;
            match_info.end_pos = !match_info.matched ? std::string::npos : string_end_pos;
        }
        else
        {
            auto b = std::sregex_iterator(begin, end, m_regex);
            auto e = std::sregex_iterator();
            match_info.matched = (b != e && b->position() == 0);
            if (match_info.matched)
                match_info.error_pos = std::string::npos;
            else 
                match_info.error_pos = b == e ? 0 : b->position();

            match_info.end_pos = !match_info.matched ? std::string::npos : b->position() + b->length();
        }
        return match_info;
    }
    ~DynamicStringImpl() {}
private:
    std::string_view::iterator get_delimeter_pos(std::string_view& str) const
    {
        std::string_view::iterator delim_pos = str.end();

        for (auto delim_it = m_delimeters.begin(); delim_it != m_delimeters.end()
            && delim_pos != str.end(); ++delim_it)
        {
            delim_pos = std::find(str.begin(), str.end(), *delim_it);
        }
        return delim_pos;
    }
    std::string m_delimeters;
    std::string m_allowed_symbols;
    std::regex m_regex;
};

#endif


