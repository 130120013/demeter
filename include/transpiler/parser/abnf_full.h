/*
See RFC-5234 at https://tools.ietf.org/pdf/rfc5234
And RFC-7405 at https://tools.ietf.org/pdf/rfc7405
*/
#include <utility>
#include <set>
#include <iterator>
#include <algorithm>
#include <list>
#include <tuple>
#include <string>
#include <regex>
#include "abnf_utils.h"
//#include <sstream>

#ifndef SIMPLE_WEBSERVER_ABNF_H
#define SIMPLE_WEBSERVER_ABNF_H

#ifndef ABNF_BEGIN
#define ABNF_BEGIN namespace abnf {
#endif //HTTP_BEGIN

#ifndef ABNF_END
#define ABNF_END }
#endif // HTTP_END

ABNF_BEGIN

template <class CharType>
using supported_char_type = std::bool_constant<
	std::is_same_v<CharType, char> || std::is_same_v<CharType, wchar_t> || std::is_same_v<CharType, ucp_t>>;

template <class CharType>
constexpr bool supported_char_type_v = supported_char_type<CharType>::value;

constexpr char impl_to_upper(char ch) noexcept
{
	return (ch >= 0x61 && ch <= 0x7A)?ch - 0x20:ch;
}

constexpr bool is_alpha(char ch) noexcept
{
	return (ch >= 0x41 && ch <= 0x5A) || (ch >= 0x61 && ch <= 0x7A);
}

constexpr bool is_bit(char ch) noexcept
{
	return ch == 0x30 || ch == 0x31;
}

constexpr bool is_char(char ch) noexcept
{
	return ch >= 1 && ch <= 0x7F;
}

constexpr bool is_cr(char ch) noexcept
{
	return ch == 0xD;
}

constexpr bool is_ctl(char ch) noexcept
{
	return ch <= 0x1F || ch == 0x7F;
}

constexpr bool is_digit(char ch) noexcept
{
	return ch >= 0x30 && ch <= 0x39;
}

constexpr bool is_dquote(char ch) noexcept
{
	return ch == 0x22;
}

constexpr bool is_hexdig(char ch) noexcept
{
	return ch <= 'f' && (is_digit(ch) ||
		(ch >= 'A' && ch <= 'F') || ch >= 'a');
}

constexpr bool is_htab(char ch) noexcept
{
	return ch == 0x9;
}

constexpr bool is_lf(char ch) noexcept
{
	return ch == 0xA;
}

constexpr bool is_octet(char ch) noexcept
{
	return ch <= 0xFF;
}

constexpr bool is_sp(char ch) noexcept
{
	return ch == 0x20;
}

constexpr bool is_vchar(char ch) noexcept
{
	return ch >= 0x21 && ch <= 0x7E;
}

constexpr bool is_wsp(char ch) noexcept
{
	return is_htab(ch) || is_sp(ch);
}

bool is_id(std::string::iterator begin, std::string::iterator end) noexcept
{
    std::ptrdiff_t new_begin_pos = -1;
    std::regex pattern("^([a-zA-Z]{1,}()\\_){0,}(\\d){0,}", std::regex_constants::icase);
    auto b = std::sregex_iterator(begin, end, pattern);
    auto e = std::sregex_iterator();
    
    return (b != e && b->length() > 0 && b->position() == 0);
}

template <class ... Rules>
struct rule_storage {};

template <>
struct rule_storage<> {};

//template <class Rule_0, class ... Rest>
//struct rule_storage<Rule_0, Rest...>;

template <std::size_t I, class RuleStorageType>
struct rule_storage_element {};

template <class Rule0, class ... RulesRest>
struct rule_storage_element<0, rule_storage<Rule0, RulesRest...>>
{
	typedef Rule0 type;
};

template <std::size_t I, class Rule0, class ... RulesRest>
struct rule_storage_element<I, rule_storage<Rule0, RulesRest...>>
{
	typedef typename rule_storage_element<I - 1, rule_storage<RulesRest...>>::type type;
};

template <std::size_t I, class RuleStorageType>
using rule_storage_element_t = typename rule_storage_element<I, RuleStorageType>::type;

template <class Rule, class Other, class = void>
struct rule_storage_1;

template <class Rule, class ... Rules>
struct rule_storage_1<Rule, rule_storage<Rules...>, std::enable_if_t<!std::is_empty_v<rule_storage<Rules...>>>>:private Rule
{
	rule_storage_1() = default;
	template <class MyRuleParamType, class ... OtherRuleParamTypes,
		class = std::enable_if_t<std::is_constructible_v<Rule, MyRuleParamType&&> && std::conjunction_v<std::is_constructible<Rules, OtherRuleParamTypes&&>...>>>
	constexpr rule_storage_1(MyRuleParamType&& rule_0, OtherRuleParamTypes&& ... rule_rest)
		noexcept(std::is_nothrow_constructible_v<Rule, MyRuleParamType&&> && std::conjunction_v<std::is_nothrow_constructible<Rules, OtherRuleParamTypes&&>...>)
		:Rule(std::forward<MyRuleParamType>(rule_0)), m_rest(std::forward<OtherRuleParamTypes>(rule_rest)...) {}
	template <std::size_t I> constexpr auto get_rule() const & noexcept -> std::enable_if_t<(I == 0), const Rule&>
	{
		return static_cast<const Rule&>(*this);
	}
	template <std::size_t I> auto get_rule() & noexcept -> std::enable_if_t<(I == 0), Rule&>
	{
		return static_cast<Rule&>(*this);
	}
	template <std::size_t I> constexpr auto get_rule() const && noexcept -> std::enable_if_t<(I == 0), const Rule&&>
	{
		return static_cast<const Rule&&>(*this);
	}
	template <std::size_t I> auto get_rule() && noexcept -> std::enable_if_t<(I == 0), Rule&&>
	{
		return static_cast<Rule&&>(*this);
	}
#ifndef _MSC_VER //see https://developercommunity.visualstudio.com/content/problem/338193/sfinae-disabled-ref-qualified-function-collides-wi.html
	template <std::size_t I, class = std::enable_if_t<(I > 0)>> constexpr decltype(auto) get_rule() const noexcept
	{
		return m_rest.template get_rule<I - 1>();
	}
	template <std::size_t I, class = std::enable_if_t<(I > 0)>> decltype(auto) get_rule() noexcept
	{
		return m_rest.template get_rule<I - 1>();
	}
#else //_MSC_VER
	template <std::size_t I, class = std::enable_if_t<(I > 0)>> constexpr decltype(auto) get_rule() const & noexcept
	{
		return m_rest.template get_rule<I - 1>();
	}
	template <std::size_t I, class = std::enable_if_t<(I > 0)>> decltype(auto) get_rule() & noexcept
	{
		return m_rest.template get_rule<I - 1>();
	}
	template <std::size_t I, class = std::enable_if_t<(I > 0)>> constexpr decltype(auto) get_rule() const && noexcept
	{
		return static_cast<const rule_storage<Rules...>&&>(m_rest).template get_rule<I - 1>();
	}
	template <std::size_t I, class = std::enable_if_t<(I > 0)>> decltype(auto) get_rule() && noexcept
	{
		return static_cast<rule_storage<Rules...>&&>(m_rest).template get_rule<I - 1>();
	}
#endif //_MSC_VER
private:
	rule_storage<Rules...> m_rest;
};

template <class Rule, class ... Rules>
struct rule_storage_1<Rule, rule_storage<Rules...>, std::enable_if_t<std::is_empty_v<rule_storage<Rules...>>>>:private Rule
{
	rule_storage_1() = default;
	template <class MyRuleParamType, class ... OtherRuleParamTypes,
		class = std::enable_if_t<std::is_constructible_v<Rule, MyRuleParamType&&> && std::conjunction_v<std::is_constructible<Rules, OtherRuleParamTypes&&>...>>>
	constexpr rule_storage_1(MyRuleParamType&& rule_0, OtherRuleParamTypes&& ... rule_rest)
		noexcept(std::is_nothrow_constructible_v<Rule, MyRuleParamType&&> && std::conjunction_v<std::is_nothrow_constructible<Rules, OtherRuleParamTypes&&>...>)
		:Rule(std::forward<MyRuleParamType>(rule_0)), rule_storage<Rules...>(std::forward<OtherRuleParamTypes>(rule_rest)...) {}
	template <std::size_t I> constexpr auto get_rule() const & noexcept -> std::enable_if_t<(I == 0), const Rule&>
	{
		return static_cast<const Rule&>(*this);
	}
	template <std::size_t I> auto get_rule() & noexcept -> std::enable_if_t<(I == 0), Rule&>
	{
		return static_cast<Rule&>(*this);
	}
	template <std::size_t I> constexpr auto get_rule() const && noexcept -> std::enable_if_t<(I == 0), const Rule&&>
	{
		return static_cast<const Rule&&>(*this);
	}
	template <std::size_t I> auto get_rule() && noexcept -> std::enable_if_t<(I == 0), Rule&&>
	{
		return static_cast<Rule&&>(*this);
	}
	template <std::size_t I, class = std::enable_if_t<(I > 0)>> constexpr auto get_rule() const &
		noexcept(std::is_nothrow_default_constructible_v<rule_storage<Rules...>> && std::is_nothrow_default_constructible_v<rule_storage_element_t<I - 1, rule_storage<Rules...>>>)
	{
		return rule_storage<Rules...>().template get_rule<I - 1>();
	}
};

template <class Rule_0, class ... Rest>
struct rule_storage<Rule_0, Rest...>:rule_storage_1<Rule_0, rule_storage<Rest...>>
{
	using rule_storage_1<Rule_0, rule_storage<Rest...>>::rule_storage_1;
};

template <std::size_t I, class RuleStorageType>
constexpr auto get(RuleStorageType&& rule_storage) noexcept(noexcept(std::declval<RuleStorageType&&>().template get_rule<I>()))
-> decltype(std::declval<RuleStorageType&&>().template get_rule<I>())
{
	return std::forward<RuleStorageType>(rule_storage).template get_rule<I>();
}

template <class ... Storages> struct rule_storage_concat {};

template <class ... StrgTypes> struct rule_storage_concat<rule_storage<StrgTypes...>>
{
	typedef rule_storage<StrgTypes...> type;
};

template <class ... Strg1Types, class ... Strg2Types> struct rule_storage_concat<rule_storage<Strg1Types...>, rule_storage<Strg2Types...>>
{
	typedef rule_storage<Strg1Types..., Strg2Types...> type;
};

template <class Strg1, class Strg2, class ... Rest> struct rule_storage_concat<Strg1, Strg2, Rest...>
{
	typedef typename rule_storage_concat<typename rule_storage_concat<Strg1, Strg2>::type, Rest...>::type type;
};

template <class ... RuleStorages> using rule_storage_concat_t = typename rule_storage_concat<RuleStorages...>::type;

template <class ForwardIterator>
class match_set;

template <class ForwardIterator>
struct match_set_value
{
	std::size_t length;
	ForwardIterator end;
};

template <class ForwardIterator>
constexpr bool operator<(const match_set_value<ForwardIterator>& left, const match_set_value<ForwardIterator>& right) noexcept
{
	return left.length < right.length;
}

template <class ForwardIterator>
constexpr bool operator==(const match_set_value<ForwardIterator>& left, const match_set_value<ForwardIterator>& right) noexcept
{
	return left.length == right.length;
}

constexpr std::size_t match_set_sbo_count = 128;

template <class ForwardIterator, std::size_t list_match_set_sbo_count = match_set_sbo_count>
class sbo_list
{
public:
	typedef match_set_value<ForwardIterator> value_type, *pointer, &reference;
	typedef const match_set_value<ForwardIterator> *const_pointer, &const_reference;
	typedef std::size_t size_type;
	typedef std::ptrdiff_t difference_type;
private:
	struct node
	{
		value_type value;
		node* next;
	};
	std::size_t m_count;
	value_type m_sbo[list_match_set_sbo_count];
	pointer m_pHead, m_pTail;
	node* m_pDynamic;

	inline const_pointer sbo_begin() const noexcept
	{
		return &m_sbo[0];
	}
	inline pointer sbo_begin() noexcept
	{
		return &m_sbo[0];
	}
	inline const_pointer sbo_end() const noexcept
	{
		return &m_sbo[list_match_set_sbo_count];
	}
	inline pointer sbo_end() noexcept
	{
		return &m_sbo[list_match_set_sbo_count];
	}
	inline bool in_sbo(const_pointer pValue) const noexcept
	{
		return pValue >= this->sbo_begin() && pValue < this->sbo_end();
	}
	static constexpr const node* get_node_ptr(const_pointer pVal) noexcept
	{
		return reinterpret_cast<const node*>(reinterpret_cast<const char*>(pVal) - offsetof(node, value));
	}
	static inline node* get_node_ptr(pointer pVal) noexcept
	{
		return reinterpret_cast<node*>(reinterpret_cast<char*>(pVal) - offsetof(node, value));
	}

	struct forward_immutable_iterator
	{
		typedef const match_set_value<ForwardIterator> value_type, *pointer, &reference;
		typedef std::size_t size_type;
		typedef std::ptrdiff_t difference_type;
		typedef std::forward_iterator_tag iterator_category;
	private:
		const sbo_list<ForwardIterator, list_match_set_sbo_count>* m_pList = nullptr;
		pointer m_pValue = nullptr;
	public:
		forward_immutable_iterator() = default;
		forward_immutable_iterator(const sbo_list<ForwardIterator, list_match_set_sbo_count>& rList, reference rValue) noexcept:m_pList(&rList), m_pValue(&rValue) {}
		reference operator*() const noexcept
		{
			return *m_pValue;
		}
		pointer operator->() const noexcept
		{
			return m_pValue;
		}
		forward_immutable_iterator& operator++() noexcept
		{
			if (m_pList->in_sbo(m_pValue))
			{
				if (++m_pValue == m_pList->m_pTail)
					m_pValue = nullptr;
				else if (m_pValue == m_pList->sbo_end())
					m_pValue = &m_pList->m_pDynamic->value;
			}else
				m_pValue = &get_node_ptr(m_pValue)->next->value;
			return *this;
		}
		forward_immutable_iterator operator++(int) noexcept
		{
			auto old = *this;
			++*this;
			return old;
		}
		bool operator==(const forward_immutable_iterator& right) const noexcept
		{
			return m_pValue == right.m_pValue;
		}
		bool operator!=(const forward_immutable_iterator& right) const noexcept
		{
			return m_pValue != right.m_pValue;
		}
	};

	struct forward_mutable_iterator:forward_immutable_iterator
	{
		typedef match_set_value<ForwardIterator> value_type, *pointer, &reference;
		typedef std::size_t size_type;
		typedef std::ptrdiff_t difference_type;
		typedef std::forward_iterator_tag iterator_category;
		forward_mutable_iterator() = default;
		forward_mutable_iterator(sbo_list<ForwardIterator, list_match_set_sbo_count>& rList, reference rValue) noexcept :forward_immutable_iterator(rList, rValue) {}
		reference operator*() const noexcept
		{
			return const_cast<reference>(*static_cast<const forward_immutable_iterator&>(*this));
		}
		pointer operator->() const noexcept
		{
			return const_cast<pointer>(this->forward_immutable_iterator::operator->());
		}
		forward_mutable_iterator& operator++() noexcept
		{
			++static_cast<forward_immutable_iterator&>(*this);
			return *this;
		}
		forward_mutable_iterator operator++(int) noexcept
		{
			auto old = *this;
			++*this;
			return old;
		}
	};

	void set_list(const sbo_list& right)
	{
		if (right.in_sbo(right.m_pHead))
		{
			m_pHead = this->sbo_begin();
			m_pTail = std::copy(const_pointer(right.m_pHead), right.in_sbo(right.m_pTail)?right.m_pTail:right.sbo_end(), this->sbo_begin());
		}
		auto pRightDynamic = right.m_pDynamic;
		while (pRightDynamic != nullptr && m_pTail < this->sbo_end())
		{
			*(m_pTail++) = pRightDynamic->value;
			pRightDynamic = pRightDynamic->next;
		}
		if (pRightDynamic != nullptr)
		{
			auto pNode = m_pDynamic = new node{pRightDynamic->value, nullptr};
			pRightDynamic = pRightDynamic->next;
			while (pRightDynamic != nullptr)
			{
				pNode = pNode->next = new node{pRightDynamic->value, nullptr};
				pRightDynamic = pRightDynamic->next;
			}
			m_pTail = &pNode->value;
		}
		m_count = right.m_count;
	}
	void set_list(sbo_list&& right)
	{
		if (right.m_pHead >= &right.m_sbo[0] && right.m_pHead < &right.m_sbo[list_match_set_sbo_count])
		{
			m_pHead = this->sbo_begin();
			m_pTail = std::move(right.m_pHead, right.in_sbo(right.m_pTail)?right.m_pTail:right.sbo_end(), this->sbo_begin());
		}
		auto pRightDynamic = right.m_pDynamic;
		while (pRightDynamic != nullptr && m_pTail < &m_sbo[list_match_set_sbo_count])
		{
			*(m_pTail++) = std::move(pRightDynamic->value);
			pRightDynamic = pRightDynamic->next;
		}
		m_pDynamic = pRightDynamic;
		right.m_pDynamic = nullptr;
		m_count = right.m_count;
		right.m_count = 0;
		if (right.m_pTail < &right.m_sbo[0] || right.m_pTail > &right.m_sbo[list_match_set_sbo_count])
			m_pTail = right.m_pTail;
		right.m_pHead = right.m_pTail = right.sbo_begin();
	}
	void destroy_nodes(node* pFirstNode) noexcept
	{
		while (pFirstNode != nullptr)
		{
			auto pOld = pFirstNode;
			pFirstNode = pFirstNode->next;
			delete pOld;
		}
	}
	void destroy_nodes() noexcept
	{
		return destroy_nodes(m_pDynamic);
	}

public:
	typedef forward_mutable_iterator iterator;
	typedef forward_immutable_iterator const_iterator;

	sbo_list():m_count(0), m_pHead(&m_sbo[0]), m_pTail(&m_sbo[0]), m_pDynamic(nullptr) {}
	~sbo_list() noexcept
	{
		this->destroy_nodes();
	}
	sbo_list(const sbo_list& right)
	{
		this->set_list(right);
	}
	sbo_list(sbo_list&& right)
	{
		this->set_list(std::move(right));
	}
	sbo_list& operator=(const sbo_list& right)
	{
		if (this == &right)
			return *this;
		this->destroy_nodes();
		this->set_list(right);
		return *this;
	}
	sbo_list& operator=(sbo_list&& right)
	{
		if (this == &right)
			return *this;
		this->destroy_nodes();
		this->set_list(std::move(right));
		return *this;
	}
	template <class ... Args>
	reference emplace_back(Args&&...args)
	{
		if (this->in_sbo(m_pTail))
		{
			*m_pTail = value_type(std::forward<Args>(args)...);
			++m_count;
			return *(m_pTail++);
		}else if (m_pTail == this->sbo_end())
		{
			m_pDynamic = new node{value_type(std::forward<Args>(args)...), nullptr};
			++m_count;
			m_pTail = &m_pDynamic->value;
			return *m_pTail;
		}else
		{
			auto pNode = get_node_ptr(m_pTail);
			pNode->next = new node{value_type(std::forward<Args>(args)...), nullptr};
			m_pTail = &pNode->next->value;
			++m_count;
			return *m_pTail;
		}
	}
	match_set_value<ForwardIterator> pop_front() noexcept(noexcept(std::is_nothrow_move_constructible_v<match_set_value<ForwardIterator>>))
	{
		if (this->in_sbo(m_pHead))
		{
			--m_count;
			auto val = std::move(*m_pHead);
			if (++m_pHead == this->sbo_end())
			{
				if (m_pDynamic != nullptr)
					m_pHead = &m_pDynamic->value;
				else
					m_pHead = m_pTail = this->sbo_begin();
			}
			return val;
		}else
		{
			auto pOld = m_pDynamic;
			auto val = std::move(pOld->value);
			m_pDynamic = pOld->next;
			--m_count;
			delete pOld;
			if (m_pDynamic != nullptr)
				m_pHead = &m_pDynamic->value;
			else
				m_pHead = m_pTail = this->sbo_begin();
			return val;
		}
	}
	const match_set_value<ForwardIterator>& front() const noexcept
	{
		return *m_pHead;
	}
	match_set_value<ForwardIterator>& front() noexcept
	{
		return *m_pHead;
	}
	iterator begin() noexcept
	{
		return forward_mutable_iterator(*this, *m_pHead);
	}
	const_iterator begin() const noexcept
	{
		return forward_immutable_iterator(*this, *m_pHead);
	}
	iterator end() noexcept
	{
		return iterator();
	}
	const_iterator end() const noexcept
	{
		return iterator();
	}
	const_iterator cbegin() const noexcept
	{
		return forward_immutable_iterator(*this, *m_pHead);
	}
	const_iterator cend() const noexcept
	{
		return iterator();
	}
	std::size_t size() const noexcept
	{
		return m_count;
	}
	bool empty() const noexcept
	{
		return this->size() == std::size_t();
	}
	template <class Predicate>
	void append_if(sbo_list&& right, Predicate&& pred)
	{
		if (this == &right)
		{
			for (auto& val:*this)
			{
				if (pred(val))
					this->emplace_back(val);
			}
			return;
		}
		if (this->in_sbo(m_pTail))
		{
			if (right.in_sbo(right.m_pHead))
			{
				auto pRightValue = right.m_pHead;
				auto pRightEnd = right.in_sbo(right.m_pTail)?right.m_pTail:right.sbo_end();
				if (pRightValue == pRightEnd)
					return;
				auto pLeftValue = m_pTail;
				right.m_pHead = right.m_pTail = right.sbo_begin();
				right.m_count = 0;
				while (!pred(*pRightValue))
					if (++pRightValue == pRightEnd)
						return;
				do
				{
					*m_pTail = std::move(*pRightValue);
					++m_count;
					if (++m_pTail == this->sbo_end())
					{
						while (++pRightValue != pRightEnd)
						{
							if (pred(*pRightValue))
							{
								auto pLeftNode = m_pDynamic = new node{std::move(*pRightValue), nullptr};
								++m_count;
								while (++pRightValue != pRightEnd)
								{
									if (pred(*pRightValue))
									{
										pLeftNode->next = new node{std::move(*pRightValue), nullptr};
										pLeftNode = pLeftNode->next;
										++m_count;
									}
								}
								pLeftNode->next = right.m_pDynamic;
								right.m_pDynamic = nullptr;
								while (pLeftNode->next)
								{
									if (pred(pLeftNode->next->value))
									{
										pLeftNode = pLeftNode->next;
										++m_count;
									}else
									{
										auto pLeftNext = pLeftNode->next->next;
										delete pLeftNode->next;
										pLeftNode->next = pLeftNext;
									}
								}
								m_pTail = &pLeftNode->value;
								return;
							}
						}
					}else
					{
						while (++pRightValue != pRightEnd)
						{
							if (pred(*pRightValue))
								break;
						}
					}
				}while (pRightValue != pRightEnd);
				auto pRightNode = right.m_pDynamic;
				right.m_pDynamic = nullptr;
				while (pRightNode != nullptr)
				{
					if (pred(pRightNode->value))
					{
						if (m_pTail == &m_sbo[list_match_set_sbo_count])
						{
							auto pLeftNode = m_pDynamic = pRightNode;
							++m_count;
							while (pRightNode->next != nullptr)
							{
								if (!pred(pRightNode->next->value))
								{
									auto pRightNext = pRightNode->next;
									pRightNode->next = pRightNext->next;
									delete pRightNext;
								}else
								{
									pRightNode = pRightNode->next;
									++m_count;
								}
							}
							m_pTail = &pRightNode->value;
							return;
						}
						*m_pTail++ = std::move(pRightNode->value);
						++m_count;
						auto pRightNext = pRightNode->next;
						delete pRightNode;
						pRightNode = pRightNext;
					}else
					{
						auto pRightNext = pRightNode->next;
						delete pRightNode;
						pRightNode = pRightNext;
					}
				}
				return;
			}
			auto pRightNode = get_node_ptr(right.m_pHead);
			right.m_pHead = right.m_pTail = &right.m_sbo[0];
			right.m_count = 0;
			right.m_pDynamic = nullptr;
			do
			{
				if (pred(pRightNode->value))
				{
					*m_pTail++ = std::move(pRightNode->value);
					++m_count;
					if (m_pTail == &m_sbo[list_match_set_sbo_count])
					{
						while (pRightNode->next != nullptr)
						{
							auto pRightNext = pRightNode->next;
							if (pred(pRightNext->value))
							{
								m_pDynamic = pRightNode = pRightNext;
								++m_count;
								while (pRightNode->next != nullptr)
								{
									pRightNext = pRightNode->next;
									if (pred(pRightNext->value))
									{
										pRightNode = pRightNext;
										++m_count;
									}else
									{
										pRightNode->next = pRightNext->next;
										delete pRightNext;
									}
								}
								m_pTail = &pRightNode->value;
								return;
							}else
							{
								delete pRightNode;
								pRightNode = pRightNext;
							}
						}
						m_pDynamic = nullptr;
						return;
					}
				}
				auto pRightNext = pRightNode->next;
				delete pRightNode;
				pRightNode = pRightNext;
			}while(pRightNode != nullptr);
			return;
		}else
		{
			node* pLeftNode;
			if (right.in_sbo(right.m_pHead))
			{
				auto pRightValue = right.m_pHead;
				auto pRightEnd = right.in_sbo(right.m_pTail)?right.m_pTail:&right.m_sbo[list_match_set_sbo_count];
				if (pRightValue == pRightEnd)
				{
					pLeftNode = right.m_pDynamic;
					while (true)
					{
						if (!pLeftNode)
							return;
						if (pred(pLeftNode->value))
							break;
						auto pOld = pLeftNode;
						pLeftNode = pLeftNode->next;
						delete pOld;
					};
					if (m_pTail == &m_sbo[list_match_set_sbo_count])
						m_pDynamic = pLeftNode;
					else
						get_node_ptr(m_pTail)->next = pLeftNode;
				}else
				{
					do
					{
						if (!pred(*pRightValue))
							++pRightValue;
						else
						{
							pLeftNode = new node{std::move(*pRightValue), nullptr};
							if (m_pTail == &m_sbo[list_match_set_sbo_count])
								m_pDynamic = pLeftNode;
							else
								get_node_ptr(m_pTail)->next = pLeftNode;
							++m_count;
							while (++pRightValue != pRightEnd)
							{
								if (pred(*pRightValue))
								{
									pLeftNode->next = new node{std::move(*pRightValue), nullptr};
									pLeftNode = pLeftNode->next;
									++m_count;
								}
							}
						}
					}while (pRightValue != pRightEnd);
					pLeftNode->next = right.m_pDynamic;
				}
			}else
			{
				auto pRightNode = get_node_ptr(right.m_pHead);
				while (!pred(pRightNode->value))
				{
					auto pRightNext = pRightNode->next;
					delete pRightNode;
					pRightNode = pRightNode->next;
					if (!pRightNode)
					{
						right.m_pDynamic = nullptr;
						right.m_pHead = right.m_pTail = &right.m_sbo[0];
						right.m_count = 0;
						return;
					}
				}
				pLeftNode = pRightNode;
				if (m_pTail == &m_sbo[list_match_set_sbo_count])
					m_pDynamic = pLeftNode;
				else
					get_node_ptr(m_pTail)->next = pLeftNode;
			}
			right.m_pDynamic = nullptr;
			right.m_pHead = right.m_pTail = &right.m_sbo[0];
			right.m_count = 0;
			while (pLeftNode->next != nullptr)
			{
				auto pLeftNext = pLeftNode->next;
				if (pred(pLeftNext->value))
				{
					pLeftNode = pLeftNext;
					++m_count;
				}else
				{
					pLeftNode->next = pLeftNext->next;
					delete pLeftNext;
				}
			}
			m_pTail = &pLeftNode->value;
		}
	}
	void clear()
	{
		this->destroy_nodes();
		m_pHead = m_pTail = &m_sbo[0];
		m_pDynamic = nullptr;
		m_count = 0;
	}
};

template <class ForwardIterator, std::size_t heap_match_set_sbo_count = match_set_sbo_count>
class sbo_set
{
	match_set_value<ForwardIterator> m_sbo[heap_match_set_sbo_count];
	std::size_t m_sbo_count = 0;
	std::set<match_set_value<ForwardIterator>> m_stDynamic;
	bool heap_find(const match_set_value<ForwardIterator>& value, std::size_t iFrom = 0) const
	{
		if (iFrom >= m_sbo_count)
			return false;
		if (m_sbo[iFrom].length < value.length)
			return false;
		if (m_sbo[iFrom].length == value.length)
			return true;
		auto iChild = iFrom * 2 + 1;
		if (heap_find(value, iChild))
			return true;
		return heap_find(value, ++iChild);
	}
	struct my_iterator
	{
		typedef match_set_value<ForwardIterator> value_type;
		typedef const value_type *pointer, &reference;
		typedef std::size_t size_type;
		typedef std::ptrdiff_t difference_type;
		typedef std::bidirectional_iterator_tag iterator_category;
	public:
		my_iterator() = default;
	private:
		const sbo_set *m_pSet;
		match_set_value<ForwardIterator>* m_pSboPtr;
		typename std::set<match_set_value<ForwardIterator>>::const_iterator m_itDynamic;
		friend class sbo_set<ForwardIterator, heap_match_set_sbo_count>;
		my_iterator(const sbo_set& my_set, std::size_t iSbo)
			:m_pSet(&my_set), m_pSboPtr(&m_pSet->m_sbo[iSbo]), m_itDynamic(m_pSet->m_stDynamic.cbegin()) {}
		my_iterator(const sbo_set& my_set, typename std::set<match_set_value<ForwardIterator>>::const_iterator itDynamic)
			:m_pSet(&my_set), m_pSboPtr(&m_pSet->m_sbo[m_pSboPtr->m_sbo_count]), m_itDynamic(itDynamic) {}
		//reference
	};
public:
	sbo_set() = default;
	sbo_set(const sbo_set&) = default;
	sbo_set& operator=(const sbo_set&) = default;
	sbo_set(sbo_set&& right):m_sbo_count(right.m_sbo_count), m_stDynamic(std::move(right.m_stDynamic))
	{
		std::move(&right.m_sbo[0], &right.m_sbo[right.m_sbo_count], m_sbo);
		right.m_sbo_count = 0;
	}
	sbo_set& operator=(sbo_set&& right)
	{
		if (this == &right)
			return *this;
		this->clear();
		m_sbo_count = right.m_sbo_count;
		m_stDynamic = std::move(right.m_stDynamic);
		std::move(&right.m_sbo[0], &right.m_sbo[right.m_sbo_count], m_sbo);
		right.m_sbo_count = 0;
		return *this;
	}
	void clear()
	{
		m_sbo_count = 0;
		m_stDynamic.clear();
	}
	bool emplace(const match_set_value<ForwardIterator>& val)
	{
		if (heap_find(val))
			return false;
		if (m_sbo_count == heap_match_set_sbo_count)
			return m_stDynamic.emplace(val).second;
		m_sbo[m_sbo_count++] = val;
		std::push_heap(&m_sbo[0], &m_sbo[m_sbo_count]);
		return true;
	}
	bool contains(const match_set_value<ForwardIterator>& val) const
	{
		return heap_find(val) || m_stDynamic.find(val) != m_stDynamic.end();
	}
	void merge(sbo_set& right)
	{
		auto iRight = right.m_sbo_count, iRightMove = right.m_sbo_count;
		while (m_sbo_count < heap_match_set_sbo_count)
		{
			if (iRightMove-- == 0)
			{
				++iRightMove;
				if (iRightMove != iRight)
				{
					std::move(&right.m_sbo[iRight], &right.m_sbo[right.m_sbo_count], &right.m_sbo[iRightMove]);
					right.m_sbo_count -= iRight - iRightMove;
					iRight = iRightMove;
				}
				auto it = right.m_stDynamic.rbegin();
				do
				{
					if (it == right.m_stDynamic.rend())
						return;
					auto l_it = it.base();
					auto it_erase = l_it;
					if (heap_find(*--it_erase) || m_stDynamic.find(*it_erase) != m_stDynamic.end())
						++it;
					else
					{
						m_sbo[m_sbo_count++] = std::move(*it_erase);
						m_stDynamic.erase(it_erase);
						it = std::make_reverse_iterator(l_it);
					}

				}while (m_sbo_count < heap_match_set_sbo_count);
				break;
			}else if (heap_find(right.m_sbo[iRightMove]))
			{
				if (auto rend = iRightMove + 1; rend != iRight)
				{
					std::move(&right.m_sbo[iRight], &right.m_sbo[right.m_sbo_count], &right.m_sbo[iRightMove + 1]);
					right.m_sbo_count -= iRight - iRightMove - 1;
				}
				iRight = iRightMove;
			}else
			{
				m_sbo[m_sbo_count++] = std::move(right.m_sbo[iRightMove]);
				std::push_heap(&m_sbo[0], &m_sbo[m_sbo_count]);
				if (!iRightMove)
				{
					std::move(&right.m_sbo[iRight], &right.m_sbo[right.m_sbo_count], &right.m_sbo[iRightMove]);
					right.m_sbo_count -= iRight - iRightMove;
					iRight = 0;
				}
			}
		}
		while (iRightMove-- != 0)
		{
			if (heap_find(right.m_sbo[iRightMove]) || !m_stDynamic.emplace(std::move(right.m_sbo[iRightMove])).second)
			{
				if (auto rend = iRightMove + 1; rend != iRight)
				{
					std::move(&right.m_sbo[iRight], &right.m_sbo[right.m_sbo_count], &right.m_sbo[rend]);
					right.m_sbo_count -= iRight - rend;
				}
				iRight = iRightMove;
			};
		}
		if (++iRightMove != iRight)
		{
			std::move(&right.m_sbo[iRight], &right.m_sbo[right.m_sbo_count], &right.m_sbo[iRightMove]);
			right.m_sbo_count -= iRight - iRightMove;
		}
		m_stDynamic.merge(right.m_stDynamic);
	}
	const match_set_value<ForwardIterator>& max_element() const
	{
		if (m_stDynamic.empty())
			return m_sbo[0];
		return std::max(m_sbo[0], *m_stDynamic.rbegin());
	}
};

template <class ForwardIterator>
class match_set
{
public:
	typedef match_set_value<ForwardIterator> value_type;
	typedef std::list<match_set_value<ForwardIterator>> list_type;
private:
	sbo_list<ForwardIterator> m_lstChildren;
	sbo_set<ForwardIterator> m_set;
public:
	match_set() = default;
	match_set(ForwardIterator end_iterator, std::size_t length)
	{
		auto val = match_set_value<ForwardIterator>{length, end_iterator};
		m_set.emplace(val);
		m_lstChildren.emplace_back(std::move(val));
	}
	value_type borrow_match()
	{
		return m_lstChildren.pop_front();
	}
	void add_matches(match_set&& matches)
	{
		m_set.merge(matches.m_set);
		m_lstChildren.append_if(std::move(matches.m_lstChildren), [&matches](const match_set_value<ForwardIterator>& val) -> bool {return !matches.m_set.contains(val);});
	}
	void add_matches(const value_type& borrowed, match_set&& matches)
	{
		for (auto& v:matches.m_lstChildren)
			v.length += borrowed.length;
		m_lstChildren.append_if(std::move(matches.m_lstChildren), [this](const match_set_value<ForwardIterator>& val) -> bool {return m_set.emplace(val);});
	}
	bool empty() const
	{
		return m_lstChildren.empty();
	}
	ForwardIterator first_match() const
	{
		return m_lstChildren.front().end;
	}
	ForwardIterator greedy_match() const
	{
		return m_set.max_element().end;
	}
};

struct EmptyRule
{
	template <class ForwardIteratorBegin, class ForwardIteratorEnd>
	auto get_match(ForwardIteratorBegin begin, ForwardIteratorEnd) const -> match_set<ForwardIteratorBegin>
	{
		return match_set<ForwardIteratorBegin>(begin, 0);
	}
};

#define ABNF_IMPL_STRING_TPARAM_MACRO_1(str, i) (std::size(str) > (i) ? str[(i)] : std::decay_t<decltype(str[0])>())
#define ABNF_IMPL_STRING_TPARAM_MACRO_2(str, i) ABNF_IMPL_STRING_TPARAM_MACRO_1(str, (i)), ABNF_IMPL_STRING_TPARAM_MACRO_1(str, (i) + 1)
#define ABNF_IMPL_STRING_TPARAM_MACRO_4(str, i) ABNF_IMPL_STRING_TPARAM_MACRO_2(str, (i)), ABNF_IMPL_STRING_TPARAM_MACRO_2(str, (i) + 2)
#define ABNF_IMPL_STRING_TPARAM_MACRO_8(str, i) ABNF_IMPL_STRING_TPARAM_MACRO_4(str, (i)), ABNF_IMPL_STRING_TPARAM_MACRO_4(str, (i) + 4)
#define ABNF_IMPL_STRING_TPARAM_MACRO_16(str, i) ABNF_IMPL_STRING_TPARAM_MACRO_8(str, (i)), ABNF_IMPL_STRING_TPARAM_MACRO_8(str, (i) + 8)
#define ABNF_IMPL_STRING_TPARAM_MACRO_32(str, i) ABNF_IMPL_STRING_TPARAM_MACRO_16(str, (i)), ABNF_IMPL_STRING_TPARAM_MACRO_16(str, (i) + 16)
#define ABNF_IMPL_STRING_TPARAM_MACRO_64(str, i) ABNF_IMPL_STRING_TPARAM_MACRO_32(str, (i)), ABNF_IMPL_STRING_TPARAM_MACRO_32(str, (i) + 32)

#define ABNF_LITERAL_STRING_2(str) std::size(str), ABNF_IMPL_STRING_TPARAM_MACRO_2(str, 0)
#define ABNF_LITERAL_STRING_4(str) std::size(str), ABNF_IMPL_STRING_TPARAM_MACRO_4(str, 0)
#define ABNF_LITERAL_STRING_8(str) std::size(str), ABNF_IMPL_STRING_TPARAM_MACRO_8(str, 0)
#define ABNF_LITERAL_STRING_16(str) std::size(str), ABNF_IMPL_STRING_TPARAM_MACRO_16(str, 0)
#define ABNF_LITERAL_STRING_32(str) std::size(str), ABNF_IMPL_STRING_TPARAM_MACRO_32(str, 0)
#define ABNF_LITERAL_STRING_64(str) std::size(str), ABNF_IMPL_STRING_TPARAM_MACRO_64(str, 0)
#define ABNF_LITERAL_STRING ABNF_LITERAL_STRING_32
#define ABNF_STATIC_STRING(str) ABNF_LITERAL_STRING(str)
#define ABNF_DYNAMIC_STRING std::size_t(-1)

template <class TraitsType, auto ... chars>
class impl_Rule_StaticString_storage_base
{
protected:
	typedef std::decay_t<std::common_type_t<decltype(chars)...>> char_type;
	static_assert(supported_char_type_v<char_type>, "Unsupported string");
	static constexpr const char_type m_pData[sizeof...(chars)] = {chars...};
protected:
	constexpr std::basic_string_view<char_type, TraitsType> get_string() const noexcept
	{
		return std::basic_string_view<char_type, TraitsType>(m_pData, sizeof...(chars) - 1);
	}
};

//template <class TraitsType, auto ... chars>
//constexpr std::add_const_t<std::common_type_t<decltype(chars)...>> impl_Rule_StaticString_storage_base<TraitsType, chars...>::m_pData[sizeof...(chars)];

template <class TraitsType, class IndexSequence> struct impl_Rule_StaticString_storage_proxy_helper {};

template <class TraitsType, std::size_t ... Ind>
struct impl_Rule_StaticString_storage_proxy_helper<TraitsType, std::index_sequence<Ind...>>
{
	template <auto ... chars>
	using stub_type = impl_Rule_StaticString_storage_base<TraitsType, std::get<Ind>(std::make_tuple(chars...))...>;
};

template <class TraitsType, class IndexSequence, auto ... chars>
using impl_Rule_StaticString_storage_proxy = typename impl_Rule_StaticString_storage_proxy_helper<TraitsType, IndexSequence>::template stub_type<chars...>;

template <class TraitsType>
struct impl_Rule_DynamicString_storage_base
{
protected:
	typedef typename TraitsType::char_type char_type;
	static_assert(supported_char_type_v<char_type>, "Unsupported string");
public:
	impl_Rule_DynamicString_storage_base() = default;
	impl_Rule_DynamicString_storage_base(const char_type* pString, std::size_t cbString):m_str(pString, cbString) {}
	template <std::size_t N>
	impl_Rule_DynamicString_storage_base(const char_type (&pString)[N]):m_str(pString, N - 1) {}
	impl_Rule_DynamicString_storage_base(const char_type* pszString):m_str(pszString) {}
	impl_Rule_DynamicString_storage_base(const std::basic_string<char_type, TraitsType>& str):m_str(str) {}
	impl_Rule_DynamicString_storage_base(std::basic_string<char_type, TraitsType>&& str):m_str(std::move(str)) {}
	impl_Rule_DynamicString_storage_base(const char* pString, std::size_t cbString)
		:m_str(u8_decode_string<TraitsType>(pString, cbString)) {}
	template <std::size_t N>
	impl_Rule_DynamicString_storage_base(const char (&pString)[N]):m_str(u8_decode_string<TraitsType>(pString)) {}
	impl_Rule_DynamicString_storage_base(const char* pszString):m_str(u8_decode_string<TraitsType>(pszString)) {}
	template <class TraitType, class AllocatorType>
	impl_Rule_DynamicString_storage_base(const std::basic_string<char, TraitType, AllocatorType>& str)
		:m_str(u8_decode_string<TraitsType>(str)) {}
	template <class TraitType>
	impl_Rule_DynamicString_storage_base(const std::basic_string_view<char, TraitType>& str)
		:m_str(u8_decode_string<TraitsType>(str)) {}
protected:
	const std::basic_string<char_type, TraitsType>& get_string() const noexcept
	{
		return m_str;
	}
private:
	std::basic_string<char_type, TraitsType> m_str;
};

template <class TraitsType, class BaseType>
struct impl_Rule_String_base:BaseType
{
	using BaseType::BaseType;
	template <class ForwardIteratorBegin, class ForwardIteratorEnd>
	auto get_match(ForwardIteratorBegin begin, ForwardIteratorEnd end) const
	{
		using char_type = typename BaseType::char_type;
		std::size_t i = 0;
		using std::size;
		auto string_length = size(this->get_string());
		auto it = begin;
		while (i < string_length)
		{
			if (it == end || cast_overflows<char_type>(*it) || !TraitsType::eq(char_type(*it), this->get_string()[i++]))
				return match_set<ForwardIteratorBegin>();
			++it;
		}
		return match_set<ForwardIteratorBegin>(it, string_length);
	}
};

template <class TraitsType, std::size_t N>
struct impl_Rule_String
{
	template <auto ... chars>
	using impl_type = impl_Rule_String_base<TraitsType, impl_Rule_StaticString_storage_proxy<TraitsType, std::make_index_sequence<N>, chars...>>;
};

template <class TraitsType>
struct impl_Rule_String<TraitsType, std::size_t(-1)>
{
	template <auto ...>
	using impl_type = impl_Rule_String_base<TraitsType, impl_Rule_DynamicString_storage_base<TraitsType>>;
};

//Usage: IString<ABNF_STATIC_STRING("stringvalue")>
//   or: IString<ABNF_DYNAMIC_STRING>
template <std::size_t N, auto ... chars> //Case-insensitive string, RFC-5234
using IString = typename impl_Rule_String<ci_char_traits<std::decay_t<std::common_type_t<decltype(chars)...>>>, N>::template impl_type<chars...>;
//Usage: Likewise
template <std::size_t N, auto ... chars>//Case-sensitive string, RFC-7405
using SString = typename impl_Rule_String<std::char_traits<std::decay_t<std::common_type_t<decltype(chars)...>>>, N>::template impl_type<chars...>;
//Default: case-insensitive (RFC-5234 and RFC-7405)
template <std::size_t N, auto ... chars>//Case-insensitive string, RFC-7405
using String = IString<N, chars...>;

template <auto code>
struct Code
{
	static_assert(supported_char_type_v<decltype(code)> && !cast_overflows<ucp_t>(code), "Unsupported character type");
	template <class ForwardIteratorBegin, class ForwardIteratorEnd>
	auto get_match(ForwardIteratorBegin begin, ForwardIteratorEnd end) const
	{
		using char_type = decltype(code);
		if (begin != end && !cast_overflows<ucp_t>(*begin) && ucp_t(*begin) == ucp_t(code))
			return match_set<ForwardIteratorBegin>(++begin, 1);
		return match_set<ForwardIteratorBegin>();
	}
};

template <auto min, auto max /*inclusive*/>
struct CodeRange
{
	static_assert(min <= max, "min must be less or equal than max");
	static_assert(std::is_same_v<decltype(min), decltype(max)> && supported_char_type_v<decltype(min)>
		&& !cast_overflows<ucp_t>(min) && !cast_overflows<ucp_t>(max), "Unsupported character type");
	template <class ForwardIteratorBegin, class ForwardIteratorEnd>
	auto get_match(ForwardIteratorBegin begin, ForwardIteratorEnd end) const
	{
		if (begin != end && ucp_t(*begin) >= ucp_t(min) && ucp_t(*begin) <= ucp_t(max))
			return match_set<ForwardIteratorBegin>(++begin, 1);
		return match_set<ForwardIteratorBegin>();
	}
};

template <auto code, bool is_ascii = (code <= 0x7f)>
struct ImplCharCI
{
	static_assert(supported_char_type_v<decltype(code)> && !cast_overflows<ucp_t>(code), "Unsupported character type");
	static const ucp_t uc_code;
	template <class ForwardIteratorBegin, class ForwardIteratorEnd>
	auto get_match(ForwardIteratorBegin begin, ForwardIteratorEnd end) const
	{
		if (begin != end && !cast_overflows<ucp_t>(*begin) && ucp_toupper(ucp_t(*begin)) == uc_code)
			return match_set<ForwardIteratorBegin>(++begin, 1);
		return match_set<ForwardIteratorBegin>();
	}
};

template <auto code, bool is_ascii>
const ucp_t ImplCharCI<code, is_ascii>::uc_code = ucp_toupper(ucp_t(code));

template <auto code>
struct ImplCharCI<code, true>
{
	static_assert(supported_char_type_v<decltype(code)> && !cast_overflows<char>(code), "Unsupported character type");
	static constexpr char uc_code = impl_to_upper(char(code));
	template <class ForwardIteratorBegin, class ForwardIteratorEnd>
	constexpr auto get_match(ForwardIteratorBegin begin, ForwardIteratorEnd end) const noexcept
	{
		if (begin != end && !cast_overflows<char>(*begin) && impl_to_upper(char(*begin)) == uc_code)
			return match_set<ForwardIteratorBegin>(++begin, 1);
		return match_set<ForwardIteratorBegin>();
	}
};

template <auto code>
struct CharCI //Case-Independent Character
	:ImplCharCI<code> {};

template <auto Predicate>
struct Predicated
{
	template <class ForwardIteratorBegin, class ForwardIteratorEnd>
	constexpr auto get_match(ForwardIteratorBegin begin, ForwardIteratorEnd end) const noexcept
	{
            //using parameter_type = typename std::tuple_element<i, std::tuple<Params...>>::type;
		using CharType = char;//typename function_traits<decltype(Predicate)>::template parameter_type<0>;
		if (begin != end && !cast_overflows<CharType>(*begin) && Predicate(CharType(*begin)))
			return match_set<ForwardIteratorBegin>(++begin, 1);
		return match_set<ForwardIteratorBegin>();
	}
};


struct IdPredicate
{
	template <class ForwardIteratorBegin, class ForwardIteratorEnd>
	auto get_match(ForwardIteratorBegin begin, ForwardIteratorEnd end) const noexcept
	{
		if (begin != end && is_id(begin, end))
			return match_set<ForwardIteratorBegin>(++begin, 1);
		return match_set<ForwardIteratorBegin>();
	}
};

template <class ... RULES> struct Concat;

template <class ... RULES> struct Alternatives;

template <class RULE, std::size_t min_length = 0, std::size_t max_length = std::numeric_limits<std::size_t>::max()>
struct Repeat;

template <class Tpl, class = void>
struct ImplConcat {};

template <class ... RULES>
struct ImplConcat<rule_storage<RULES...>>:private rule_storage<RULES...>
{
	template <class ForwardIteratorBegin, class ForwardIteratorEnd>
	auto get_match(ForwardIteratorBegin begin, ForwardIteratorEnd end) const
		-> match_set<ForwardIteratorBegin>
	{
		return this->get_submatch(begin, end);
	}
	ImplConcat() noexcept(std::conjunction_v<std::is_nothrow_default_constructible<RULES>...>) = default;
	template <class ... Rules>
	ImplConcat(Rules&& ... rules) noexcept(std::conjunction_v<std::is_nothrow_constructible<RULES, Rules&&>...>) :rule_storage<RULES...>(std::forward<Rules>(rules)...) {}
private:
	template <std::size_t I = 0, class ForwardIteratorBegin, class ForwardIteratorEnd>
	auto get_submatch(ForwardIteratorBegin begin, ForwardIteratorEnd) const noexcept
		-> std::enable_if_t<(I >= sizeof...(RULES)), match_set<ForwardIteratorBegin>>
	{
		return match_set<ForwardIteratorBegin>();
	}
	template <std::size_t I = 0, class ForwardIteratorBegin, class ForwardIteratorEnd>
	auto get_submatch(ForwardIteratorBegin begin, ForwardIteratorEnd end) const
		-> std::enable_if_t<(I + 1 == sizeof...(RULES)), match_set<ForwardIteratorBegin>>
	{
		return this->template get_rule<I>().get_match(begin, end);
	}
	template <std::size_t I = 0, class ForwardIteratorBegin, class ForwardIteratorEnd>
	auto get_submatch(ForwardIteratorBegin begin, ForwardIteratorEnd end) const
		-> std::enable_if_t<(I + 1 < sizeof...(RULES)), match_set<ForwardIteratorBegin>>
	{
                while (begin != end && (*begin == ' ' /*|| *begin == '\n' || *begin == '\t'*/))
                {
                    ++begin;
                    continue;
                }
		match_set<ForwardIteratorBegin> cur_match = this->template get_rule<I>().get_match(begin, end), result;
		while (!cur_match.empty())
		{
			auto val = cur_match.borrow_match();
			auto next_match = get_submatch<I + 1>(val.end, end);
			result.add_matches(val, std::move(next_match));
		}
		return result;
	}
};

template <class T> struct impl_cat_elem_to_rule_storage {typedef rule_storage<T> type;};
template <class ... CatRules> struct impl_cat_elem_to_rule_storage<Concat<CatRules...>> {typedef rule_storage_concat_t<typename impl_cat_elem_to_rule_storage<CatRules>::type...> type;};

template <class ... RULES>
struct Concat:ImplConcat<rule_storage_concat_t<typename impl_cat_elem_to_rule_storage<RULES>::type...>>
{
    using mybase = ImplConcat<rule_storage_concat_t<typename impl_cat_elem_to_rule_storage<RULES>::type...>>;
	Concat() = default;
	template <class ... Rules>
	Concat(Rules&& ... rules) noexcept(std::conjunction_v<std::is_nothrow_constructible<RULES, Rules&&>...>) :mybase(std::forward<Rules>(rules)...) {}
};

template <>
struct Concat<>:EmptyRule {};

template <class RULE_0>
struct Concat<RULE_0>:RULE_0
{
	Concat() = default;
	template <class Rule_0>
	Concat(Rule_0&& rule_0) noexcept(std::is_nothrow_constructible_v<RULE_0, Rule_0&&>) :RULE_0(std::forward<Rule_0>(rule_0)) {}
};

template <class ... Rules>
struct rule_invariant_length;

template <class T>
struct rule_invariant_length<T>:std::integral_constant<std::size_t, std::size_t(-1)> {};

template <>
struct rule_invariant_length<EmptyRule>:std::integral_constant<std::size_t, 0> {};

template <class TraitsType, auto ... chars>
struct rule_invariant_length<impl_Rule_String_base<TraitsType, impl_Rule_StaticString_storage_base<TraitsType, chars...>>>:std::integral_constant<std::size_t, sizeof...(chars)> {};

template <auto code>
struct rule_invariant_length<Code<code>>:std::integral_constant<std::size_t, std::size_t(1)> {};

template <auto min, auto max>
struct rule_invariant_length<CodeRange<min, max>>:std::integral_constant<std::size_t, std::size_t(1)> {};

template <auto code>
struct rule_invariant_length<CharCI<code>>:std::integral_constant<std::size_t, std::size_t(1)> {};

template <class ... Rules>
struct rule_invariant_length:
	std::conditional<
		std::conjunction_v<std::bool_constant<rule_invariant_length<rule_storage_element_t<0, rule_storage<Rules...>>>::value == rule_invariant_length<Rules>::value>...>,
		typename rule_invariant_length<rule_storage_element_t<0, rule_storage<Rules...>>>::type, std::integral_constant<std::size_t, std::size_t(-1)>>::type {};

#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable: 4307)
#endif //_MSC_VER

template <class ... Rules>
struct rule_invariant_length<Concat<Rules...>>:std::integral_constant<std::size_t, rule_invariant_length<Rules...>::value == std::size_t(-1)?
	std::size_t(-1):rule_invariant_length<Rules...>::value * sizeof...(Rules)> {};

template <class ... Rules>
struct rule_invariant_length<Alternatives<Rules...>>:rule_invariant_length<Rules...>::type {};

template <class Rule, std::size_t length>
struct rule_invariant_length<Repeat<Rule, length, length>>:
	std::integral_constant<std::size_t, rule_invariant_length<Rule>::value == std::size_t(-1)?std::size_t(-1):rule_invariant_length<Rule>::value * length> {};

#ifdef _MSC_VER
#pragma warning (pop)
#endif //_MSC_VER

template <class Tpl, class = void>
struct ImplAlternatives {};

template <class...RULES>
struct ImplAlternatives<rule_storage<RULES...>, std::enable_if_t<rule_invariant_length<RULES...>::value == std::size_t(-1)>>:private rule_storage<RULES...>
{
	template <class ForwardIteratorBegin, class ForwardIteratorEnd>
	auto get_match(ForwardIteratorBegin begin, ForwardIteratorEnd end) const -> match_set<ForwardIteratorBegin>
	{
		return this->get_submatch(begin, end);
	}
	ImplAlternatives() noexcept(std::conjunction_v<std::is_nothrow_default_constructible<RULES>...>) = default;
	template <class ... Rules>
	ImplAlternatives(Rules&& ... rules) noexcept(std::conjunction_v<std::is_nothrow_constructible<RULES, Rules&&>...>):rule_storage<RULES...>(std::forward<Rules>(rules)...) {}
private:
	template <std::size_t I = 0, class ForwardIteratorBegin, class ForwardIteratorEnd>
	auto get_submatch(ForwardIteratorBegin, ForwardIteratorEnd) const -> std::enable_if_t<(I >= sizeof...(RULES)), match_set<ForwardIteratorBegin>>
	{
		return match_set<ForwardIteratorBegin>();
	}
	template <std::size_t I = 0, class ForwardIteratorBegin, class ForwardIteratorEnd>
	auto get_submatch(ForwardIteratorBegin begin, ForwardIteratorEnd end) const -> std::enable_if_t<(I < sizeof...(RULES)), match_set<ForwardIteratorBegin>>
	{
		auto cur_set = this->template get_rule<I>().get_match(begin, end);
		cur_set.add_matches(this->get_submatch<I + 1>(begin, end));
		return cur_set;
	}
};

template <class...RULES>
struct ImplAlternatives<rule_storage<RULES...>, std::enable_if_t<rule_invariant_length<RULES...>::value != std::size_t(-1)>>:private rule_storage<RULES...>
{
	template <class ForwardIteratorBegin, class ForwardIteratorEnd>
	auto get_match(ForwardIteratorBegin begin, ForwardIteratorEnd end) const
		-> match_set<ForwardIteratorBegin>
	{
		return this->get_submatch(begin, end);
	}
	ImplAlternatives() noexcept(std::conjunction_v<std::is_nothrow_default_constructible<RULES>...>) = default;
	template <class ... Rules>
	ImplAlternatives(Rules&& ... rules) noexcept(std::conjunction_v<std::is_nothrow_constructible<RULES, Rules&&>...>) :rule_storage<RULES...>(std::forward<Rules>(rules)...) {}
private:
	template <std::size_t I = 0, class ForwardIteratorBegin, class ForwardIteratorEnd>
	auto get_submatch(ForwardIteratorBegin, ForwardIteratorEnd) const -> std::enable_if_t<(I >= sizeof...(RULES)), match_set<ForwardIteratorBegin>>
	{
		return match_set<ForwardIteratorBegin>();
	}
	template <std::size_t I = 0, class ForwardIteratorBegin, class ForwardIteratorEnd>
	auto get_submatch(ForwardIteratorBegin begin, ForwardIteratorEnd end) const -> std::enable_if_t<(I < sizeof...(RULES)), match_set<ForwardIteratorBegin>>
	{
		auto cur_set = this->template get_rule<I>().get_match(begin, end);
		if (cur_set.empty())
			cur_set.add_matches(this->get_submatch<I + 1>(begin, end));
		return cur_set;
	}
};

template <class T> struct impl_alt_elem_to_rule_storage {typedef rule_storage<T> type;};
template <class ... Alts> struct impl_alt_elem_to_rule_storage<Alternatives<Alts...>> {typedef rule_storage_concat_t<typename impl_alt_elem_to_rule_storage<Alts>::type...> type;};

template <class ... RULES>
struct Alternatives: ImplAlternatives<rule_storage_concat_t<typename impl_alt_elem_to_rule_storage<RULES>::type...>>
{
    using andreychusovisdabest = ImplAlternatives<rule_storage_concat_t<typename impl_alt_elem_to_rule_storage<RULES>::type...>>;
	Alternatives() = default;
	template <class ... Rules>
	Alternatives(Rules&& ... rules) noexcept(std::conjunction_v<std::is_nothrow_constructible<RULES, Rules&&>...>) :andreychusovisdabest(std::forward<Rules>(rules)...) {}
};

template <class RULE_0>
struct Alternatives<RULE_0>:RULE_0
{
	Alternatives() = default;
	template <class Rule_0>
	Alternatives(Rule_0&& rule_0) noexcept(std::is_nothrow_constructible_v<RULE_0, Rule_0&&>) :RULE_0(std::forward<Rule_0>(rule_0)) {}
};

template <>
struct Alternatives<>:EmptyRule {};

template <class ... RULES>
using Alternatives_FMW = Alternatives<RULES...>; //First-match-wins

template <class RULE, std::size_t min_length, std::size_t max_length, class = void>
struct ImplRepeat:private RULE //TODO: specialize for RULE of invariant length
{
	static_assert(max_length > 0);
	template <class ForwardIteratorBegin, class ForwardIteratorEnd>
	auto get_match(ForwardIteratorBegin begin, ForwardIteratorEnd end) const -> match_set<ForwardIteratorBegin>
	{
		std::size_t cRepeats = 0;
		match_set<ForwardIteratorBegin> result, last_match = match_set<ForwardIteratorBegin>(begin, 0);
		if (!min_length)
			result = match_set<ForwardIteratorBegin>(begin, 0);
		while (cRepeats++ < max_length)
		{
			match_set<ForwardIteratorBegin> next_match;
			while (!last_match.empty())
			{
				auto borrowed = last_match.borrow_match();
				auto match = this->RULE::get_match(borrowed.end, end);
				if (!match.empty())
					next_match.add_matches(borrowed, std::move(match));
			}
			if (next_match.empty())
				return result;
			last_match = next_match;
			if (cRepeats >= min_length)
				result.add_matches(std::move(next_match));
		}
		return result;
	}

	ImplRepeat() = default;
	template <class Rule>
	ImplRepeat(Rule&& rule):RULE(std::forward<Rule>(rule)) {}
};

template <class RULE, std::size_t min_length, std::size_t max_length>
struct ImplRepeat<RULE, min_length, max_length, std::enable_if_t<rule_invariant_length<RULE>::value != std::size_t(-1)>>:private RULE //TODO: specialize for RULE of invariant length
{
	static_assert(max_length > 0);
	template <class ForwardIteratorBegin, class ForwardIteratorEnd>
	auto get_match(ForwardIteratorBegin begin, ForwardIteratorEnd end) const -> match_set<ForwardIteratorBegin>
	{
		std::size_t cRepeats = 0;
		auto it = begin;
		while (cRepeats < min_length)
		{
			auto match = this->RULE::get_match(it, end);
			if (match.empty())
				return match_set<ForwardIteratorBegin>();
			it = match.first_match();
			++cRepeats;
		}
		auto result = match_set<ForwardIteratorBegin>(it, cRepeats * rule_invariant_length<RULE>::value);
		while (cRepeats < max_length)
		{
			auto match = this->RULE::get_match(it, end);
			if (match.empty())
				return result;
			it = match.first_match();
			result.add_matches(match_set<ForwardIteratorBegin>(it, cRepeats * rule_invariant_length<RULE>::value));
			++cRepeats;
		}
		return result;
	}

	ImplRepeat() = default;
	template <class Rule>
	ImplRepeat(Rule&& rule):RULE(std::forward<Rule>(rule)) {}
};

template <class RULE>
struct ImplRepeat<RULE, 0, 0>:EmptyRule {};

template <class RULE, std::size_t min_length, std::size_t max_length>
struct Repeat:ImplRepeat<RULE, min_length, max_length>
{
	Repeat() = default;
	template <class Rule>
	Repeat(Rule&& rule) noexcept(std::is_nothrow_constructible_v<RULE, Rule&&>) :ImplRepeat<RULE, min_length, max_length>(std::forward<Rule>(rule)) {}
};

template <class RULE>
using Optional = Alternatives<EmptyRule, RULE>;

using Rule_ALPHA = Predicated<is_alpha>;
using Rule_BIT = Predicated<is_bit>;
using Rule_CHAR = Predicated<is_char>;
using Rule_CR = Predicated<is_cr>;
using Rule_LF = Predicated<is_lf>;
using Rule_CRLF = Concat<Rule_CR, Rule_LF>;
using Rule_CTL = Predicated<is_ctl>;
using Rule_DIGIT = Predicated<is_digit>;
using Rule_DQUOTE = Predicated<is_dquote>;
using Rule_HEXDIG = Predicated<is_hexdig>;
using Rule_HTAB = Predicated<is_htab>;
using Rule_OCTET = Predicated<is_octet>;
using Rule_SP = Predicated<is_sp>;
using Rule_VCHAR = Predicated<is_vchar>;
using Rule_WSP = Predicated<is_wsp>;
using Rule_LWSP = Repeat<Concat<Alternatives<Rule_WSP, Rule_CRLF>, Rule_WSP>>;
using Rule_token = abnf::Repeat<Rule_CHAR, 1>;
using Rule_comment = Concat<CharCI<';'>, Repeat<Alternatives<Rule_WSP, Rule_VCHAR>>, Rule_CRLF>;
using Rule_c_nl = Alternatives<Rule_comment, Rule_CRLF>;
using Rule_c_wsp = Alternatives<Rule_WSP, Concat<Rule_c_nl, Rule_WSP>>;
// RFC-5234 definition of char-val:
//using Rule_char_val = Concat<
//	Rule_DQUOTE,
//	Repeat<
//		Alternatives<
//			CodeRange<0x20, 0x21>,
//			CodeRange<0x23, 0x7E>
//		>
//	>,
//	Rule_DQUOTE
//>;
// RFC-7405 definition of char-val:
using Rule_quoted_string = Concat<
	Rule_DQUOTE,
	Repeat<
		Alternatives<
			CodeRange<0x20, 0x21>,
			CodeRange<0x23, 0x7E>
		>
	>,
	Rule_DQUOTE
>;
using Rule_case_sensitive_string = Concat<String<ABNF_STATIC_STRING("%s")>, Rule_quoted_string>;
using Rule_case_insensitive_string = Concat<Optional<String<ABNF_STATIC_STRING("%i")>>, Rule_quoted_string>;
using Rule_char_val = Alternatives<Rule_case_insensitive_string, Rule_case_sensitive_string>;

using Rule_bin_val = Concat<
	CharCI<'b'>,
	Repeat<Rule_BIT, 1>,
	Optional<
		Alternatives<
			Repeat<
				Concat<
					CharCI<'.'>,
					Repeat<Rule_BIT, 1>
				>,
			1>,
			Concat<
				CharCI<'-'>,
				Repeat<Rule_BIT, 1>
			>
		>
	>
>;
using Rule_dec_val = Concat<
	CharCI<'d'>,
	Repeat<Rule_DIGIT, 1>,
	Optional<
		Alternatives<
			Repeat<
				Concat<
					CharCI<'.'>,
					Repeat<Rule_DIGIT, 1>
				>,
			1>,
			Concat<
				CharCI<'-'>,
				Repeat<Rule_DIGIT, 1>
			>
		>
	>
>;
using Rule_hex_val = Concat<
	CharCI<'x'>,
	Repeat<Rule_HEXDIG, 1>,
	Optional<
		Alternatives<
			Repeat<
				Concat<
					CharCI<'.'>,
					Repeat<Rule_HEXDIG, 1>
				>,
			1>,
			Concat<
				CharCI<'-'>,
				Repeat<Rule_HEXDIG, 1>
			>
		>
	>
>;
using Rule_num_val = Concat<CharCI<'%'>, Alternatives<Rule_bin_val, Rule_dec_val, Rule_hex_val>>;

template <class Rule, class ForwardIteratorBegin, class ForwardIteratorEnd>
[[nodiscard]] auto get_match(Rule&& rule, ForwardIteratorBegin begin, ForwardIteratorEnd end)
	-> std::enable_if_t<
		std::is_base_of_v<std::forward_iterator_tag, typename std::iterator_traits<ForwardIteratorBegin>::iterator_category> &&
		std::is_base_of_v<std::forward_iterator_tag, typename std::iterator_traits<ForwardIteratorEnd>::iterator_category>,
		std::pair<bool, ForwardIteratorBegin>
	>
{
	auto matches = std::forward<Rule&&>(rule).get_match(begin, end);
	if (matches.empty())
		return std::make_pair(false, begin);
	return std::make_pair(true, matches.first_match());
}

template <class Rule, class Container>
[[nodiscard]] auto get_match(Rule&& rule, const Container& container)
	-> decltype(get_match(std::declval<Rule&&>(), std::begin(std::declval<const Container&>()), std::end(std::declval<const Container&>())))
{
	return get_match(std::forward<Rule&&>(rule), std::begin(container), std::end(container));
}

template <class Rule, class ForwardIteratorBegin, class ForwardIteratorEnd, class ProcessMatchedCharacter>
[[nodiscard]] auto get_match(Rule&& rule, ForwardIteratorBegin begin, ForwardIteratorEnd end, ProcessMatchedCharacter&& process_char)
	-> decltype(get_match(std::declval<Rule&&>(), ForwardIteratorBegin(), ForwardIteratorEnd(), std::declval<ProcessMatchedCharacter&&>()))
{
	auto pr = get_match(std::forward<Rule>(rule), begin, end);
	if (pr.first)
		for (auto it = begin; it != pr.second; ++it)
			process_char(*it);
	return pr;
}

template <class Rule, class Container, class ProcessMatchedCharacter>
[[nodiscard]] auto get_match(Rule&& rule, const Container& container, ProcessMatchedCharacter&& process_char)
	-> decltype(get_match(std::declval<Rule&&>(), std::begin(std::declval<const Container&>()), std::end(std::declval<const Container&>()),
		std::declval<ProcessMatchedCharacter&&>()))
{
	return get_match(std::forward<Rule>(rule), std::begin(container), std::end(container), std::forward<ProcessMatchedCharacter>(process_char));
}

template <class Rule, class ForwardIteratorBegin, class ForwardIteratorEnd>
[[nodiscard]] auto matches(Rule&& rule, ForwardIteratorBegin begin, ForwardIteratorEnd end)
	-> std::enable_if_t<
		std::is_base_of_v<std::forward_iterator_tag, typename std::iterator_traits<ForwardIteratorBegin>::iterator_category> &&
		std::is_base_of_v<std::forward_iterator_tag, typename std::iterator_traits<ForwardIteratorEnd>::iterator_category>,
		bool
	>
{
	auto matches = std::forward<Rule&&>(rule).get_match(begin, end);
	if (matches.empty())
		return false;
	return matches.greedy_match() == end;
}

template <class Rule, class Container>
[[nodiscard]] auto matches(Rule&& rule, const Container& container)
	-> decltype(matches(std::declval<Rule&&>(), std::begin(std::declval<const Container&>()), std::end(std::declval<const Container&>())))
{
	return matches(std::forward<Rule&&>(rule), std::begin(container), std::end(container));
}

template <class Rule, class ForwardIteratorBegin, class ForwardIteratorEnd>
[[nodiscard]] auto get_greedy_match(Rule&& rule, ForwardIteratorBegin begin, ForwardIteratorEnd end)
	-> std::enable_if_t<
		std::is_base_of_v<std::forward_iterator_tag, typename std::iterator_traits<ForwardIteratorBegin>::iterator_category> &&
		std::is_base_of_v<std::forward_iterator_tag, typename std::iterator_traits<ForwardIteratorEnd>::iterator_category>,
		std::pair<bool, ForwardIteratorBegin>
	>
{
	auto matches = std::forward<Rule&&>(rule).get_match(begin, end);
	if (matches.empty())
		return std::make_pair(false, begin);
	return std::make_pair(true, matches.greedy_match());
}

template <class Rule, class Container>
[[nodiscard]] auto get_greedy_match(Rule&& rule, const Container& container)
	-> decltype(get_greedy_match(std::declval<Rule&&>(), std::begin(std::declval<const Container&>()), std::end(std::declval<const Container&>())))
{
	return get_greedy_match(std::forward<Rule&&>(rule), std::begin(container), std::end(container));
}

template <class Rule, class ForwardIteratorBegin, class ForwardIteratorEnd, class ProcessMatchedCharacter>
[[nodiscard]] auto get_greedy_match(Rule&& rule, ForwardIteratorBegin begin, ForwardIteratorEnd end, ProcessMatchedCharacter&& process_char)
	-> decltype(get_greedy_match(std::declval<Rule&&>(), ForwardIteratorBegin(), ForwardIteratorEnd(), std::declval<ProcessMatchedCharacter&&>()))
{
	auto pr = get_greedy_match(std::forward<Rule>(rule), begin, end);
	if (pr.first)
		for (auto it = begin; it != pr.second; ++it)
			process_char(*it);
	return pr;
}

template <class Rule, class Container, class ProcessMatchedCharacter>
[[nodiscard]] auto get_greedy_match(Rule&& rule, const Container& container, ProcessMatchedCharacter&& process_char)
	-> decltype(get_greedy_match(std::declval<Rule&&>(), std::begin(std::declval<const Container&>()), std::end(std::declval<const Container&>()),
		std::declval<ProcessMatchedCharacter&&>()))
{
	return get_greedy_match(std::forward<Rule>(rule), std::begin(container), std::end(container), std::forward<ProcessMatchedCharacter>(process_char));
}

template <class TargetTraitsType, class TargetAllocatorType, class Rule, class ForwardIteratorBegin, class ForwardIteratorEnd>
[[nodiscard]] auto parse_to_string_pair(const Rule& rule, ForwardIteratorBegin begin, ForwardIteratorEnd end)
	-> std::enable_if_t<
		std::is_base_of_v<std::forward_iterator_tag, typename std::iterator_traits<ForwardIteratorBegin>::iterator_category> &&
		std::is_base_of_v<std::forward_iterator_tag, typename std::iterator_traits<ForwardIteratorEnd>::iterator_category>,
		std::pair<bool, std::basic_string<typename std::iterator_traits<ForwardIteratorBegin>::value_type, TargetTraitsType, TargetAllocatorType>>
	>
{
	using target_string = std::basic_string<typename std::iterator_traits<ForwardIteratorBegin>::value_type, TargetTraitsType, TargetAllocatorType>;
	auto pr = get_greedy_match(rule, begin, end);
	return std::make_pair(pr.first, pr.first?target_string(begin, pr.second):target_string());
}

template <class TargetTraitsType, class Rule, class ForwardIteratorBegin, class ForwardIteratorEnd>
[[nodiscard]] auto parse_to_string_pair(const Rule& rule, ForwardIteratorBegin begin, ForwardIteratorEnd end)
-> std::enable_if_t<
	std::is_base_of_v<std::forward_iterator_tag, typename std::iterator_traits<ForwardIteratorBegin>::iterator_category> &&
	std::is_base_of_v<std::forward_iterator_tag, typename std::iterator_traits<ForwardIteratorEnd>::iterator_category>,
	std::pair<bool, std::basic_string<typename std::iterator_traits<ForwardIteratorBegin>::value_type, TargetTraitsType>>
>
{
	return parse_to_string_pair<TargetTraitsType, std::allocator<typename std::iterator_traits<ForwardIteratorBegin>::value_type>>
		(rule, begin, end);
}

template <class Rule, class ForwardIteratorBegin, class ForwardIteratorEnd>
[[nodiscard]] auto parse_to_string_pair(const Rule& rule, ForwardIteratorBegin begin, ForwardIteratorEnd end)
-> std::enable_if_t<
	std::is_base_of_v<std::forward_iterator_tag, typename std::iterator_traits<ForwardIteratorBegin>::iterator_category> &&
	std::is_base_of_v<std::forward_iterator_tag, typename std::iterator_traits<ForwardIteratorEnd>::iterator_category>,
	std::pair<bool, std::basic_string<typename std::iterator_traits<ForwardIteratorBegin>::value_type>>
>
{
	return parse_to_string_pair<
		std::char_traits<typename std::iterator_traits<ForwardIteratorBegin>::value_type>,
		std::allocator<typename std::iterator_traits<ForwardIteratorBegin>::value_type>
		>(rule, begin, end);
}

template <class TargetTraitsType, class TargetAllocatorType, class Rule, class ForwardIterator>
[[nodiscard]] auto parse_to_string_pair(const Rule& rule, ForwardIterator it, std::make_unsigned_t<typename std::iterator_traits<ForwardIterator>::difference_type> max_count)
{
	return parse_to_string_pair<TargetTraitsType, TargetAllocatorType>(rule, offset_iterator_begin(it), offset_iterator_end(it, max_count));
}

template <class TargetTraitsType, class Rule, class ForwardIterator>
[[nodiscard]] auto parse_to_string_pair(const Rule& rule, ForwardIterator it, std::make_unsigned_t<typename std::iterator_traits<ForwardIterator>::difference_type> max_count)
{
	return parse_to_string_pair<TargetTraitsType>(rule, offset_iterator_begin(it), offset_iterator_end(it, max_count));
}

template <class Rule, class ForwardIterator>
[[nodiscard]] auto parse_to_string_pair(const Rule& rule, ForwardIterator it, std::make_unsigned_t<typename std::iterator_traits<ForwardIterator>::difference_type> max_count)
{
	return parse_to_string_pair(rule, offset_iterator_begin(it), offset_iterator_end(it, max_count));
}

template <class TargetTraitsType, class Rule, class CharType>
[[nodiscard]] auto parse_to_string_view_pair(const Rule& rule, const CharType* pString, std::size_t cString)
{
	typedef std::basic_string_view<CharType, TargetTraitsType> target_string_view;
	auto pr = get_greedy_match(rule, pString, pString + cString);
	return std::make_pair(pr.first, pr.first?target_string_view(pString, std::size_t(pr.second - pString)):target_string_view());
}

template <class Rule, class CharType>
[[nodiscard]] auto parse_to_string_view_pair(const Rule& rule, const CharType* pString, std::size_t cString)
{
	return parse_to_string_view_pair<std::char_traits<CharType>>(pString, cString);
}

template <class TargetTraitsType, class TargetAllocatorType, class Rule, class ForwardIteratorBegin, class EndIteratorOrSize>
[[nodiscard]] auto parse_to_string(const Rule& rule, ForwardIteratorBegin begin, EndIteratorOrSize end)
{
	return parse_to_string_pair<TargetTraitsType, TargetAllocatorType>(rule, begin, end).second;
}

template <class TargetTraitsType, class Rule, class ForwardIteratorBegin, class EndIteratorOrSize>
[[nodiscard]] auto parse_to_string(const Rule& rule, ForwardIteratorBegin begin, EndIteratorOrSize end)
{
	return parse_to_string_pair<TargetTraitsType>(rule, begin, end).second;
}

template <class Rule, class ForwardIteratorBegin, class EndIteratorOrSize>
[[nodiscard]] auto parse_to_string(const Rule& rule, ForwardIteratorBegin begin, EndIteratorOrSize end)
{
	return parse_to_string_pair(rule, begin, end).second;
}

template <class TargetTraitsType, class Rule, class CharType>
[[nodiscard]] auto parse_to_string_view(const Rule& rule, const CharType* pString, std::size_t cString)
{
	return parse_to_string_view_pair<TargetTraitsType>(rule, pString, cString).second;
}

template <class Rule, class CharType>
[[nodiscard]] auto parse_to_string_view(const Rule& rule, const CharType* pString, std::size_t cString)
{
	return parse_to_string_view_pair(rule, pString, cString).second;
}

template <class TargetTraitsType, class TargetAllocatorType, class Rule, class ForwardIteratorBegin, class ForwardIteratorEnd>
[[nodiscard]] auto make_string_pair(const Rule& rule, ForwardIteratorBegin begin, ForwardIteratorEnd end)
	-> std::enable_if_t<!std::is_convertible_v<ForwardIteratorEnd, std::make_unsigned_t<typename std::iterator_traits<ForwardIteratorBegin>::difference_type>>,
	std::pair<bool, std::basic_string<typename std::iterator_traits<ForwardIteratorBegin>::value_type, TargetTraitsType, TargetAllocatorType>>>
{
	return parse_to_string_pair<TargetTraitsType, TargetAllocatorType>(rule, begin, end);
}

template <class TargetTraitsType, class Rule, class ForwardIteratorBegin, class ForwardIteratorEnd>
[[nodiscard]] auto make_string_pair(const Rule& rule, ForwardIteratorBegin begin, ForwardIteratorEnd end)
-> std::enable_if_t<!std::is_convertible_v<ForwardIteratorEnd, std::make_unsigned_t<typename std::iterator_traits<ForwardIteratorBegin>::difference_type>>,
	std::pair<bool, std::basic_string<typename std::iterator_traits<ForwardIteratorBegin>::value_type, TargetTraitsType>>>
{
	return parse_to_string_pair<TargetTraitsType>(rule, begin, end);
}

template <class Rule, class ForwardIteratorBegin, class ForwardIteratorEnd>
[[nodiscard]] auto make_string_pair(const Rule& rule, ForwardIteratorBegin begin, ForwardIteratorEnd end)
-> std::enable_if_t<!std::is_convertible_v<ForwardIteratorEnd, std::make_unsigned_t<typename std::iterator_traits<ForwardIteratorBegin>::difference_type>>,
	std::pair<bool, std::basic_string<typename std::iterator_traits<ForwardIteratorBegin>::value_type>>>
{
	return parse_to_string_pair(rule, begin, end);
}

template <class TargetTraitsType, class TargetAllocatorType, class Rule, class CharType>
[[nodiscard]] auto make_string_pair(const Rule& rule, const CharType* pInput, std::size_t cInput)
	-> std::pair<bool, std::basic_string<CharType, TargetTraitsType, TargetAllocatorType>>
{
	return make_string_pair<TargetTraitsType, TargetAllocatorType>(rule, pInput, &pInput[cInput]);
}

template <class TargetTraitsType, class Rule, class CharType>
[[nodiscard]] auto make_string_pair(const Rule& rule, const CharType* pInput, std::size_t cInput)
-> std::pair<bool, std::basic_string<CharType, TargetTraitsType>>
{
	return make_string_pair<TargetTraitsType>(rule, pInput, &pInput[cInput]);
}

template <class Rule, class CharType>
[[nodiscard]] auto make_string_pair(const Rule& rule, const CharType* pInput, std::size_t cInput)
-> std::pair<bool, std::basic_string<CharType>>
{
	return make_string_pair(rule, pInput, &pInput[cInput]);
}

template <class TargetTraitsType, class TargetAllocatorType, class Rule, class Container>
[[nodiscard]] auto make_string_pair(const Rule& rule, Container&& cont)
{
	return make_string_pair<TargetTraitsType, TargetAllocatorType>(rule, std::begin(cont), std::end(cont));
}

template <class TargetTraitsType, class Rule, class Container>
[[nodiscard]] auto make_string_pair(const Rule& rule, Container&& cont)
{
	return make_string_pair<TargetTraitsType>(rule, std::begin(cont), std::end(cont));
}

template <class Rule, class Container>
[[nodiscard]] auto make_string_pair(const Rule& rule, Container&& cont)
{
	return make_string_pair(rule, std::begin(cont), std::end(cont));
}

template <class TargetTraitsType, class TargetAllocatorType, class Rule, class ForwardIteratorBegin, class EndIteratorOrSize>
[[nodiscard]] auto make_string(const Rule& rule, ForwardIteratorBegin begin, EndIteratorOrSize end)
{
	return make_string_pair<TargetTraitsType, TargetAllocatorType>(rule, begin, end).second;
}

template <class TargetTraitsType, class Rule, class ForwardIteratorBegin, class EndIteratorOrSize>
[[nodiscard]] auto make_string(const Rule& rule, ForwardIteratorBegin begin, EndIteratorOrSize end)
{
	return make_string_pair<TargetTraitsType>(rule, begin, end).second;
}

template <class Rule, class ForwardIteratorBegin, class EndIteratorOrSize>
[[nodiscard]] auto make_string(const Rule& rule, ForwardIteratorBegin begin, EndIteratorOrSize end)
{
	return make_string_pair(rule, begin, end).second;
}

template <class TargetTraitsType, class TargetAllocatorType, class Rule, class Container>
[[nodiscard]] auto make_string(const Rule& rule, Container&& cont)
{
	return make_string_pair<TargetTraitsType, TargetAllocatorType>(rule, std::forward<Container>(cont)).second;
}

template <class TargetTraitsType, class Rule, class Container>
[[nodiscard]] auto make_string(const Rule& rule, Container&& cont)
{
	return make_string_pair<TargetTraitsType>(rule, std::forward<Container>(cont)).second;
}

template <class Rule, class Container>
[[nodiscard]] auto make_string(const Rule& rule, Container&& cont)
{
	return make_string_pair(rule, std::forward<Container>(cont)).second;
}

ABNF_END

#endif //SIMPLE_WEBSERVER_ABNF_H
