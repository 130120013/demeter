#pragma warning(disable: 4786)

#include <string>
#include <vector>
#include <iostream>
#include "../../../bnflite/bnflite.h"

using namespace bnf;
using namespace std;


/* somple ini file configuration example:
[section_1]
var1=value1
var2=value2

[section_2]
var1=value1
var2=value2
*/

struct Section
{
     string name; 
     vector< pair< string, string> > value;
     Section(const char* name, size_t len) :name(name, len) {}
};
vector <struct Section> Ini;  // ini-file configuration container 

// example for custom interface instead of "typedef Interface<int> Gen;"
class Gen :public  Interface<>
{
public:
    Gen(const Gen& ifc, const char* text, size_t length, const char* name)
        :Interface<>(ifc, text, length, name){}
    Gen(const char* text, size_t length,  const char* name)
        :Interface<>(text, length, name){}
    Gen(const Gen& front, const Gen& back)
        :Interface<>(front, back) {}
    Gen() :Interface<>() {};
};


static bool printMsg(const char* lexem, size_t len)
{   // debug function
    printf("Debug: %.*s;\n", len, lexem);
    return true;
}

Gen DoAssignment(vector<Gen>& res)
{   // save new section, it is 2nd lexem in section Rule in main
    for (auto& r : res) 
    {
        std::cout << "resourse text: " << r.text << "\n";
        std::cout << "resourse length: " << r.length << "\n";
        std::cout << "resourse name: " << r.name << "\n";
    }
    Ini.push_back(Section(res[0].text, 2));

    std::cout << Ini.size() << "\n";
    return Gen(res[0].text, res[0].length, res[0].name);
}

Gen DoSection(vector<Gen>& res)
{   // save new section, it is 2nd lexem in section Rule in main
    Ini.push_back(Section(res[1].text, res[1].length));
    return Gen(res.front(), res.back());
}

Gen DoValue(vector<Gen>& res)
{   // save new entry: 4th lexem - name and 7th lexem is property value
   int i = res.size();
   if (i > 2 )
    Ini.back().value.push_back(make_pair(
       string(res[0].text, res[0].length), string(res[2].text, res[2].length)));
   return Gen(res.front(), res.back());
}
/*
static void Bind(Rule& section, Rule& entry)
{
    Bind(section, DoSection);
    Bind(entry, DoValue);
}
*/
// example of custom pre-parser
static const char* ini_zero_parse(const char* ptr)
{   // skip ini file comments
    //if (*ptr ==';' ||  *ptr =='#' || *ptr == ' ')
    //    while (*ptr != 0)
    //        if( *ptr++ == '\n')
     //           break;
    return ptr;
}


int main()
{
    Token delimiter(";");     // consider new lines as grammar part too
    Token name("_");  // start declare with special symbols
    name.Add('0', '9'); // appended numeric part
    name.Add('a', 'z'); // appended alphabetic lowercase part
    name.Add('A', 'Z'); // appended alphabetic capital part
    Token value(1,255); value.Remove("\n");

    Lexem Name = 1*name;
    Lexem Value = *value;
    Lexem Assign = "<-";
    Lexem Left  = "{";        // bracket
    Lexem Right  = "}";
    Lexem Delimiter  = *delimiter;
    Token Digit("01234567");
    Lexem Number = Iterate(1, Digit);

    //Rule Assignment = Name + Assign + Number + Delimiter;
    Rule Assignment;
    Rule Variable = Name;
    Bind(Variable, DoSection);
    Bind(Assignment, DoAssignment);

    auto ini = "x<-y;";
    Gen gen; // this is Interface object

    int tst = Analyze(Assignment, ini, gen, ini_zero_parse);
    if (tst > 0)
        cout << "Section read:" << Ini.size();
    else
        cout << "Parsing errors detected, status = " << hex << tst << endl
         << "stopped at: " << (gen.data + gen.length)  << endl;

    for (vector<struct Section>::iterator j = Ini.begin(); j != Ini.end(); ++j) {
        cout << endl << "Section " << j->name << " has " << (*j).value.size() << " values: "; 
        for (vector<pair<string, string> >::iterator i = j->value.begin(); i != j->value.end(); ++i) {
            cout << i->first << "="  << i->second <<"; ";
        }
    }

    return  0; 
}
