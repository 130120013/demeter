#include "abnf.hpp"
#include <string>
#include <iostream>
#include <fstream>

int main(int argc, char* argv[])
{
    std::string code_string;

    auto Ignorable = new DynamicStringImpl(std::regex("\\s{0,}\\n{0,}\\t{0,}\\r{0,}"));
    auto Assign_sign = new StaticStringImpl("<-");
    auto Delimeter_sign = new StaticStringImpl(";");
    auto Definition_sign = new AlterImpl{ new StaticStringImpl("var"), new StaticStringImpl("arr") };
    auto Rule_relation = new AlterImpl
    {
        new StaticStringImpl("<"), new StaticStringImpl("<="), new StaticStringImpl(">"), new StaticStringImpl(">="),
        new StaticStringImpl("=="), new StaticStringImpl("!=")
    };

    auto Rule_type = new AlterImpl{ new StaticStringImpl("Number") };
    auto Rule_digit = new RangeAlterImpl('0', '9');
    auto Rule_number = new ConcatImpl{ Rule_digit, new RepeatImpl{Rule_digit} };
    auto Rule_string = new DynamicStringImpl(std::regex("\"[\\w]{0,}\""));
    auto Rule_id_char = new DynamicStringImpl(std::regex("\'[\\w]\'"));
    auto Rule_identificator = new DynamicStringImpl(
        std::regex("\\b(?!end\\b)([a-zA-Z]{1,})(\\_){0,}(\\d){0,}\\b"));
    auto Rule_variable = new AlterImpl
    {
        new ConcatImpl
        {
            Rule_identificator, Ignorable, new StaticStringImpl("["),
            Ignorable, Rule_number, Ignorable, new StaticStringImpl("]")
        }, Rule_identificator
    };
    auto Rule_arithmetic_sign = new AlterImpl
    {
        new StaticStringImpl("+"), new StaticStringImpl("-"), new StaticStringImpl("*"),
        new StaticStringImpl("/"), new StaticStringImpl("%")
    };
    auto Rule_definition = new ConcatImpl{ Definition_sign, Ignorable, Rule_variable };
    
    auto Rule_non_literal_operand = new AlterImpl{Rule_variable, Rule_number, new RuleBase() };
    auto Rule_term = new AlterImpl
    { 
        new RuleBase(),
        new ConcatImpl
        {
            Ignorable, new RepeatImpl{new AlterImpl{new StaticStringImpl("-"), new StaticStringImpl("+")}, 0, 1},
            Ignorable, Rule_non_literal_operand, Ignorable
        }, 
        Rule_id_char, Rule_string
    };
    auto Rule_expression = new ConcatImpl
    {
        Rule_term, Ignorable, new RepeatImpl {new ConcatImpl{Ignorable, Rule_arithmetic_sign, Ignorable, Rule_term, Ignorable}}
    };
    auto Rule_func_call = new ConcatImpl
    {
        Rule_identificator, Ignorable, new StaticStringImpl("("), Ignorable,
        new RepeatImpl{Rule_expression}, Ignorable, new RepeatImpl{new StaticStringImpl(",")}, Ignorable,
        new StaticStringImpl(")"), Ignorable
    };
    Rule_term->set_rule_at(0, Rule_func_call);
    Rule_non_literal_operand->set_rule_at(2, Rule_func_call);

    auto Rule_assignment = new ConcatImpl
    {
        new AlterImpl
        {
            new ConcatImpl
            {
                Rule_definition, Ignorable, Assign_sign, Ignorable, new RepeatImpl{Rule_expression}
            },
            new ConcatImpl
            {
                Rule_variable, Ignorable,  Assign_sign, Ignorable, new RepeatImpl{Rule_expression}
            }
        }, Ignorable, Delimeter_sign, Ignorable
    };

    auto Rule_condition = new ConcatImpl
    {
        Rule_expression, Ignorable, Rule_relation, Ignorable, Rule_expression
    };

    auto Rule_command = new AlterImpl{ new RuleBase(), new RuleBase(), Rule_assignment };
    auto Rule_func_def = new ConcatImpl
    {
        Ignorable, new StaticStringImpl("func"), Ignorable, Rule_identificator, Ignorable, new StaticStringImpl("("), Ignorable,
        new RepeatImpl{new ConcatImpl{Rule_identificator, Ignorable, new RepeatImpl{new StaticStringImpl(",")}, Ignorable}},
        new StaticStringImpl(")"), Ignorable,

        new StaticStringImpl("{"), Ignorable, new RepeatImpl { new ConcatImpl{Ignorable, Rule_command, Ignorable}},
        new StaticStringImpl("}"), Ignorable
    };
    auto Rule_if = new ConcatImpl
    { 
        Ignorable, new StaticStringImpl("if"), Ignorable, new StaticStringImpl("("), Ignorable,
        Rule_condition, Ignorable, new StaticStringImpl(")"), Ignorable, new StaticStringImpl("then"), Ignorable,
        new StaticStringImpl("{"), Ignorable, new RepeatImpl { new ConcatImpl{Ignorable, Rule_command, Ignorable}},
        new StaticStringImpl("}"), Ignorable,
        new RepeatImpl
        {
            new ConcatImpl
            {
                new StaticStringImpl("else"), Ignorable, new StaticStringImpl("{"), Ignorable,
                new RepeatImpl{Rule_command}, Ignorable, new StaticStringImpl("}"), Ignorable
            }, 0, 1
        }, 
    };
    auto Rule_while = new ConcatImpl
    {
        Ignorable, new StaticStringImpl("while"), Ignorable,
        new StaticStringImpl("("), Ignorable, Rule_condition, Ignorable, new StaticStringImpl(")"), Ignorable,
        new StaticStringImpl("{"), Ignorable,

        new RepeatImpl { new ConcatImpl{Ignorable, Rule_command, Ignorable}},
        new StaticStringImpl("}"), Ignorable
    };
    Rule_command->set_rule_at(0, Rule_if);
    Rule_command->set_rule_at(1, Rule_while);

    auto Rule_program = new ConcatImpl
    {
        new StaticStringImpl("begin"),
        new RepeatImpl
        {
            new AlterImpl
            {
                new ConcatImpl {Ignorable, Rule_func_def, Ignorable},
                new ConcatImpl {Ignorable, Rule_expression, Ignorable, Delimeter_sign, Ignorable},
                new ConcatImpl {Ignorable, Rule_definition, Ignorable, Delimeter_sign, Ignorable},
                new ConcatImpl {Ignorable, Rule_command, Ignorable},
                new ConcatImpl {Ignorable, Rule_if, Ignorable},
                new ConcatImpl {Ignorable, Rule_while, Ignorable}
            }
        },
        Ignorable, new StaticStringImpl("end.")
    };
    
    if (argc < 2)
    {
        std::cout << "Usage: ODSL <path-to-code>" << '\n';
        std::cout << "Example: ./ODSL \"/path/to/odsl/code.txt\"" << '\n';
    }
    else
    {
        std::ifstream fstream(argv[1], std::ios::in);
        code_string = std::string((std::istreambuf_iterator<char>(fstream)), std::istreambuf_iterator<char>()) ;
    }

    if (Rule_program->get_match(code_string.begin(), code_string.end()).matched)
        std::cout << "This program is correct\n";
    else
        std::cout << "Met error\n";

    delete Ignorable; delete Assign_sign; delete Delimeter_sign;
    delete Definition_sign; delete Rule_relation; delete Rule_type;
    delete Rule_digit; delete Rule_number; delete Rule_string;
    delete Rule_id_char; delete Rule_identificator; delete Rule_variable;
    delete Rule_arithmetic_sign; delete Rule_definition; delete Rule_non_literal_operand;
    delete Rule_term; delete Rule_expression; delete Rule_func_call;
    delete Rule_assignment; delete Rule_condition; delete Rule_command;
    delete Rule_func_def; delete Rule_if; delete Rule_while; delete Rule_program;

    return 0;
}

