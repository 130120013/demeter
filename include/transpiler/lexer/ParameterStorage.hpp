#include <cstdlib>
#include <type_traits>
#include <utility>
#include <vector>
#include <concepts>
#include <tuple>
#include <stdexcept>
#include <functional>

#ifndef PARAMETER_STORAGE_HPP
#define PARAMETER_STORAGE_HPP

template<class... Args>
constexpr std::size_t length()
{
    return sizeof... (Args);
}

template <class... T>
class dynamic_parameter_storage 
{
public:
    constexpr dynamic_parameter_storage() {}
    //dynamic_parameter_storage(dynamic_parameter_storage<T...>&& strg) :
      //  params(std::forward<T...>(strg.params)) {}
    dynamic_parameter_storage(const dynamic_parameter_storage<T...>& other)
    {
        copy_tuple(other.params, params);
    }    
    template<class... U>
    dynamic_parameter_storage(U... args) 
    {
        std::tuple<U...> from(args...);
        copy_tuple(args..., params);
    }
    constexpr std::size_t get_params_count() const noexcept
    {
        return length<T...>();
    }
    template<class U>
    auto& operator[](U) noexcept
    {
        return std::get<std::is_placeholder<U>::value - 1>(params);
    }

    template<class U>
    auto const& operator[](U) const noexcept
    {
        return std::get<std::is_placeholder<U>::value - 1>(params);
    }
    std::tuple<T...> get_params() const noexcept
    {
        return params;
    }
private:
    std::tuple<T...> params;


// Limit case
template<std::size_t I = 0, typename ...From, typename ...To>
typename std::enable_if<(I >= sizeof...(From) || I >= sizeof...(To))>::type
copy_tuple(std::tuple<From...> const & from, std::tuple<To...> & to) {}

// Recursive case
template<std::size_t I = 0, typename ...From, typename ...To>
typename std::enable_if<(I < sizeof...(From) && I < sizeof...(To))>::type
copy_tuple(std::tuple<From...> const & from, std::tuple<To...> & to) 
{
    std::get<I>(to) = std::get<I>(from);
    copy_tuple<I + 1>(from,to);
}
};

template <class T, std::size_t N>
class static_parameter_storage
{
	struct { T params[N]; } strg;
	T* top = strg.params;
public:
	static_parameter_storage() = default;
	static_parameter_storage(const static_parameter_storage& right)
	{
		*this = right;
	}
	static_parameter_storage(const std::vector<T>& right)
	{
		for (std::size_t i = 0; i < N; ++i)
			strg.params[i] = right[i];
	}
	static_parameter_storage(static_parameter_storage&& right)
	{
		*this = std::move(right);
	}
	static_parameter_storage& operator=(const static_parameter_storage& right)
	{
		strg = right.strg;
		return *this;
	}
	static_parameter_storage& operator=(static_parameter_storage&& right)
	{
		strg = std::move(right.strg);
		return *this;
	}
	const T& operator[](std::size_t index) const
	{
		if (index < N)
			return strg.params[index];
		throw std::range_error("static_parameter_storage: invalid parameter index");
	}
	T& operator[](std::size_t index)
	{
		return const_cast<T&>(const_cast<const static_parameter_storage&>(*this)[index]);
	}
	template <class U>
	auto push_argument(U&& arg) -> std::enable_if_t<std::is_convertible<std::decay_t<U>, T>::value>
	{
		if (top - strg.params >= N)
			throw std::range_error("static_parameter_storage: buffer overflow");
		*(top++) = std::forward<U>(arg);
	}
	bool is_ready() const
	{
		return top == &strg.params[N] && this->is_ready_from<0>();
	}
private:
	template <std::size_t I, class = void>
	auto is_ready_from() const -> std::enable_if_t<(I >= N), bool>
	{
		return true;
	}
	template <std::size_t I, class = void>
	auto is_ready_from() const->std::enable_if_t<(I < N), bool>
	{
		return strg.params[I]->is_ready() && this->is_ready_from<I + 1>();
	}
};

#endif

