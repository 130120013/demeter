#ifndef OPERATOR_PROPERTY_H
#define OPERATOR_PROPERTY_H

#include <cstdio>
#include <concepts>
#include <type_traits>
#include <functional>
#include <tuple>
#include "ParameterStorage.hpp"

class CommutativeProperty {};
class AnticommutativeProperty {};
class AssociativeProperty {};
class DistributiveProperty {};
class IdempotenceProperty {};
class CancelableProperty {};
class InverseOperation {};
class multiple_inverse_operation_tag {};

template<class ReturnType, class... ParamTypes>
class ArityWrapper 
{
public:
    constexpr ArityWrapper() {}
    ArityWrapper(
        const dynamic_parameter_storage<ParamTypes...>& params,
        ReturnType(*functor)(ParamTypes...)):
        m_params(params)
    {
        m_functor = functor;
        m_params = params;
    }
    ArityWrapper(const std::tuple<ParamTypes...>& params,
        std::function<ReturnType(ParamTypes...)> functor)
    {
        m_params = dynamic_parameter_storage<ParamTypes...>(params);
        m_functor = functor;
    }
    constexpr ReturnType compute() 
    {
       return std::apply(m_functor, m_params.get_params());    
    }
protected:
        dynamic_parameter_storage<ParamTypes...> m_params;
        //std::function<ReturnType(ParamTypes...)> m_functor;
        ReturnType(*m_functor)(ParamTypes...);
        dynamic_parameter_storage<std::function<ReturnType
            (dynamic_parameter_storage<ParamTypes...>)>> m_parameter_inverse_function;
};

template<class T>
inline constexpr bool is_commutative() 
{ 
	return std::is_base_of<CommutativeProperty, std::remove_pointer<T>>::value; 
}

template<class T>
inline constexpr bool is_anticommutative() 
{ 
	return std::is_base_of<AnticommutativeProperty, std::remove_pointer<T>>::value; 
}

template<class T>
inline constexpr bool is_associative() 
{ 
	return std::is_base_of<AssociativeProperty, std::remove_pointer<T>>::value; 
}
template<class T>
inline constexpr bool is_idempotent() 
{ 
	return std::is_base_of<IdempotenceProperty, std::remove_pointer<T>>::value; 
}
template<class T>
inline constexpr bool is_distributive() 
{ 
	return std::is_base_of<DistributiveProperty, std::remove_pointer<T>>::value; 
}
template<class T>
inline constexpr bool is_cancelable() 
{ 
	return std::is_base_of<CancelableProperty, std::remove_pointer<T>>::value; 
}
template<class T>
inline constexpr bool is_inverse() 
{ 
	return std::is_base_of<InverseOperation, std::remove_pointer<T>>::value; 
}
template<class T>
inline constexpr bool has_multiple_inverse_operation() 
{
	return  std::is_base_of<multiple_inverse_operation_tag, std::remove_pointer<T>>::value;
}


#endif

