#include "OperatorProperty.hpp"
#include <cmath>
#include <string>
#include <regex>
#include <functional>
#include <utility>
#include <vector>
#include <array>
#include <ranges>

#ifndef LEXIC_H
#define LEXIC_H
#pragma once

std::string get_function_regex(std::string name) 
{
    return std::string("^(" + name + ")(\\s){0,}(\\t){0,}(\\(.+\\)){1,}");
}  
std::string get_operator_regex(std::string name) 
{
    return std::string("^(" + name + ")(\\s){0,}(\\t){0,}");
}  
std::string get_identificator_regex() 
{
    return "^([a-zA-Z]{1,})(\\_){0,}(\\d){0,}";
}  
std::string get_terminal_regex() 
{
    return ";";
}  
std::string get_ingorable_regex() 
{
    return "(\\s){0,}(\\t){0,}";
}
std::string get_number_regex() 
{
    return "^[0-9]{1,}";
}
class IToken 
{
    public:
    virtual std::string get_regex() const
    {
        return "=";
    }
    
    virtual ~IToken() {}
};

class Identificator : public IToken
{
    public:
    constexpr Identificator() {}
    virtual std::string get_regex() const
    {
        return get_identificator_regex();
    }
    virtual ~Identificator() {}
};

template<class RetType, class... ArgTypes>
class Keyword : public IToken 
{
public:
    constexpr Keyword() {}
    Keyword(std::string name, 
            const ArityWrapper<RetType, ArgTypes...>& arity_wrapper,
            short priority):
        m_name(name), m_arity_wrapper(arity_wrapper), m_priority(priority) {}
    constexpr RetType compute()
    {
        return m_arity_wrapper.compute();
    }
    virtual ~Keyword() {}
protected:
    std::string m_name;
    ArityWrapper<RetType, ArgTypes...> m_arity_wrapper;
    short m_priority = 0;    
};

template<class RetType, class... ArgTypes>
class Operator : public Keyword<RetType, ArgTypes...> //+ - * / > < == != >= <= 
{
    public: 
    constexpr Operator() {}
    Operator(std::string name, 
            const ArityWrapper<RetType, ArgTypes...>& arity_wrapper,
            short priority): 
        Keyword<RetType, ArgTypes...>(name, arity_wrapper, priority)
        {}
    /*virtual constexpr std::string get_regex() 
    {
        return "(+|-|/|*|>|<|==|!=|>=|<=)";
    }*/
    virtual std::string get_regex() const
    {
        return get_operator_regex(this->m_name);
    }
    virtual ~Operator(){}
};

template<class RetType, class... ArgTypes>
class Function: public Keyword<RetType, ArgTypes...>
{
    public: 
    constexpr Function() {}
    Function(std::string name, 
            const ArityWrapper<RetType, ArgTypes...>& arity_wrapper,
            short priority): 
        Keyword<RetType, ArgTypes...>(name, arity_wrapper, priority)
        {}
    virtual std::string get_regex() const 
    {
        return get_function_regex(this->m_name);
    }  
    virtual ~Function() {}
};

class Literal : public IToken
{
    public:
    constexpr Literal() {}
    virtual ~Literal(){}
};

class Terminal : public IToken 
{
    public:
    constexpr Terminal(){}
    virtual std::string get_regex() const
    {
        return get_terminal_regex();
    }  
    virtual ~Terminal(){}
};

enum class LexemClasses
{
    TRM,
    CON,
    IDN
};

struct IdentificatorRecord 
{
    LexemClasses type;
    IToken* token;
    std::size_t begin_pos;
    std::size_t end_pos;
};

enum class Tables
{
    terminals,
    operators,
    functions,
    consts,
    ids
};

struct TerminalSymbolInfo 
{
    std::string name;
    std::string(*regex_func)();
    bool is_delimeter;
    bool is_ignorable;
};

struct LexemInfo 
{
    Tables table;
    std::size_t position;
    std::string name;
};
std::vector<TerminalSymbolInfo> allowed_tokens 
{
    TerminalSymbolInfo{.name = " ", .regex_func = +[](){return std::string("\\s{0,}");}, .is_delimeter = true, .is_ignorable = true},
    TerminalSymbolInfo{.name = "\\t", .regex_func = +[](){return std::string("\\t{0,}");}, .is_delimeter = true, .is_ignorable = true},
    TerminalSymbolInfo{.name = "(", .regex_func = +[](){return std::string("\\(");}, .is_delimeter = true, .is_ignorable = false},
    TerminalSymbolInfo{.name = ")", .regex_func = +[](){return std::string("\\)");}, .is_delimeter = true, .is_ignorable = false},
    TerminalSymbolInfo{.name = ",", .regex_func = +[](){return std::string(",");}, .is_delimeter = true, .is_ignorable = false},
    TerminalSymbolInfo{.name = "+", .regex_func = +[](){return get_operator_regex("\\+");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = "-", .regex_func = +[](){return get_operator_regex("-");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = "*", .regex_func = +[](){return get_operator_regex("\\*");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = "/", .regex_func = +[](){return get_operator_regex("/");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = "<-", .regex_func = +[](){return get_operator_regex("<-");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = "<", .regex_func = +[](){return get_operator_regex("<");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = ">", .regex_func = +[](){return get_operator_regex(">");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = "<=", .regex_func = +[](){return get_operator_regex("<=");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = ">=", .regex_func = +[](){return get_operator_regex(">=");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = "==", .regex_func = +[](){return get_operator_regex("==");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = "!=", .regex_func = +[](){return get_operator_regex("!=");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = ";", .regex_func = get_terminal_regex, .is_delimeter = true, .is_ignorable = false},
    TerminalSymbolInfo{.name = "sin", .regex_func = +[](){return get_operator_regex("sin");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = "cos", .regex_func = +[](){return get_operator_regex("cos");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = "tan", .regex_func = +[](){return get_operator_regex("tan");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = "asin", .regex_func = +[](){return get_operator_regex("asin");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = "acos", .regex_func = +[](){return get_operator_regex("acos");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = "atan", .regex_func = +[](){return get_operator_regex("atan");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = "j0", .regex_func = +[](){return get_operator_regex("j0");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = "j1", .regex_func = +[](){return get_operator_regex("j1");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = "jn", .regex_func = +[](){return get_operator_regex("jn");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = "y0", .regex_func = +[](){return get_operator_regex("y0");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = "y1", .regex_func = +[](){return get_operator_regex("y1");}, .is_delimeter = false, .is_ignorable = false},
    TerminalSymbolInfo{.name = "yn", .regex_func = +[](){return get_operator_regex("yn");}, .is_delimeter = false, .is_ignorable = false}
};
/*dynamic_parameter_storage<int, int, double> d;
    ArityWrapper<int, int> arity(d.get_params(), [](int a) -> int
            {return std::sin(a);});
std::vector<IToken*> allowed_tokens {new Identificator(), new Operator<>("<-",void(),), new Operator("+",,),
new Operator("-",,), new Operator("*",,), new Operator("/",,), new Operator("<",,), new Operator(">",,),
new Operator("<=",,), new Operator(">=",,), new Operator("==",,), new Operator("!=",,), new Terminal(),
new Function("sin",,), new Function("cos",,), new Function("tan",,), new Function("asin",,),
new Function("acos",,), new Function("atan",,), new Function("log",,), new Function("ln",,),
new Function("log10",,), new Function("j0",,), new Function("y0",,), new Function("j1",,),
new Function("y1",,), new Function("jn",,), new Function("yn",,)};
*/
//std::vector<std::pair<IToken*, bool>> terminals;
std::vector<std::pair<IToken*, bool>> terminals;
std::vector<IToken*> operators;
std::vector<std::pair<IToken*, std::string>> constants;
std::vector<IdentificatorRecord> identificators;
std::vector<LexemInfo> lexems;

std::ptrdiff_t filter_by_regex(std::string& s, std::string regex)
{
    std::ptrdiff_t new_begin_pos = -1;
    std::regex pattern(regex, std::regex_constants::icase);
    auto begin = std::sregex_iterator(s.begin(), s.end(), pattern);
    auto end = std::sregex_iterator();

    auto d = std::distance(begin, end); 
    //if the position of the first enterance begins from non-zero it means 
    //that we need apply another regex 
    //example: \d+ text: wwww.111.com
    //position of 111 is not 0, we need another regex to parse www
    auto l = begin->length();
    if (begin != end && begin->length() > 0 && begin->position() == 0)
        new_begin_pos = begin->length();
    return new_begin_pos;
}

void parse_token(const std::string& expression, std::size_t& pos)
{
    auto text_end = -1;
    std::size_t i = 0;
    LexemInfo info;
    std::string chunk(expression.begin() + pos, expression.end());
    while(i < allowed_tokens.size())
    {
        text_end = filter_by_regex(chunk, allowed_tokens[i].regex_func());
        if (text_end != -1)
        {
            if (!allowed_tokens[i].is_ignorable)
            {
                dynamic_parameter_storage<int, int, double> d;
                ArityWrapper<int, int> arity(d.get_params(), [](int a) -> int
                    {return std::sin(a);});
                operators.push_back(new Operator<int, int>(allowed_tokens[i].name, arity, 1));                 
            
                info.table = Tables::terminals;
                info.position = pos;
                info.name = allowed_tokens[i].name;
                lexems.push_back(info);
            }
            break;
        }
        ++i;
    }
    if (text_end == -1)
    {
        text_end = filter_by_regex(chunk, get_identificator_regex());
        if (text_end == -1) 
        {
            text_end = filter_by_regex(chunk, get_number_regex());
            if (text_end == -1)
                throw "Unknown lexem";
            else
            {
                constants.push_back(std::make_pair(identificators.back().token, info.name));
        
                info.table = Tables::consts;
                info.position = pos;
                info.name = std::string(expression.begin() + pos, expression.begin() + pos + text_end);
                lexems.push_back(info);
            }
        }
        else
        {            
            identificators.push_back(IdentificatorRecord {.type = LexemClasses::IDN,
                .token = new Identificator(), .begin_pos = pos, .end_pos = static_cast<std::size_t>(text_end) + pos});

            info.table = Tables::ids;
            info.position = pos;
            info.name = std::string(expression.begin() + pos, expression.begin() + pos + text_end);
            lexems.push_back(info);
        }
    } 
    pos += text_end;
    //if (text_end == -1)
    //    throw std::logic_error("Parse token: Unexpected token");

    //return token_string_entity(pStart, pStart + text_end);
}
    
void lex(const std::string& source) 
{
    std::size_t cur_pos = 0;
    while(source.begin() + cur_pos != source.end())
    {
       parse_token(source, cur_pos); 
    }    
}
#endif
