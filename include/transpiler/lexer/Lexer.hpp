#include <cstddef>
#include <stack>
#include <cstdlib>
#include <cstring>
#include <memory>
#include <list>
#include <algorithm>
#include <queue> //to store arguments
#include <stdexcept> //for exceptions
#include <memory>
#include <map>
#include <list>
#include <limits>
#include <cmath>
#include <iterator>
#include <cassert>
#include <algorithm>
#include <iostream>
#include <regex>

#ifndef PARSER_H
#define PARSER_H

#ifdef _MSC_VER 
#define Y0 _y0
#else
#define Y0 y0
#endif

#ifdef _MSC_VER 
#define J0 _j0
#else
#define J0 j0
#endif

#ifdef _MSC_VER 
#define Y1 _y1
#else
#define Y1 y1
#endif

#ifdef _MSC_VER 
#define J1 _j1
#else
#define J1 j1
#endif

#ifdef _MSC_VER 
#define YN _yn
#else
#define YN yn
#endif

#ifdef _MSC_VER 
#define JN _jn
#else
#define JN jn
#endif

constexpr bool iswhitespace(char ch) noexcept
{
	return ch == ' ' || ch == '\t' || ch == '\n' || ch == '\r' || ch == '\0'; //see also std::isspace
}

constexpr bool isdigit(char ch) noexcept
{
	return ch >= '0' && ch <= '9';
};

constexpr bool isalpha(char ch) noexcept
{
	return (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z');
};

constexpr bool isalnum(char ch) noexcept
{
	return isdigit(ch) || isalpha(ch);
}

constexpr bool iscompare(char ch) noexcept
{
    return ch >= '<' && ch <= '>';
}

constexpr bool is_endcommand(char ch) noexcept 
{
    return ch == ';';
}

template <class Iterator>
Iterator skipSpaces(Iterator pBegin, Iterator pEnd) noexcept
{
	auto pCurrent = pBegin;
	while (iswhitespace(*pCurrent) && ++pCurrent < pEnd);
	return pCurrent;
}

const char* skipSpaces(const char* input_string, std::size_t length) noexcept
{
	return skipSpaces(input_string, input_string + length);
}

const char* skipSpaces(const char* input_string) noexcept
{
	auto pCurrent = input_string;
	while (iswhitespace(*pCurrent) && *pCurrent++ != 0);
	return pCurrent;
}

inline char* skipSpaces(char* input_string, std::size_t length) noexcept
{
	return const_cast<char*>(skipSpaces(const_cast<const char*>(input_string), length));
}

inline char* skipSpaces(char* input_string) noexcept
{
	return const_cast<char*>(skipSpaces(const_cast<const char*>(input_string)));
}

struct token_string_entity
{
	token_string_entity() = default;
    token_string_entity(const char* start_ptr, const char* end_ptr) :m_pStart(start_ptr), m_pEnd(end_ptr) {}
    token_string_entity(std::string tkn): m_pStart(&tkn.front()), m_pEnd(&tkn.front() + tkn.size()) {}
	//return a negative if the token < pString, 0 - if token == pString, a positive if token > pString
	int compare(const char* pString, std::size_t cbString) const noexcept
	{
		auto my_size = this->size();
		auto min_length = my_size < cbString ? my_size : cbString;
		for (std::size_t i = 0; i < min_length; ++i)
		{
			if (m_pStart[i] < pString[i])
				return -1;
			if (m_pStart[i] > pString[i])
				return -1;
		}
		return my_size < cbString ? -1 : (my_size > cbString ? 1 : 0);
	}
	inline int compare(const token_string_entity& right) const noexcept
	{
		return this->compare(right.begin(), right.size());
	}
	inline int compare(const char* pszString) const noexcept
	{
		return this->compare(pszString, std::strlen(pszString));
	}
	template <std::size_t N>
	inline auto compare(const char(&strArray)[N]) const noexcept -> std::enable_if_t<N == 0, int>
	{
		return this->size() == 0;
	}
	template <std::size_t N>
	inline auto compare(const char(&strArray)[N]) const noexcept->std::enable_if_t<(N > 0), int>
	{
		return strArray[N - 1] == '\0' ?
			compare(strArray, N - 1) ://null terminated string, like "sin"
			compare(strArray, N); //generic array, like const char Array[] = {'s', 'i', 'n'}
	}
	inline const char* begin() const noexcept
	{
		return m_pStart;
	}
	inline const char* end() const noexcept
	{
		return m_pEnd;
	}
	inline std::size_t size() const noexcept
	{
		return std::size_t(this->end() - this->begin());
    }
private:
	const char* m_pStart = nullptr, *m_pEnd = nullptr;
};

bool operator==(const token_string_entity& left, const token_string_entity& right) noexcept
{
	return left.compare(right) == 0;
}

bool operator==(const token_string_entity& left, const char* pszRight) noexcept
{
	return left.compare(pszRight) == 0;
}

template <std::size_t N>
bool operator==(const token_string_entity& left, const char(&strArray)[N]) noexcept
{
	return left.compare(strArray) == 0;
}

bool operator!=(const token_string_entity& left, const token_string_entity& right) noexcept
{
	return left.compare(right) != 0;
}

bool operator!=(const token_string_entity& left, const char* pszRight) noexcept
{
	return left.compare(pszRight) != 0;
}

template <std::size_t N>
bool operator!=(const token_string_entity& left, const char(&strArray)[N]) noexcept
{
	return left.compare(strArray) != 0;
}


bool operator==(const char* pszLeft, const token_string_entity& right) noexcept
{
	return right == pszLeft;
}

template <std::size_t N>
bool operator==(const char(&strArray)[N], const token_string_entity& right) noexcept
{
	return right == strArray;
}

bool operator!=(const char* pszRight, const token_string_entity& right) noexcept
{
	return right != pszRight;
}

template <std::size_t N>
bool operator!=(const char(&strArray)[N], const token_string_entity& right) noexcept
{
	return right != strArray;
}

std::ptrdiff_t filter_by_regex(std::string& s, std::string regex)
{
    std::ptrdiff_t new_begin_pos = -1;
    std::regex pattern(regex, std::regex_constants::icase);
    auto begin = std::sregex_iterator(s.begin(), s.end(), pattern);
    auto end = std::sregex_iterator();

    auto d = std::distance(begin, end); 
    //if the position of the first enterance begins from non-zero it means 
    //that we need apply another regex 
    //example: \d+ text: wwww.111.com
    //position of 111 is not 0, we need another regex to parse www
    auto l = begin->length();
    if (begin != end && begin->length() > 0 && begin->position() == 0)
        new_begin_pos = begin->length();
    return new_begin_pos;
}

struct invalid_exception :std::exception
{
	invalid_exception() = default;
	invalid_exception(const char* decription);
};

struct bad_expession_parameters :std::exception
{
	bad_expession_parameters() = default;
	bad_expession_parameters(const char* decription);
};

enum class TokenType : unsigned int
{
	UnaryPlus,
	UnaryMinus,
	BinaryPlus,
	BinaryMinus,
	operatorMul,
	operatorDiv,
	operatorPow,
	sqrtFunction,
	sqrtnFunction,
	sinFunction,
	asinFunction,
	cosFunction,
	acosFunction,
	tanFunction,
	atanFunction,
	log10Function,
	lnFunction,
	logFunction,
	j0Function,
	j1Function,
	jnFunction,
	y0Function,
	y1Function,
	ynFunction,
	minFunction,
	maxFunction,
	bracket,
	number,
	variable,
    boolean,
    operatorAssign,
    operatorGreater,
    operatorLess,
    operatorGreaterEqual,
    operatorLessEqual,
    operatorEqual,
    operatorNotEqual,
    semicolon
};

constexpr bool IsOperatorTokenTypeId(TokenType id)
{
	return id == TokenType::BinaryPlus || id == TokenType::BinaryMinus
		|| id == TokenType::operatorMul || id == TokenType::operatorDiv
		|| id == TokenType::operatorPow;
}

constexpr bool is_logical_operator(TokenType id)
{
    return id == TokenType::operatorEqual || id == TokenType::operatorNotEqual
        || id == TokenType::operatorGreater || id == TokenType::operatorGreaterEqual
        || id == TokenType::operatorLess || id == TokenType::operatorLessEqual;
}

class CommutativeOperator {};
class UnaryOperator {};

template<class T>
inline constexpr bool is_commutative() 
{ 
	return std::is_base_of<CommutativeOperator, std::remove_pointer<T>>::value; 
}

template<class T>
inline constexpr bool is_unary() 
{
	return  std::is_base_of<UnaryOperator, std::remove_pointer<T>>::value;
}

template <class T, std::size_t N>
class static_parameter_storage
{
	struct { T params[N]; } strg;
	T* top = strg.params;
public:
	static_parameter_storage() = default;
	static_parameter_storage(const static_parameter_storage& right)
	{
		*this = right;
	}
	static_parameter_storage(const std::vector<T>& right)
	{
		for (std::size_t i = 0; i < N; ++i)
			strg.params[i] = right[i];
	}
	static_parameter_storage(static_parameter_storage&& right)
	{
		*this = std::move(right);
	}
	static_parameter_storage& operator=(const static_parameter_storage& right)
	{
		strg = right.strg;
		return *this;
	}
	static_parameter_storage& operator=(static_parameter_storage&& right)
	{
		strg = std::move(right.strg);
		return *this;
	}
	const T& operator[](std::size_t index) const
	{
		if (index < N)
			return strg.params[index];
		throw std::range_error("static_parameter_storage: invalid parameter index");
	}
	T& operator[](std::size_t index)
	{
		return const_cast<T&>(const_cast<const static_parameter_storage&>(*this)[index]);
	}
	template <class U>
	auto push_argument(U&& arg) -> std::enable_if_t<std::is_convertible<std::decay_t<U>, T>::value>
	{
		if (top - strg.params >= N)
			throw std::range_error("static_parameter_storage: buffer overflow");
		*(top++) = std::forward<U>(arg);
	}
	bool is_ready() const
	{
		return top == &strg.params[N] && this->is_ready_from<0>();
	}
private:
	template <std::size_t I, class = void>
	auto is_ready_from() const -> std::enable_if_t<(I >= N), bool>
	{
		return true;
	}
	template <std::size_t I, class = void>
	auto is_ready_from() const->std::enable_if_t<(I < N), bool>
	{
		return strg.params[I]->is_ready() && this->is_ready_from<I + 1>();
	}
};

template <class T>
class IToken
{
public:
	virtual T operator()() const = 0; //All derived classes must implement the same method with the same return type
	/*Push a required (as defined at runtime, perhaps erroneously which must be detected by implementations) number of arguments.
	Push arguments from first to last. Then call the operator() above.
	*/
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value) = 0;
	virtual std::shared_ptr<IToken<T>> simplify() const = 0;
	virtual std::size_t get_required_parameter_count() const = 0;
	virtual bool is_ready() const = 0; //all parameters are specified
	virtual ~IToken() {} //virtual d-tor is to allow correct destruction of polymorphic objects
	virtual TokenType type() = 0;
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const = 0;
	virtual short get_priority() = 0;
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx) = 0;
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const = 0;
    virtual constexpr std::string get_regex() {return "=";}
protected:
};

template <class T>
class Number : public IToken<T>
{
public:
    Number(): value(0) {}
	Number(T val) : value(val) {}
	Number(const Number<T>& num) = default;

    virtual constexpr std::string get_regex() {return "\\d+(\\.\\d+)?";}
	virtual T operator()() const
	{
		return value;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		return std::make_shared<Number<T>>(*this);
	}
	virtual bool is_ready() const
	{
		return true;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		throw std::logic_error("not implemented for number");
	}
	T operator+(const Number<T>& num) const
	{
		return this->value + num();
	}
	T operator-(const Number<T>& num) const
	{
		return this->value - num();
	}
	T operator*(const Number<T>& num) const
	{
		return this->value * num();
	}
	T operator/(const Number<T>& num) const
	{
		return this->value / num();
	}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		//do nothing for literals - they do not accept parameters. But the implementation (even empty) must be provided for polymorphic methods.
		//or throw an exception
#ifndef __CUDACC__
		throw std::invalid_argument("Unexpected call");
#endif //__CUDACC__
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 0;
	}
	virtual TokenType type()
	{
		return TokenType::number;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("There is no reverse operation for a number");
	}
	virtual short get_priority()
	{
		return 0;
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
private:
	T value;
};

template <class T>
class Boolean : public IToken<T>
{
public:
    Boolean() : value(true) {}
	Boolean(T val) : value(val) {};
	Boolean(const Boolean<T>& num) = default;

    virtual constexpr std::string get_regex() {return "(true|false)";}
	virtual T operator()() const
	{
		return value;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		return std::make_shared<Boolean<T>>(*this);
	}
	virtual bool is_ready() const
	{
		return true;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		throw std::logic_error("not implemented for boolean");
	}
	T operator!() const
	{
		return !this->value;
	}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		//do nothing for literals - they do not accept parameters. But the implementation (even empty) must be provided for polymorphic methods.
		//or throw an exception
#ifndef __CUDACC__
		throw std::invalid_argument("Unexpected call");
#endif //__CUDACC__
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 0;
	}
	virtual TokenType type()
	{
		return TokenType::boolean;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("There is no reverse operation for a number");
	}
	virtual short get_priority()
	{
		return 0;
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
protected:
private:
	bool value;
};

template <class T>
class Header;

template <class T>
class Variable : public IToken<T> //arguments of Header, e.g. F(x) x - Variable
{
	const T* m_pValue = nullptr;
	std::unique_ptr<char[]> name ;
	std::size_t name_length = 0;
	//bool isReady;
public:
    Variable() {}
	Variable(const char* varname, std::size_t len)
		:m_pValue(nullptr), name_length(len)
	{
		this->name = std::make_unique<char[]>(len + 1);
		std::strncpy(this->name.get(), varname, len);
		this->name[len] = 0;
	}
	Variable(Variable<T>&& val) = default;
	Variable(const Variable<T>& val) {
		*this = val;
	}
    virtual constexpr std::string get_regex() {return "^([a-zA-z]{1,})(\\_){0,}(\\d){0,}";}
	Variable& operator=(Variable<T>&& val) = default;
	Variable& operator=(const Variable<T>& val)
	{
		m_pValue = val.m_pValue;
		name_length = val.name_length;
		this->name = std::make_unique<char[]>(val.name_length + 1);
		std::strncpy(this->name.get(), val.name.get(), val.name_length);
		this->name[val.name_length] = 0;
		return *this;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		return std::make_shared<Variable<T>>(*this);
	}

	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		throw std::logic_error("not implemented for variable");
	}

	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("There is no reverse operation for a number");
	}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
#ifndef __CUDACC__
		throw std::invalid_argument("Unexpected call");
#endif //__CUDACC__
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 0;
	}
	virtual T operator()() const
	{
		return *m_pValue;
	}
	virtual bool is_ready() const
	{
		return true;
	}
	virtual TokenType type()
	{
		return TokenType::variable;
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		if (!strcmp((this->name).get(), target_variable))
			return std::make_shared<Variable<T>>(*this);
		return nullptr;
	}

	virtual short get_priority()
	{
		return 0;
	}
};

template <class T>
class Operator : public IToken<T>
{
    public:
        Operator(){}
    private:
	virtual void set_required_parameter_count(std::size_t value)
	{
		throw std::logic_error("Invalid operation");
	}

	virtual std::shared_ptr<IToken<T>> get_operand(std::size_t index) const
	{
		throw std::logic_error("Cannot get operand with index" + index);
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("There is no reverse operation for a number");
	}

	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		throw std::logic_error("not implemented for operator");
	}
};

template <class T>
class OperatorDiv;
template <class T>
class BinaryMinus;
template <class T>
class UnaryMinus;
template <class T>
class AtanFunction;
template <class T>
class AsinFunction;
template <class T>
class AcosFunction;

template <class T>
class Plus : public IToken<T>
{
    public:
    Plus() {}
    virtual constexpr std::string get_regex() {return "+";}
	virtual void set_required_parameter_count(std::size_t value)
	{
		throw std::logic_error("Invalid operation");
	}

	virtual std::shared_ptr<IToken<T>> get_operand(std::size_t index) const
	{
		throw std::logic_error("Cannot get operand with index" + index);
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("There is no reverse operation for a number");
	}

	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		throw std::logic_error("not implemented for operator");
	}
};
template <class T>
class Minus : public IToken<T>
{
    public:
    Minus() {}
    virtual constexpr std::string get_regex() {return "-";}
	virtual void set_required_parameter_count(std::size_t value)
	{
		throw std::logic_error("Invalid operation");
	}

	virtual std::shared_ptr<IToken<T>> get_operand(std::size_t index) const
	{
		throw std::logic_error("Cannot get operand with index" + index);
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("There is no reverse operation for a number");
	}

	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		throw std::logic_error("not implemented for operator");
	}
};

template <class T>
class UnaryPlus : public Plus<T>, public UnaryOperator //+-*/
{
	/*This replacement is unnecessary, but the code would be more maintainable, if the storage of parameters
	for functions (with fixed numbers of the parameters) will be managed in one place (static_parameter_storage). */
	static_parameter_storage<std::shared_ptr<IToken<T>>, 1> ops;

public:
	UnaryPlus() {}
	UnaryPlus(const static_parameter_storage<std::shared_ptr<IToken<T>>, 1> operands) : ops(operands) {}

    virtual constexpr std::string get_regex() {return "+";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		ops.push_argument(value);
	}
	virtual T operator()() const/*Implementation of IToken<T>::operator()()*/
	{
		if (!this->is_ready())
			throw std::logic_error("Invalid arguments of an unary plus operator.");

		return (*ops[0])();
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!this->is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		return ops[0]->simplify(); //unary + does no do anything
	}
	virtual bool is_ready() const
	{
		return ops.is_ready();
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 1;
	}
	virtual TokenType type()
	{
		return TokenType::UnaryPlus;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		//static_parameter_storage<std::shared_ptr<IToken<T>>, 1> operand(operands);
		return std::make_shared<UnaryPlus<T>>(*this);
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		static_parameter_storage<std::shared_ptr<IToken<T>>, 1> operands;
		operands.push_argument((ops[0].get())->transform(root, target_variable, target_operator_idx));

		if (this == root.get())
		{
			return operands[0];
		}
		else if (operands[0] != nullptr)
			return std::make_shared<UnaryPlus<T>>(*this);

		return nullptr;
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return ops[idx];
	}
};

template <class T>
class UnaryMinus : public Minus<T>, public UnaryOperator //+-*/
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 1> ops;

public:
	UnaryMinus() {}
	UnaryMinus(const static_parameter_storage<std::shared_ptr<IToken<T>>, 1> operands) : ops(operands) {}

    virtual constexpr std::string get_regex() {return "-";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		ops.push_argument(value);
	}
	virtual T operator()() const/*Implementation of IToken<T>::operator()()*/
	{
		if (!this->is_ready())
			throw std::logic_error("Invalid arguments of an unary minus operator.");

		return -(*ops[0])();
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!this->is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto op0 = ops[0]->simplify();

		if (op0->type() == TokenType::number)
			return std::make_shared<Number<T>>(-(*dynamic_cast<Number<T>*>(op0.get()))());
		auto op_new = std::make_shared<UnaryMinus<T>>(*this);
		op_new->push_argument(std::move(op0));
		return op_new;
	}
	virtual bool is_ready() const
	{
		return ops.is_ready();
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		//static_parameter_storage<std::shared_ptr<IToken<T>>, 1> operand(operands);
		return std::make_shared<UnaryMinus<T>>(*this);
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 1;
	}
	virtual TokenType type()
	{
		return TokenType::UnaryMinus;
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return ops[idx];
	}
};

template <class T>
class BinaryPlus : public Plus<T>, public CommutativeOperator //+-*/
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 2> ops;

public:
	BinaryPlus() {}
	BinaryPlus(const static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands) : ops(operands) {}

    virtual constexpr std::string get_regex() {return "+";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		ops.push_argument(value);
	}
	virtual T operator()() const/*Implementation of IToken<T>::operator()()*/
	{
		if (!this->is_ready())
			throw std::logic_error("Invalid arguments of a binary plus operator.");

		return (*ops[0])() + (*ops[1])();
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto op0 = ops[0]->simplify();
		auto op1 = ops[1]->simplify();

		if (op0->type() == TokenType::number && op1->type() == TokenType::number)
			return std::make_shared<Number<T>>((*op0)() + (*op1)());
		auto op_new = std::make_shared<BinaryPlus<T>>();
		op_new->push_argument(std::move(op0));
		op_new->push_argument(std::move(op1));
		return op_new;
	}
	virtual bool is_ready() const
	{
		return ops.is_ready();
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 2;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		static_parameter_storage<std::shared_ptr<IToken<T>>, 2> arguments(operands);
		return std::make_shared<BinaryMinus<T>>(arguments);
	}
	virtual TokenType type()
	{
		return TokenType::BinaryPlus;
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands;
		operands.push_argument((ops[0].get())->transform(root, target_variable, target_operator_idx));
		operands.push_argument((ops[1].get())->transform(root, target_variable, target_operator_idx));

		for (std::size_t i = 0; i < 2; ++i)
		{
			if (operands[i].get() != nullptr) {
				*target_operator_idx = i;
				if (this == root.get())
					return operands[i];
				else
					return std::make_shared<BinaryPlus<T>>(*this);
			}
		}

		return nullptr;
	}
	virtual short get_priority()
	{
		return 1;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return ops[idx];
	}
};
template <class T>
class BinaryMinus : public Minus<T>
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 2> ops;

public:
	BinaryMinus() {}
	BinaryMinus(const static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands) : ops(operands) {}
    virtual constexpr std::string get_regex() {return "-";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		ops.push_argument(value);
	}
	virtual T operator()() const/*Implementation of IToken<T>::operator()()*/
	{
		if (!this->is_ready())
			throw std::logic_error("Invalid arguments of a binary minus operator.");

		return  (*ops[1])() - (*ops[0])();
	}
	virtual bool is_ready() const
	{
		return ops.is_ready();
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 2;
	}
	virtual TokenType type()
	{
		return TokenType::BinaryMinus;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		if (target_idx == 1)
		{
			static_parameter_storage<std::shared_ptr<IToken<T>>, 2> reversed_operands;
			reversed_operands.push_argument(operands[1]);
			reversed_operands.push_argument(operands[0]);
			return std::make_shared<BinaryMinus<T>>(reversed_operands);
		}
		else if (target_idx == 0) 
		{
			static_parameter_storage<std::shared_ptr<IToken<T>>, 2> arguments(operands);
			return std::make_shared<BinaryPlus<T>>(arguments);
		}
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands;
		operands.push_argument((ops[0].get())->transform(root, target_variable, target_operator_idx));
		operands.push_argument((ops[1].get())->transform(root, target_variable, target_operator_idx));

		for (std::size_t i = 0; i < 2; ++i)
		{
			if (operands[i].get() != nullptr) {
				*target_operator_idx = i;
				if (this == root.get())
					return operands[i];
				else
					return std::make_shared<BinaryMinus<T>>(*this);
			}
		}

		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto op0 = ops[0]->simplify();
		auto op1 = ops[1]->simplify();

		if (op0->type() == TokenType::number && op1->type() == TokenType::number)
			return std::make_shared<Number<T>>((*op1)() - (*op0)());
		auto op_new = std::make_shared<BinaryMinus<T>>();
		op_new->push_argument(std::move(op0));
		op_new->push_argument(std::move(op1));
		return op_new;
	}
	virtual short get_priority()
	{
		return 1;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return ops[idx];
	}
};
template <class T>
class OperatorMul : public Operator<T>, public CommutativeOperator
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 2> ops;

public:
	OperatorMul() {}
	OperatorMul(const static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands) : ops(operands) {}
    virtual constexpr std::string get_regex() {return "*";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		ops.push_argument(value);
	}
	virtual T operator()() const
	{
		if (!this->is_ready())
			throw std::logic_error("Invalid arguments of a multiplication operator.");

		return (*ops[0])() * (*ops[1])();
	}
	virtual bool is_ready() const
	{
		return ops.is_ready();
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 2;
	}
	virtual TokenType type()
	{
		return TokenType::operatorMul;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		static_parameter_storage<std::shared_ptr<IToken<T>>, 2> arguments(operands);
		return std::make_shared<OperatorDiv<T>>(arguments);
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return ops[idx];
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands;
		operands.push_argument((ops[0].get())->transform(root, target_variable, target_operator_idx));
		operands.push_argument((ops[1].get())->transform(root, target_variable, target_operator_idx));

		for (std::size_t i = 0; i < 2; ++i)
		{
			if (operands[i].get() != nullptr) {
				*target_operator_idx = i;
				if (this == root.get())
					return operands[i];
				else
					return std::make_shared<OperatorMul<T>>(*this);
			}
		}

		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto op0 = ops[0]->simplify();
		auto op1 = ops[1]->simplify();

		if (op0->type() == TokenType::number && op1->type() == TokenType::number)
			return std::make_shared<Number<T>>((*op0)() * (*op1)());
		auto op_new = std::make_shared<OperatorMul<T>>();
		op_new->push_argument(std::move(op0));
		op_new->push_argument(std::move(op1));
		return op_new;
	}
};
template <class T>
class OperatorDiv : public Operator<T>
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 2> ops;

public:
	OperatorDiv() {}
	OperatorDiv(const static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands) : ops(operands) {}
    virtual constexpr std::string get_regex() {return "/";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		ops.push_argument(value);
	}
	virtual T operator()() const
	{
		if (!this->is_ready())
			throw std::logic_error("Invalid arguments of a division operator.");

		return (*ops[1])() / (*ops[0])();
	}
	virtual bool is_ready() const
	{
		return ops.is_ready();
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 2;
	}
	virtual TokenType type()
	{
		return TokenType::operatorDiv;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		if (target_idx == 1)
		{
			static_parameter_storage<std::shared_ptr<IToken<T>>, 2> reversed_operands;
			reversed_operands.push_argument(operands[1]);
			reversed_operands.push_argument(operands[0]);
			return std::make_shared<OperatorDiv<T>>(reversed_operands);
		}
		else if (target_idx == 0)
		{
			static_parameter_storage<std::shared_ptr<IToken<T>>, 2> arguments(operands);
			return std::make_shared<OperatorMul<T>>(arguments);
		}
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands;
		operands.push_argument((ops[0].get())->transform(root, target_variable, target_operator_idx));
		operands.push_argument((ops[1].get())->transform(root, target_variable, target_operator_idx));

		for (std::size_t i = 0; i < 2; ++i)
		{
			if (operands[i].get() != nullptr) {
				*target_operator_idx = i;
				if (this == root.get())
					return operands[i];
				else
					return std::make_shared<OperatorDiv<T>>(*this);
			}
		}

		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto op0 = ops[0]->simplify();
		auto op1 = ops[1]->simplify();

		if (op0->type() == TokenType::number && op1->type() == TokenType::number)
			return std::make_shared<Number<T>>((*op1)() / (*op0)());
		auto op_new = std::make_shared<OperatorDiv<T>>();
		op_new->push_argument(std::move(op0));
		op_new->push_argument(std::move(op1));
		return op_new;
	}

	virtual std::shared_ptr<IToken<T>> get_operand(std::size_t index) const
	{
		return ops[index];
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return ops[idx];
	}
};
template <class T>
class OperatorPow : public Operator<T>
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 2> ops;

public:
	OperatorPow() {}
	OperatorPow(const static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands) : ops(operands) {}
    virtual constexpr std::string get_regex() {return "^";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		ops.push_argument(value);
	}
	virtual T operator()() const
	{
		if (!this->is_ready())
			throw std::logic_error("Invalid arguments of a power operator.");

		return std::pow((*ops[0])(), (*ops[1])());
	}
	virtual bool is_ready() const
	{
		return ops.is_ready();
	}
	virtual short get_priority()
	{
		return 3;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 2;
	}
	virtual TokenType type()
	{
		return TokenType::operatorPow;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("");
		//return TokenType::sqrtFunction; //TODO: operator root or log
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto op0 = ops[0]->simplify();
		auto op1 = ops[1]->simplify();

		if (op0->type() == TokenType::number && op1->type() == TokenType::number)
			return std::make_shared<Number<T>>(std::pow((*op1)(), (*op0)()));
		auto op_new = std::make_shared<OperatorPow<T>>();
		op_new->push_argument(std::move(op0));
		op_new->push_argument(std::move(op1));
		return op_new;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return ops[idx];
	}
};
template <class T>
class OperatorNotEqual;

template <class T>
class OperatorEqual : public Operator<T>
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 2> ops;

public:
	OperatorEqual() {}
	OperatorEqual(const static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands) : ops(operands) {}
    virtual constexpr std::string get_regex() {return "==";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		ops.push_argument(value);
	}
	virtual T operator()() const
	{
		if (!this->is_ready())
			throw std::logic_error("Invalid arguments of a equal operator.");

		return (*ops[1])() == (*ops[0])();
	}
	virtual bool is_ready() const
	{
		return ops.is_ready();
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 2;
	}
	virtual TokenType type()
	{
		return TokenType::operatorEqual;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
        static_parameter_storage<std::shared_ptr<IToken<T>>, 2> reversed_operands;
		reversed_operands.push_argument(operands[1]);
		reversed_operands.push_argument(operands[0]);
		return std::make_shared<OperatorNotEqual<T>>(reversed_operands);
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		/*static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands;
		operands.push_argument((ops[0].get())->transform(root, target_variable, target_operator_idx));
		operands.push_argument((ops[1].get())->transform(root, target_variable, target_operator_idx));

		for (std::size_t i = 0; i < 2; ++i)
		{
			if (operands[i].get() != nullptr) {
				*target_operator_idx = i;
				if (this == root.get())
					return operands[i];
				else
					return std::make_shared<OperatorDiv<T>>(*this);
			}
		}
        */
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto op0 = ops[0]->simplify();
		auto op1 = ops[1]->simplify();

		if (op0->type() == TokenType::number && op1->type() == TokenType::number)
			return std::make_shared<Number<T>>((*op1)() / (*op0)());
		auto op_new = std::make_shared<OperatorEqual<T>>();
		op_new->push_argument(std::move(op0));
		op_new->push_argument(std::move(op1));
		return op_new;
	}

	virtual std::shared_ptr<IToken<T>> get_operand(std::size_t index) const
	{
		return ops[index];
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return ops[idx];
	}
};

template <class T>
class OperatorNotEqual : public Operator<T>
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 2> ops;

public:
	OperatorNotEqual() {}
	OperatorNotEqual(const static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands) : ops(operands) {}
    virtual constexpr std::string get_regex() {return "!=";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		ops.push_argument(value);
	}
	virtual T operator()() const
	{
		if (!this->is_ready())
			throw std::logic_error("Invalid arguments of a not equal operator.");

		return (*ops[1])() != (*ops[0])();
	}
	virtual bool is_ready() const
	{
		return ops.is_ready();
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 2;
	}
	virtual TokenType type()
	{
		return TokenType::operatorNotEqual;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
        static_parameter_storage<std::shared_ptr<IToken<T>>, 2> reversed_operands;
		reversed_operands.push_argument(operands[1]);
		reversed_operands.push_argument(operands[0]);
		return std::make_shared<OperatorEqual<T>>(reversed_operands);
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		/*static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands;
		operands.push_argument((ops[0].get())->transform(root, target_variable, target_operator_idx));
		operands.push_argument((ops[1].get())->transform(root, target_variable, target_operator_idx));

		for (std::size_t i = 0; i < 2; ++i)
		{
			if (operands[i].get() != nullptr) {
				*target_operator_idx = i;
				if (this == root.get())
					return operands[i];
				else
					return std::make_shared<OperatorDiv<T>>(*this);
			}
		}
        */
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto op0 = ops[0]->simplify();
		auto op1 = ops[1]->simplify();

		if (op0->type() == TokenType::number && op1->type() == TokenType::number)
			return std::make_shared<Number<T>>((*op1)() / (*op0)());
		auto op_new = std::make_shared<OperatorNotEqual<T>>();
		op_new->push_argument(std::move(op0));
		op_new->push_argument(std::move(op1));
		return op_new;
	}

	virtual std::shared_ptr<IToken<T>> get_operand(std::size_t index) const
	{
		return ops[index];
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return ops[idx];
	}
};

template<class T>
class OperatorLess;

template <class T>
class OperatorGreater : public Operator<T>
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 2> ops;

public:
	OperatorGreater() {}
	OperatorGreater(const static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands) : ops(operands) {}
    virtual constexpr std::string get_regex() {return ">";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		ops.push_argument(value);
	}
	virtual T operator()() const
	{
		if (!this->is_ready())
			throw std::logic_error("Invalid arguments of a greater operator.");

		return (*ops[1])() > (*ops[0])();
	}
	virtual bool is_ready() const
	{
        return ops.is_ready();
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 2;
	}
	virtual TokenType type()
	{
		return TokenType::operatorGreater;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
        static_parameter_storage<std::shared_ptr<IToken<T>>, 2> reversed_operands;
		reversed_operands.push_argument(operands[1]);
		reversed_operands.push_argument(operands[0]);
		return std::make_shared<OperatorLess<T>>(reversed_operands);
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		/*static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands;
		operands.push_argument((ops[0].get())->transform(root, target_variable, target_operator_idx));
		operands.push_argument((ops[1].get())->transform(root, target_variable, target_operator_idx));

		for (std::size_t i = 0; i < 2; ++i)
		{
			if (operands[i].get() != nullptr) {
				*target_operator_idx = i;
				if (this == root.get())
					return operands[i];
				else
					return std::make_shared<OperatorDiv<T>>(*this);
			}
		}
        */
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto op0 = ops[0]->simplify();
		auto op1 = ops[1]->simplify();

		if (op0->type() == TokenType::number && op1->type() == TokenType::number)
			return std::make_shared<Number<T>>((*op1)() / (*op0)());
		auto op_new = std::make_shared<OperatorGreater<T>>();
		op_new->push_argument(std::move(op0));
		op_new->push_argument(std::move(op1));
		return op_new;
	}

	virtual std::shared_ptr<IToken<T>> get_operand(std::size_t index) const
	{
		return ops[index];
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return ops[idx];
	}
};

template <class T>
class OperatorLess : public Operator<T>
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 2> ops;

public:
	OperatorLess() {}
	OperatorLess(const static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands) : ops(operands) {}
    virtual constexpr std::string get_regex() {return "<";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		ops.push_argument(value);
	}
	virtual T operator()() const
	{
		if (!this->is_ready())
			throw std::logic_error("Invalid arguments of a less operator.");

		return (*ops[1])() < (*ops[0])();
	}
	virtual bool is_ready() const
	{
		return ops.is_ready();
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 2;
	}
	virtual TokenType type()
	{
		return TokenType::operatorLess;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
        static_parameter_storage<std::shared_ptr<IToken<T>>, 2> reversed_operands;
		reversed_operands.push_argument(operands[1]);
		reversed_operands.push_argument(operands[0]);
		return std::make_shared<OperatorGreater<T>>(reversed_operands);
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		/*static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands;
		operands.push_argument((ops[0].get())->transform(root, target_variable, target_operator_idx));
		operands.push_argument((ops[1].get())->transform(root, target_variable, target_operator_idx));

		for (std::size_t i = 0; i < 2; ++i)
		{
			if (operands[i].get() != nullptr) {
				*target_operator_idx = i;
				if (this == root.get())
					return operands[i];
				else
					return std::make_shared<OperatorDiv<T>>(*this);
			}
		}
        */
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto op0 = ops[0]->simplify();
		auto op1 = ops[1]->simplify();

		if (op0->type() == TokenType::number && op1->type() == TokenType::number)
			return std::make_shared<Number<T>>((*op1)() / (*op0)());
		auto op_new = std::make_shared<OperatorLess<T>>();
		op_new->push_argument(std::move(op0));
		op_new->push_argument(std::move(op1));
		return op_new;
	}

	virtual std::shared_ptr<IToken<T>> get_operand(std::size_t index) const
	{
		return ops[index];
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return ops[idx];
	}
};

template<class T>
class OperatorLessEqual;

template <class T>
class OperatorGreaterEqual : public Operator<T>
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 2> ops;

public:
	OperatorGreaterEqual() {}
	OperatorGreaterEqual(const static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands) : ops(operands) {}
    virtual constexpr std::string get_regex() {return ">=";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		ops.push_argument(value);
	}
	virtual T operator()() const
	{
		if (!this->is_ready())
			throw std::logic_error("Invalid arguments of a greater_equal operator.");

		return (*ops[1])() >= (*ops[0])();
	}
	virtual bool is_ready() const
	{
		return ops.is_ready();
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 2;
	}
	virtual TokenType type()
	{
		return TokenType::operatorGreaterEqual;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
        static_parameter_storage<std::shared_ptr<IToken<T>>, 2> reversed_operands;
		reversed_operands.push_argument(operands[1]);
		reversed_operands.push_argument(operands[0]);
		return std::make_shared<OperatorLessEqual<T>>(reversed_operands);
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		/*static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands;
		operands.push_argument((ops[0].get())->transform(root, target_variable, target_operator_idx));
		operands.push_argument((ops[1].get())->transform(root, target_variable, target_operator_idx));

		for (std::size_t i = 0; i < 2; ++i)
		{
			if (operands[i].get() != nullptr) {
				*target_operator_idx = i;
				if (this == root.get())
					return operands[i];
				else
					return std::make_shared<OperatorDiv<T>>(*this);
			}
		}
        */
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto op0 = ops[0]->simplify();
		auto op1 = ops[1]->simplify();

		if (op0->type() == TokenType::number && op1->type() == TokenType::number)
			return std::make_shared<Number<T>>((*op1)() / (*op0)());
		auto op_new = std::make_shared<OperatorGreaterEqual<T>>();
		op_new->push_argument(std::move(op0));
		op_new->push_argument(std::move(op1));
		return op_new;
	}

	virtual std::shared_ptr<IToken<T>> get_operand(std::size_t index) const
	{
		return ops[index];
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return ops[idx];
	}
};
template <class T>
class OperatorLessEqual : public Operator<T>
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 2> ops;

public:
	OperatorLessEqual() {}
	OperatorLessEqual(const static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands) : ops(operands) {}
    virtual constexpr std::string get_regex() {return "<=";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		ops.push_argument(value);
	}
	virtual T operator()() const
	{
		if (!this->is_ready())
			throw std::logic_error("Invalid arguments of a greater operator.");

		return (*ops[1])() <= (*ops[0])();
	}
	virtual bool is_ready() const
	{
		return ops.is_ready();
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 2;
	}
	virtual TokenType type()
	{
		return TokenType::operatorLessEqual;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
        static_parameter_storage<std::shared_ptr<IToken<T>>, 2> reversed_operands;
		reversed_operands.push_argument(operands[1]);
		reversed_operands.push_argument(operands[0]);
		return std::make_shared<OperatorLessEqual<T>>(reversed_operands);
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		/*static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands;
		operands.push_argument((ops[0].get())->transform(root, target_variable, target_operator_idx));
		operands.push_argument((ops[1].get())->transform(root, target_variable, target_operator_idx));

		for (std::size_t i = 0; i < 2; ++i)
		{
			if (operands[i].get() != nullptr) {
				*target_operator_idx = i;
				if (this == root.get())
					return operands[i];
				else
					return std::make_shared<OperatorDiv<T>>(*this);
			}
		}
        */
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto op0 = ops[0]->simplify();
		auto op1 = ops[1]->simplify();

		if (op0->type() == TokenType::number && op1->type() == TokenType::number)
			return std::make_shared<Number<T>>((*op1)() / (*op0)());
		auto op_new = std::make_shared<OperatorLessEqual<T>>();
		op_new->push_argument(std::move(op0));
		op_new->push_argument(std::move(op1));
		return op_new;
	}

	virtual std::shared_ptr<IToken<T>> get_operand(std::size_t index) const
	{
		return ops[index];
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return ops[idx];
	}
};
template <class T>
class OperatorAssign : public Operator<T>
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 2> ops;

public:
	OperatorAssign() {}
	OperatorAssign(const static_parameter_storage<std::shared_ptr<IToken<T>>, 2> operands) : ops(operands) {}
	
    virtual constexpr std::string get_regex() {return "<-";}
    virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		ops.push_argument(value);
	}
	virtual T operator()() const
	{
		if (!this->is_ready())
			throw std::logic_error("Invalid arguments of an assign operator.");

		*ops[0] = *ops[1];
        return 0;
	}
	virtual bool is_ready() const
	{
		return ops.is_ready();
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 2;
	}
	virtual TokenType type()
	{
		return TokenType::operatorAssign;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
        //TODO: how to simplify <-
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto op0 = ops[0]->simplify();
		auto op1 = ops[1]->simplify();

		if (op0->type() == TokenType::number && op1->type() == TokenType::number)
			return std::make_shared<Number<T>>((*op1)() / (*op0)());
		auto op_new = std::make_shared<OperatorLessEqual<T>>();
		op_new->push_argument(std::move(op0));
		op_new->push_argument(std::move(op1));
		return op_new;
	}


	virtual std::shared_ptr<IToken<T>> get_operand(std::size_t index) const
	{
		return ops[index];
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return ops[idx];
	}
};

template <class T>
class Function : public IToken<T> //sin,cos...
{
public:
	virtual void set_required_parameter_count(std::size_t value)
	{
		throw std::logic_error("Invalid operation");
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		throw std::logic_error("Cannot transform abstract function");
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("There is no reverse operation for a number");
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		throw std::logic_error("not implemented for function");
	}
};

template <class T>
class SqrtnFunction : public Function<T>
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 2> ops;
public:
    SqrtnFunction() {}

    virtual constexpr std::string get_regex() {return "sqrtn";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		ops.push_argument(value);
	}
	virtual T operator()() const/*Implementation of IToken<T>::operator()()*/
	{
		if (!this->is_ready())
			throw std::logic_error("Insufficient number are given for the sqrtn function.");

		return std::pow((*ops[1])(), -(*ops[0])());
	}
	virtual bool is_ready() const
	{
		return ops.is_ready();
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 2;
	}
	virtual const char* get_function_name() const
	{
		return "sqrtn";
	}
	virtual TokenType type()
	{
		return TokenType::sqrtnFunction;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		return nullptr;
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto op0 = ops[0]->simplify();
		auto op1 = ops[1]->simplify();

		if (op0->type() == TokenType::number && op1->type() == TokenType::number)
			return std::make_shared<Number<T>>(std::pow((*op0)(), (*op1)()));

		auto pNewTkn = std::make_shared<SqrtnFunction<T>>();
		pNewTkn->push_argument(std::move(op0));
		pNewTkn->push_argument(std::move(op1));
		return pNewTkn;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return ops[idx];
	}
};

template <class T>
class SqrtFunction : public Function<T>, public UnaryOperator
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 1> ops;
public:
    SqrtFunction() {}
    virtual constexpr std::string get_regex() {return "sqrt";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		ops.push_argument(value);
	}
	virtual T operator()() const/*Implementation of IToken<T>::operator()()*/
	{
		if (!this->is_ready())
			throw std::logic_error("Insufficient number are given for the sqrt function.");

		return std::sqrt((*ops[0])());
	}
	virtual bool is_ready() const
	{
		return ops.is_ready();
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 1;
	}
	virtual const char* get_function_name() const
	{
		return "sqrt";
	}
	virtual TokenType type()
	{
		return TokenType::sqrtFunction;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		return nullptr;
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto newarg = ops[0]->simplify();
		if (newarg->type() == TokenType::number)
			return std::make_shared<Number<T>>(std::sqrt((*newarg)()));
		auto pNewTkn = std::make_shared<SqrtFunction<T>>();
		pNewTkn->push_argument(std::move(newarg));
		return pNewTkn;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return ops[idx];
	}
}; 

template <class T>
class SinFunction : public Function<T>, public UnaryOperator
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 1> op;
public:
	SinFunction() {}
	SinFunction(const static_parameter_storage<std::shared_ptr<IToken<T>>, 1> operands) : op(operands) {}
    virtual constexpr std::string get_regex() {return "sin";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		op.push_argument(value);
	}
	virtual T operator()() const/*Implementation of IToken<T>::operator()()*/
	{
		if (!this->is_ready())
			throw std::logic_error("Insufficient number are given for the sin function.");

		return std::sin((*op[0])());
	}
	virtual bool is_ready() const
	{
		return op[0]->is_ready();
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 1;
	}
	virtual const char* get_function_name() const
	{
		return "sin";
	}
	virtual TokenType type()
	{
		return TokenType::sinFunction;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		return std::make_shared<AsinFunction<T>>(operands);
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto newarg = op[0]->simplify();
		if (newarg->type() == TokenType::number)
			return std::make_shared<Number<T>>(std::sin((*newarg)()));
		auto pNewTkn = std::make_shared<SinFunction<T>>();
		pNewTkn->push_argument(std::move(newarg));
		return pNewTkn;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return op[idx];
	}
};
template <class T>
class AsinFunction : public Function<T>, public UnaryOperator
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 1> op;
public:
	AsinFunction() {}
	AsinFunction(const static_parameter_storage<std::shared_ptr<IToken<T>>, 1> operands) : op(operands) {}
    virtual constexpr std::string get_regex() {return "asin";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		op.push_argument(value);
	}
	virtual T operator()() const/*Implementation of IToken<T>::operator()()*/
	{
		if (!this->is_ready())
			throw std::logic_error("Insufficient number are given for the sin function.");

		return std::asin((*op[0])());
	}
	virtual bool is_ready() const
	{
		return op[0]->is_ready();
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 1;
	}
	virtual const char* get_function_name() const
	{
		return "asin";
	}
	virtual TokenType type()
	{
		return TokenType::asinFunction;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		return std::make_shared<SinFunction<T>>(operands);
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto newarg = op[0]->simplify();
		if (newarg->type() == TokenType::number)
			return std::make_shared<Number<T>>(std::sin((*newarg)()));
		auto pNewTkn = std::make_shared<AsinFunction<T>>();
		pNewTkn->push_argument(std::move(newarg));
		return pNewTkn;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return op[idx];
	}
}; 

template <class T>
class CosFunction : public Function<T>, public UnaryOperator
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 1> op;
public:
	CosFunction() {}
	CosFunction(const static_parameter_storage<std::shared_ptr<IToken<T>>, 1> operands) : op(operands) {}
    virtual constexpr std::string get_regex() {return "cos";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		op.push_argument(value);
	}
	virtual T operator()() const /*Implementation of IToken<T>::operator()()*/
	{
		if (!op[0]->is_ready())
			throw std::logic_error("Insufficient number are given for the cos function.");

		return std::cos((*op[0])());
	}
	virtual bool is_ready() const
	{
		return op[0]->is_ready();
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 1;
	}
	virtual const char* get_function_name() const
	{
		return "cos";
	}
	virtual TokenType type()
	{
		return TokenType::cosFunction;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		return std::make_shared<AcosFunction<T>>(operands);
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto newarg = op[0]->simplify();
		if (newarg->type() == TokenType::number)
			return std::make_shared<Number<T>>(std::cos((*newarg)()));
		auto pNewTkn = std::make_shared<CosFunction<T>>();
		pNewTkn->op[0] = std::move(newarg);
		return pNewTkn;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return op[idx];
	}
};

template <class T>
class AcosFunction : public Function<T>, public UnaryOperator
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 1> op;
public:
	AcosFunction() {}
	AcosFunction(const static_parameter_storage<std::shared_ptr<IToken<T>>, 1> operands) : op(operands) {}
    virtual constexpr std::string get_regex() {return "acos";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		op.push_argument(value);
	}
	virtual T operator()() const /*Implementation of IToken<T>::operator()()*/
	{
		if (!op[0]->is_ready())
			throw std::logic_error("Insufficient number are given for the cos function.");

		return std::acos((*op[0])());
	}
	virtual bool is_ready() const
	{
		return op[0]->is_ready();
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 1;
	}
	virtual const char* get_function_name() const
	{
		return "acos";
	}
	virtual TokenType type()
	{
		return TokenType::acosFunction;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		return std::make_shared<CosFunction<T>>(operands);
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto newarg = op[0]->simplify();
		if (newarg->type() == TokenType::number)
			return std::make_shared<Number<T>>(std::cos((*newarg)()));
		auto pNewTkn = std::make_shared<AcosFunction<T>>();
		pNewTkn->op[0] = std::move(newarg);
		return pNewTkn;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return op[idx];
	}
};

template <class T>
class TanFunction : public Function<T>, public UnaryOperator
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 1> op;
public:
	TanFunction() {}
	TanFunction(const static_parameter_storage<std::shared_ptr<IToken<T>>, 1> operands) : op(operands) {}
    virtual constexpr std::string get_regex() {return "tan";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		op.push_argument(value);
	}
	virtual T operator()() const /*Implementation of IToken<T>::operator()()*/
	{
		if (!op[0]->is_ready())
			throw std::logic_error("Insufficient number are given for the tg function.");

		return std::tan((*op[0])());
	}
	virtual bool is_ready() const
	{
		return op[0]->is_ready();
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 1;
	}
	virtual const char* get_function_name() const
	{
		return "tan";
	}
	virtual TokenType type()
	{
		return TokenType::tanFunction;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		return std::make_shared<AtanFunction<T>>(operands);
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto newarg = op[0]->simplify();
		if (newarg->type() == TokenType::number)
			return std::make_shared<Number<T>>(std::tan((*newarg)()));
		auto pNewTkn = std::make_shared<TanFunction<T>>();
		pNewTkn->op[0] = std::move(newarg);
		return pNewTkn;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return op[idx];
	}
};
template <class T>
class AtanFunction : public Function<T>, public UnaryOperator
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 1> op;
public:
	AtanFunction() {}
	AtanFunction(const static_parameter_storage<std::shared_ptr<IToken<T>>, 1> operands) : op(operands) {}
    virtual constexpr std::string get_regex() {return "atan";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		op.push_argument(value);
	}
	virtual T operator()() const /*Implementation of IToken<T>::operator()()*/
	{
		if (!op[0]->is_ready())
			throw std::logic_error("Insufficient number are given for the tg function.");

		return std::atan((*op[0])());
	}
	virtual bool is_ready() const
	{
		return op[0]->is_ready();
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 1;
	}
	virtual const char* get_function_name() const
	{
		return "atan";
	}
	virtual TokenType type()
	{
		return TokenType::atanFunction;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		return std::make_shared<AtanFunction<T>>(operands);
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto newarg = op[0]->simplify();
		if (newarg->type() == TokenType::number)
			return std::make_shared<Number<T>>(std::tan((*newarg)()));
		auto pNewTkn = std::make_shared<AtanFunction<T>>();
		pNewTkn->op[0] = std::move(newarg);
		return pNewTkn;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return op[idx];
	}
};

////////// not ready

template <class T>
class Log10Function : public Function<T>, public UnaryOperator
{
	std::shared_ptr<IToken<T>> op;
public:
    Log10Function() {}
    virtual constexpr std::string get_regex() {return "log10";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		op = value;
	}
	virtual T operator()() const /*Implementation of IToken<T>::operator()()*/
	{
		if (!op->is_ready())
			throw std::logic_error("Insufficient number are given for the log10 function.");

		return std::log10((*op)());
	}
	virtual bool is_ready() const
	{
		return op->is_ready();
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 1;
	}
	virtual const char* get_function_name() const
	{
		return "log10";
	}
	virtual TokenType type()
	{
		return TokenType::log10Function;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		return std::make_shared<OperatorPow<T>>(operands); //TODO: pow to 10
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto newarg = op->simplify();
		if (newarg->type() == TokenType::number)
			return std::make_shared<Number<T>>(std::log10((*newarg)()));
		auto pNewTkn = std::make_shared<Log10Function<T>>();
		pNewTkn->op = std::move(newarg);
		return pNewTkn;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return op;
	}
};
template <class T>
class LnFunction : public Function<T>, public UnaryOperator
{
	std::shared_ptr<IToken<T>> op;
public:
    LnFunction() {}
    virtual constexpr std::string get_regex() {return "ln";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		op = value;
	}
	virtual T operator()() const /*Implementation of IToken<T>::operator()()*/
	{
		if (!op->is_ready())
			throw std::logic_error("Insufficient number are given for the ln function.");

		return std::log((*op)());
	}
	virtual bool is_ready() const
	{
		return op->is_ready();
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 1;
	}
	virtual const char* get_function_name() const
	{
		return "ln";
	}
	virtual TokenType type()
	{
		return TokenType::lnFunction;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		return std::make_shared<OperatorPow<T>>(operands); //TODO: pow e
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto newarg = op->simplify();
		if (newarg->type() == TokenType::number)
			return std::make_shared<Number<T>>(std::log((*newarg)()));
		auto pNewTkn = std::make_shared<LnFunction<T>>();
		pNewTkn->op = std::move(newarg);
		return pNewTkn;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return op;
	}
};
template <class T>
class LogFunction : public Function<T>
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 2> ops;
public:
    LogFunction() {}
    virtual constexpr std::string get_regex() {return "log";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		ops.push_argument(value);
	}
	virtual T operator()() const /*Implementation of IToken<T>::operator()()*/
	{
		if (!this->is_ready())
			throw std::logic_error("Insufficient number are given for the log function.");

		return std::log((*ops[1])()) / std::log((*ops[0])());
	}
	virtual bool is_ready() const
	{
		return ops.is_ready();
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 2;
	}
	virtual const char* get_function_name() const
	{
		return "log";
	}
	virtual TokenType type()
	{
		return TokenType::logFunction;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		return std::make_shared<OperatorPow<T>>(operands); //TODO: pow arbitrary
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto op0 = ops[0]->simplify();
		auto op1 = ops[1]->simplify();

		if (op0->type() == TokenType::number && op1->type() == TokenType::number)
			return std::make_shared<Number<T>>(std::log((*op1)()) / std::log((*op0)()));
		auto op_new = std::make_shared<LogFunction<T>>();
		op_new->push_argument(std::move(op0));
		op_new->push_argument(std::move(op1));
		return op_new;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return ops[idx];
	}
};
template <class T>
class JnFunction : public Function<T>
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 2> ops;
public:
    JnFunction() {}
    virtual constexpr std::string get_regex() {return "jn";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		ops.push_argument(value);
	}
	virtual T operator()() const /*Implementation of IToken<T>::operator()()*/
	{
		if (!this->is_ready())
			throw std::logic_error("Insufficient number are given for the jn function.");

		return JN(int((*ops[0])()), (*ops[1])());
	}
	virtual bool is_ready() const
	{
		return ops.is_ready();
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 2;
	}
	virtual const char* get_function_name() const
	{
		return "jn";
	}
	virtual TokenType type()
	{
		return TokenType::jnFunction;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("There is no reverse operation for jn");
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto op0 = ops[0]->simplify();
		auto op1 = ops[1]->simplify();

		if (op0->type() == TokenType::number && op1->type() == TokenType::number)
			return std::make_shared<Number<T>>(JN(int((*ops[0])()), (*ops[1])()));
		auto op_new = std::make_shared<JnFunction<T>>();
		op_new->push_argument(std::move(op0));
		op_new->push_argument(std::move(op1));
		return op_new;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return ops[idx];
	}
};
template <class T>
class J0Function : public Function<T>, public UnaryOperator
{
	std::shared_ptr<IToken<T>> op;
public:
    J0Function() {}
    virtual constexpr std::string get_regex() {return "j0";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		op = value;
	}
	virtual T operator()() const /*Implementation of IToken<T>::operator()()*/
	{
		if (!op->is_ready())
			throw std::logic_error("Insufficient number are given for the j0 function.");

		return J0((*op)());
	}
	virtual bool is_ready() const
	{
		return op->is_ready();
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 1;
	}
	virtual const char* get_function_name() const
	{
		return "j0";
	}
	virtual TokenType type()
	{
		return TokenType::j0Function;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("There is no reverse operation for j0");
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto newarg = op->simplify();
		if (newarg->type() == TokenType::number)
			return std::make_shared<Number<T>>(J0((*newarg)()));
		auto pNewTkn = std::make_shared<J0Function<T>>();
		pNewTkn->op = std::move(newarg);
		return pNewTkn;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return op;
	}
};
template <class T>
class J1Function : public Function<T>, public UnaryOperator
{
	std::shared_ptr<IToken<T>> op;
public:
    J1Function() {}
    virtual constexpr std::string get_regex() {return "j1";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		op = value;
	}
	virtual T operator()() const /*Implementation of IToken<T>::operator()()*/
	{
		if (!op->is_ready())
			throw std::logic_error("Insufficient number are given for the j1 function.");

		return J1((*op)());
	}
	virtual bool is_ready() const
	{
		return op->is_ready();
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 1;
	}
	virtual const char* get_function_name() const
	{
		return "j1";
	}
	virtual TokenType type()
	{
		return TokenType::j1Function;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("There is no reverse operation for j1");
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto newarg = op->simplify();
		if (newarg->type() == TokenType::number)
			return std::make_shared<Number<T>>(J1((*newarg)()));
		auto pNewTkn = std::make_shared<J1Function<T>>();
		pNewTkn->op = std::move(newarg);
		return pNewTkn;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return op;
	}
};
template <class T>
class YnFunction : public Function<T>
{
	static_parameter_storage<std::shared_ptr<IToken<T>>, 2> ops;
public:
    YnFunction() {}
    virtual constexpr std::string get_regex() {return "yn";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		ops.push_argument(value);
	}
	virtual T operator()() const /*Implementation of IToken<T>::operator()()*/
	{
		if (!this->is_ready())
			throw std::logic_error("Insufficient number are given for the yn function.");

		return YN(int((*ops[0])()), (*ops[1])());
	}
	virtual bool is_ready() const
	{
		return ops.is_ready();
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 2;
	}
	virtual const char* get_function_name() const
	{
		return "yn";
	}
	virtual TokenType type()
	{
		return TokenType::ynFunction;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("There is no reverse operation for yn");
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto op0 = ops[0]->simplify();
		auto op1 = ops[1]->simplify();

		if (op0->type() == TokenType::number && op1->type() == TokenType::number)
			return std::make_shared<Number<T>>(YN(int((*ops[0])()), (*ops[1])()));
		auto op_new = std::make_shared<YnFunction<T>>();
		op_new->push_argument(std::move(op0));
		op_new->push_argument(std::move(op1));
		return op_new;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return ops[idx];
	}
};
template <class T>
class Y0Function : public Function<T>, public UnaryOperator
{
	std::shared_ptr<IToken<T>> op;
public:
    Y0Function() {}
    virtual constexpr std::string get_regex() {return "y0";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		op = value;
	}
	virtual T operator()() const /*Implementation of IToken<T>::operator()()*/
	{
		if (!op->is_ready())
			throw std::logic_error("Insufficient number are given for the y0 function.");

		return Y0((*op)());
	}
	virtual bool is_ready() const
	{
		return op->is_ready();
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 1;
	}
	virtual const char* get_function_name() const
	{
		return "y0";
	}
	virtual TokenType type()
	{
		return TokenType::y0Function;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("There is no reverse operation for y0");
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto newarg = op->simplify();
		if (newarg->type() == TokenType::number)
			return std::make_shared<Number<T>>(Y0((*newarg)()));
		auto pNewTkn = std::make_shared<Y0Function<T>>();
		pNewTkn->op = std::move(newarg);
		return pNewTkn;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return op;
	}
};
template <class T>
class Y1Function : public Function<T>, public UnaryOperator
{
	std::shared_ptr<IToken<T>> op;
public:
    Y1Function() {}
    virtual constexpr std::string get_regex() {return "y1";}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		op = value;
	}
	virtual T operator()() const /*Implementation of IToken<T>::operator()()*/
	{
		if (!op->is_ready())
			throw std::logic_error("Insufficient number are given for the y1 function.");

		return Y1((*op)());
	}
	virtual bool is_ready() const
	{
		return op->is_ready();
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return 1;
	}
	virtual const char* get_function_name() const
	{
		return "y1";
	}
	virtual TokenType type()
	{
		return TokenType::y1Function;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("There is no reverse operation for y1");
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify an operator");
		auto newarg = op->simplify();
		if (newarg->type() == TokenType::number)
			return std::make_shared<Number<T>>(Y1((*newarg)()));
		auto pNewTkn = std::make_shared<Y1Function<T>>();
		pNewTkn->op = std::move(newarg);
		return pNewTkn;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return op;
	}
};

template <class T, class Implementation, class TokenBinPredicate>
class ExtremumFunction : public Function<T>, public CommutativeOperator
{
	std::vector<std::shared_ptr<IToken<T>>> ops;
	std::size_t nRequiredParamsCount = 0;
	TokenBinPredicate m_pred;
public:
	ExtremumFunction() = default;
	ExtremumFunction(std::size_t paramsNumber) : nRequiredParamsCount(paramsNumber) {}

	//not used, but in case a state is needed by the definition of the predicate:
	template <class Predicate, class = std::enable_if_t<std::is_constructible<TokenBinPredicate, Predicate&&>::value>>
	ExtremumFunction(std::size_t paramsNumber, Predicate&& pred) : nRequiredParamsCount(paramsNumber), m_pred(std::forward<Predicate>(pred)) {}

	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		ops.push_back(value);
	}
	virtual T operator()() const /*Implementation of IToken<T>::operator()()*/
	{
		if (!this->is_ready())
			throw std::logic_error("Insufficient number are given for the extremum function.");

		return (*std::min_element(ops.begin(), ops.end(), m_pred)).get()->operator()();
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("There is no reverse operation for extremum");
	}
	virtual bool is_ready() const
	{
		if (ops.size() != nRequiredParamsCount)
			return false;
		for (auto op = ops.begin(); op != ops.end(); ++op)
		{
			if (!op->get()->is_ready())
				return false;
		}
		return true;
	}
	virtual short get_priority()
	{
		return 2;
	}
	virtual std::size_t get_required_parameter_count() const
	{
		return nRequiredParamsCount;
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		if (!is_ready())
			throw std::logic_error("Not ready to simplify");
		std::vector<std::shared_ptr<IToken<T>>> newargs;
		newargs.reserve(ops.size());
		std::vector<std::shared_ptr<IToken<T>>> newargsVar;
		newargsVar.reserve(ops.size());

		for (const auto& op : ops)
		{
			auto newarg = op->simplify();
			if (newarg->type() == TokenType::number)
				newargs.push_back(newarg);
			else
				newargsVar.push_back(newarg);
		}
		if (newargsVar.empty())
			return *std::min_element(newargs.begin(), newargs.end(), m_pred);

		std::shared_ptr<Implementation> pNewTkn;
		if (newargs.empty())
			pNewTkn = std::make_shared<Implementation>(Implementation(newargsVar.size()));
		else
		{
			pNewTkn = std::make_shared<Implementation>(Implementation(newargsVar.size() + 1));
			pNewTkn->push_argument(*std::min_element(newargs.begin(), newargs.end(), m_pred));
		}
		for (const auto& op : newargsVar)
			pNewTkn->push_argument(op);
		return pNewTkn;
	}
	void set_required_parameter_count(std::size_t value)
	{
		nRequiredParamsCount = value;
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		return ops[idx];
	}
};

template <class T>
struct TokenLess
{
	bool operator()(const std::shared_ptr<IToken<T>>& left, const std::shared_ptr<IToken<T>>& right) const
	{
		return (*left)() < (*right)();
	};
};

template <class T>
struct TokenGreater
{
	bool operator()(const std::shared_ptr<IToken<T>>& left, const std::shared_ptr<IToken<T>>& right) const
	{
		return (*left)() > (*right)();
	};
};

template <class T>
class MaxFunction : public ExtremumFunction<T, MaxFunction<T>, TokenGreater<T>>
{
	typedef ExtremumFunction<T, MaxFunction<T>, TokenGreater<T>> MyBase;
public:
	using MyBase::ExtremumFunction; //c-tor inheritance
    MaxFunction() {}
    virtual constexpr std::string get_regex() {return "max";}
	virtual const char* get_function_name() const
	{
		return "max";
	}
	virtual TokenType type()
	{
		return TokenType::maxFunction;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("");
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
};
template <class T>
class MinFunction : public ExtremumFunction<T, MinFunction<T>, TokenLess<T>>
{
	typedef ExtremumFunction<T, MinFunction<T>, TokenLess<T>> MyBase;
public:
	using MyBase::ExtremumFunction; //c-tor inheritance
    MinFunction() {}
    virtual constexpr std::string get_regex() {return "min";}
	virtual const char* get_function_name() const
	{
		return "min";
	}
	virtual TokenType type()
	{
		return TokenType::minFunction;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("Error");
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
};

template <class T>
class Bracket : public Operator<T> //,' '()
{
public:
	Bracket() = default;

    virtual constexpr std::string get_regex() {return "(\\(|\\))";}
	virtual T operator()() const
	{
		return true;
	}

	virtual bool is_ready() const
	{
		return true;
	}

	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		return; //openingBracket = value; //true is for opening bracket, false is for closing.
	}
	virtual short get_priority()
	{
		return -1;
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual TokenType type()
	{
		return TokenType::bracket;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("There is no reverse operation for a bracket");
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		//return std::make_shared<Bracket<T>>(nullptr);
		throw std::logic_error("Unexpected call");
	}
	std::size_t get_required_parameter_count() const
	{
		return 0;
	}
	void set_required_parameter_count(std::size_t value)
	{
		throw std::logic_error("Invalid operation");
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		throw std::logic_error("not implemented for bracket");
	}
};


template <class T>
class OperatorBracket : public Bracket<T> //begin_if - end_if and so on
{
public:
	OperatorBracket() = default;

    virtual constexpr std::string get_regex() {return "(\\(|\\))";}
	virtual T operator()() const
	{
		return true;
	}

	virtual bool is_ready() const
	{
		return true;
	}

	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		return; //openingBracket = value; //true is for opening bracket, false is for closing.
	}
	virtual short get_priority()
	{
		return -1;
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual TokenType type()
	{
		return TokenType::bracket;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("There is no reverse operation for a bracket");
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		//return std::make_shared<Bracket<T>>(nullptr);
		throw std::logic_error("Unexpected call");
	}
	std::size_t get_required_parameter_count() const
	{
		return 0;
	}
	void set_required_parameter_count(std::size_t value)
	{
		throw std::logic_error("Invalid operation");
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		throw std::logic_error("not implemented for bracket");
	}
};

template <class T>
class Semicolon : public Operator<T> //,' '()
{
public:
	Semicolon() = default;

    virtual constexpr std::string get_regex() {return ";";}
	virtual T operator()() const
	{
		return true;
	}
	virtual bool is_ready() const
	{
		return true;
	}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		return;
	}
	virtual short get_priority()
	{
		return -1;
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual TokenType type()
	{
		return TokenType::bracket;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("There is no reverse operation for a semicolon");
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		throw std::logic_error("Unexpected call");
	}
	std::size_t get_required_parameter_count() const
	{
		return 0;
	}
	void set_required_parameter_count(std::size_t value)
	{
		throw std::logic_error("Invalid operation");
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		throw std::logic_error("not implemented for semicolon");
	}
};

template <class T>
class Loop : public Operator<T> //for, while
{
public:

	virtual T operator()() const
	{
		return true;
	}
	virtual bool is_ready() const
	{
		return true;
	}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		return;
	}
	virtual short get_priority()
	{
		return -1;
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual TokenType type()
	{
		return TokenType::bracket;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("There is no reverse operation for a semicolon");
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		throw std::logic_error("Unexpected call");
	}
	std::size_t get_required_parameter_count() const
	{
		return 0;
	}
	void set_required_parameter_count(std::size_t value)
	{
		throw std::logic_error("Invalid operation");
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		throw std::logic_error("not implemented for semicolon");
	}
};
template <class T>
class Condition : public Operator<T> //if, else, switch?, case?
{
public:

	virtual T operator()() const
	{
		return true;
	}
	virtual bool is_ready() const
	{
		return true;
	}
	virtual void push_argument(const std::shared_ptr<IToken<T>>& value)
	{
		return;
	}
	virtual short get_priority()
	{
		return -1;
	}
	virtual std::shared_ptr<IToken<T>> transform(std::shared_ptr<IToken<T>> root, const char* target_variable, std::size_t* target_operator_idx) const
	{
		return nullptr;
	}
	virtual TokenType type()
	{
		return TokenType::bracket;
	}
	virtual std::shared_ptr<IToken<T>> get_reverse_operator(const std::vector<std::shared_ptr<IToken<T>>> operands, std::size_t target_idx = -1) const
	{
		throw std::logic_error("There is no reverse operation for a semicolon");
	}
	virtual std::shared_ptr<IToken<T>> simplify() const
	{
		throw std::logic_error("Unexpected call");
	}
	std::size_t get_required_parameter_count() const
	{
		return 0;
	}
	void set_required_parameter_count(std::size_t value)
	{
		throw std::logic_error("Invalid operation");
	}
	virtual std::shared_ptr<IToken<T>> get_param(std::size_t idx)
	{
		throw std::logic_error("not implemented for semicolon");
	}
};
template <class T>
class ExprGraphNode 
{
	std::vector<std::pair<std::size_t, std::size_t>> coordinates;
	std::shared_ptr<IToken<T>> value;
public:
	ExprGraphNode(std::shared_ptr<IToken<T>> _value, std::pair<std::size_t, std::size_t> _coordinates) :
		value(_value), coordinates(_coordinates) {}

	void set_coordinates(std::size_t idx, std::pair<std::size_t, std::size_t> _value)
	{
		coordinates[idx] = _value;
	}
	void push_argument(std::pair<std::size_t, std::size_t> _value)
	{
		coordinates.push_back(_value);
	}
	void push_argument(std::vector<std::pair<std::size_t, std::size_t>> _value)
	{
		coordinates = _value;
	}
	void set_value(T _value)
	{
		value = _value;
	}
	std::shared_ptr<IToken<T>> get_value() const
	{
		return value;
	}
	//���� ����� ����� � ������ �����, ����� ����� ������ � �� ���������
	//TODO: ��������� ��������������� � �����������������
	bool operator==(const ExprGraphNode& rhs) 
	{
		auto is_args_equals = false;
		if (is_commutative<T>() || is_unary<T>())
			is_args_equals = std::is_permutation(this->coordinates.begin(), this->coordinates.end(),
				rhs.coordinates.begin(), rhs.coordinates.end());
		else
			is_args_equals = std::equal(this->coordinates.begin(), this->coordinates.end(),
				rhs.coordinates.begin(), rhs.coordinates.end());
		return is_args_equals && this->value == rhs.value;
	}
};

template <class T>
class ExprGraphMatrix 
{
public:
	template <class ElementType>
	auto push_element(const std::shared_ptr<IToken<T>>& value) ->
		std::enable_if_t <std::is_same_v<Variable<ElementType>, T> ||
		std::is_same_v<Number<ElementType>, T>, void>
	{
		std::size_t row = (value.get())->get_priority();
		std::vector<std::size_t> elements = find_element(value);

		auto element_coordinates = std::make_pair<std::size_t, std::size_t>(row, matrix[row].size());
		if (elements.empty())
			matrix[row].push_back(new ExprGraphNode<T>(value, element_coordinates));
		variable_order.push(element_coordinates);
	}
	template <class ElementType>
	auto push_element(const std::shared_ptr<IToken<T>>& value) ->
		std::enable_if_t<std::is_base_of<Operator<ElementType>, T>::value ||
		std::is_base_of<Function<ElementType>, T>::value, void>
	{
		std::size_t row = (value.get())->get_priority();
		auto element_coordinates = std::make_pair<std::size_t, std::size_t>(row, matrix[row].size());
		auto new_operator = new ExprGraphNode<T>(value, element_coordinates);
		////���� ��������� �������� value ������ ��� ����� ���������� ��������� �������� � ������� ��������,
		////����� ���������� �������� ��������� � �������� (��. ��� 8)
		if (row <= operation_order.front().first/*.get_priority()*/)
		{
			if (!variable_order.empty())
			{
				auto coordinate = operation_order.front();
				std::vector<std::pair<std::size_t, std::size_t>> op_arguments;
				for (auto i = 0; i < variable_order.size(); ++i)
                {
					op_arguments.push_back(variable_order.top());
                    variable_order.pop();
                }
				matrix[coordinate.first][coordinate.second].push_argument(op_arguments);
			}
			if (variable_order.empty() && operation_order.size() > 1)
			{
				//���� ��� ����������/�����, � ��������� ��� ����, �� ��� ��������� ������ ���������
				new_operator.push_argument(new std::vector<std::size_t, std::size_t>(operation_order.rend(),
					operation_order.rend() - value->get_required_parameter_count()));
			}

		}
		std::vector<std::size_t> elements = find_element(new_operator);

		if (elements.empty())
			matrix[row].push_back(new_operator);
		operation_order.push_back(element_coordinates);
	}
	std::vector<std::size_t> find_element(const std::shared_ptr<IToken<T>>& value)
	{
		std::size_t row = (value.get())->get_priority();
		std::vector<std::size_t> elements;

		for (std::size_t i = 0; i < matrix[row].size(); ++i) 
		{
			if (matrix[row][i].get_value() == value)
				elements.push_back(i);
		}
        return elements;
	}
	std::vector<std::size_t> find_element(const ExprGraphNode<T>& value)
	{
		std::size_t row = (value.get())->get_priority();
		std::vector<std::size_t> elements;

		for (std::size_t i = 0; i < matrix[row].size(); ++i) 
		{
			if (matrix[row][i] == value)
				elements.push_back(i);
		}
        return elements;
	}
private:
	std::vector<std::vector<ExprGraphNode<T>>> matrix;
	std::vector<std::pair<std::size_t, std::size_t>> operation_order;

	//��������������� ��������� ��� ����������; ��������� � ������������ �������
	std::stack<std::pair<std::size_t, std::size_t>> variable_order;

	void push_argument(std::pair<std::size_t, std::size_t> operand,
		std::vector<std::pair<std::size_t, std::size_t>> argument)
	{
		matrix[operand.first][operand.second].push_argument(argument);
	}

	void push_argument(std::pair<std::size_t, std::size_t> operand,
		std::pair<std::size_t, std::size_t> argument)
	{
		matrix[operand.first][operand.second].push_argument(argument);
	}
};

template <class T>
class TokenStorage
{
	std::stack<std::shared_ptr<IToken<T>>> operationStack;
	std::list<std::shared_ptr<IToken<T>>> outputList;

public:

	template <class TokenParamType>
	auto push_token(TokenParamType&& op) -> std::enable_if_t<
		std::is_base_of<Operator<T>, std::decay_t<TokenParamType>>::value ||
		std::is_base_of<Function<T>, std::decay_t<TokenParamType>>::value,
		IToken<T>*
	>
	{
		//not ready
		auto my_priority = op.get_priority();
		while (operationStack.size() != 0 && my_priority <= operationStack.top()->get_priority() && op.type() != TokenType::bracket)
		{
			outputList.push_back(operationStack.top());
			operationStack.pop();
		}
		operationStack.push(std::make_shared<std::decay_t<TokenParamType>>(std::forward<TokenParamType>(op)));
		return operationStack.top().get();
	}

	template <class TokenParamType>
	auto push_token(TokenParamType&& value)->std::enable_if_t<
		!(std::is_base_of<Operator<T>, std::decay_t<TokenParamType>>::value ||
			std::is_base_of<Function<T>, std::decay_t<TokenParamType>>::value ||
			std::is_base_of<Bracket<T>, std::decay_t<TokenParamType>>::value),
		IToken<T>*
	>
	{
		//not ready
		outputList.push_back(std::make_shared<std::decay_t<TokenParamType>>(std::forward<TokenParamType>(value)));
		return outputList.back().get();
	}

	void pop_bracket()
	{
		bool isOpeningBracket = false;
		while (operationStack.size() != 0)
		{
			if (operationStack.top().get()->type() != TokenType::bracket)
			{
				this->outputList.push_back(operationStack.top());
				operationStack.pop();
			}
			else
			{
				isOpeningBracket = true;
				break;
			}
		}
		if (!isOpeningBracket)
			throw std::invalid_argument("There is no opening bracket!");
		else
			operationStack.pop();

		//return Bracket<T>(); //: operationStack.top().get();
	}
	std::list<std::shared_ptr<IToken<T>>>&& finalize() &&
	{
		while (operationStack.size() != 0)
		{
			if (operationStack.top().get()->type() == TokenType::bracket) //checking enclosing brackets
				throw std::invalid_argument("Enclosing bracket!");
			else
			{
				outputList.push_back(std::move(operationStack.top()));
				operationStack.pop();
			}
		}
		return std::move(outputList);
	}
	std::shared_ptr<IToken<T>>& get_top_operation()
	{
		return operationStack.top();
	}
	void comma_parameter_replacement()
	{
		bool isOpeningBracket = false;

		while (!isOpeningBracket && operationStack.size() != 0) //while an opening bracket is not found or an operation stack is not empty
		{
			if (operationStack.top().get()->type() != TokenType::bracket) //if the cast to Bracket is not successfull, return NULL => it is not '('
			{
				outputList.push_back(operationStack.top());
				operationStack.pop();
			}
			else
			{
				isOpeningBracket = true;
			}
		}

		if (!isOpeningBracket) //missing '('
			throw std::invalid_argument("There is no opening bracket!");
	}
};

template <class T>
class Header
{
	std::map<std::string, T> m_arguments;
	std::vector<std::string> m_parameters;
	std::string function_name;
	bool isReady = false;
public:
	Header() = default;
	Header(const char* expression, std::size_t expression_len, char** endPtr)
	{
		char* begPtr = (char*)(expression);
		std::list<std::string> params;

		bool isOpeningBracket = false;
		bool isClosingBracket = false;
		std::size_t commaCount = 0;

		while (*begPtr != '=' && begPtr < expression + expression_len)
		{
			if (isalpha(*begPtr))
			{
				auto l_endptr = begPtr + 1;
				for (; isalnum(*l_endptr); ++l_endptr);
				if (this->function_name.empty())
					this->function_name = std::string(begPtr, l_endptr);
				else
				{
					if (!isOpeningBracket)
						throw std::invalid_argument("Unexpected token"); //parameter outside of parenthesis
					auto param_name = std::string(begPtr, l_endptr);
					if (!m_arguments.emplace(param_name, T()).second)
						throw std::invalid_argument("Parameter " + param_name + " is not unique!"); //duplicated '('
					params.emplace_back(std::move(param_name));
				}
				begPtr = l_endptr;
			}

			if (*begPtr == ' ')
			{
				begPtr += 1;
				continue;
			}

			if (*begPtr == '(')
			{
				if (isOpeningBracket)
					throw std::invalid_argument("ERROR!"); //duplicated '('
				isOpeningBracket = true;
				begPtr += 1;
			}

			if (*begPtr == ',') //a-zA_Z0-9
			{
				commaCount += 1;
				begPtr += 1;
			}

			if (*begPtr == ')')
			{
				if (!isOpeningBracket)
					throw std::invalid_argument("ERROR!"); //missing ')'
				if (isClosingBracket)
					throw std::invalid_argument("ERROR!"); //dublicated ')'
				isClosingBracket = true;
				begPtr += 1;
			}
		}
		m_parameters.reserve(params.size());
		for (auto& param : params)
			m_parameters.emplace_back(std::move(param));
		*endPtr = begPtr;
	}
	Header(const Header<T>& val)
	{
		std::size_t size = val.get_name_length();
		this->function_name = val.function_name;
		this->m_arguments = val.m_arguments;
		this->m_parameters = val.m_parameters;
		isReady = true;
	}
	virtual bool is_ready() const
	{
		return isReady;
	}
	void push_argument(const char* name, std::size_t parameter_name_size, const T& value)
	{
		auto it = m_arguments.find(std::string(name, name + parameter_name_size));
		if (it == m_arguments.end())
			throw std::invalid_argument("Header parameter is not found");
		it->second = value;
	}
	const T& get_argument(const char* parameter_name, std::size_t parameter_name_size) const //call this from Variable::operator()().
	{
		auto it = m_arguments.find(std::string(parameter_name, parameter_name + parameter_name_size));
		if (it == m_arguments.end())
			throw std::invalid_argument("Parameter is not found");
		return it->second;
	}
	T& get_argument(const char* parameter_name, std::size_t parameter_name_size) //call this from Variable::operator()().
	{
		return const_cast<T&>(const_cast<const Header<T>*>(this)->get_argument(parameter_name, parameter_name_size));
	}
	const T& get_argument_by_index(std::size_t index) const //call this from Variable::operator()().
	{
		return this->get_argument(m_parameters[index].c_str(), m_parameters[index].size());
	}
	T& get_argument_by_index(std::size_t index) //call this from Variable::operator()().
	{
		return this->get_argument(m_parameters[index].c_str(), m_parameters[index].size());
	}
	std::size_t get_required_parameter_count() const
	{
		return m_parameters.size();
	}
	const char* get_function_name() const
	{
		return function_name.c_str();
	}
	size_t get_name_length() const
	{
		return function_name.size();
	}
	/*const std::vector<std::string>& get_params_vector() const
	{
		return m_parameters;
	}*/
	std::size_t get_param_index(const std::string& param_name)
	{
		for (std::size_t i = 0; i < this->m_parameters.size(); ++i)
		{
			if (this->m_parameters[i] == param_name)
				return i;
		}
		throw std::invalid_argument("Parameter not found");
	}
	std::shared_ptr<IToken<T>> get_header_expression() const
	{
		return std::make_shared<Variable<T>>(function_name.c_str(), function_name.size());
	}

	Header(Header&&) = default;
	Header& operator=(Header&&) = default;
};

//TODO::abstract factory for tokens
template <class T>
class Command 
{
    public:
        virtual TokenType execute() = 0;
        virtual ~Command() {}
};

template<class T>
class NumberCommand : public Command<T>
{
    public:
        virtual ~NumberCommand() {}
        virtual TokenType execute() 
        {
			static_assert(std::is_same<T, double>::value, "The following line is only applicable to double");
			return TokenType::number;
        }
};

template <class T>
class Mathexpr
{
public:
	Mathexpr(Header<T> m_header, std::shared_ptr<IToken<T>> m_body) : header(m_header), body(m_body) {}
	Mathexpr(const char* sMathExpr, std::size_t cbMathExpr);
	Mathexpr(const char* szMathExpr) :Mathexpr(szMathExpr, std::strlen(szMathExpr)) {}
	template <class Traits, class Alloc>
	Mathexpr(const std::basic_string<char, Traits, Alloc>& strMathExpr) : Mathexpr(strMathExpr.c_str(), strMathExpr.size()) {}
	T compute() const
	{
		//auto result = body;
		//simplify_body(result);
		//if (result.size() != 1)
		//	throw std::logic_error("Invalid expression");
		return (*body)();
	}
	void init_variables(const std::vector<T>& parameters)
	{
		if (parameters.size() < header.get_required_parameter_count())
			throw std::invalid_argument("Count of arguments < " + header.get_required_parameter_count());
		for (std::size_t iArg = 0; iArg < header.get_required_parameter_count(); ++iArg)
			header.get_argument_by_index(iArg) = parameters[iArg];
	}

	std::shared_ptr<IToken<T>> transformation(const char* target_parameter, std::size_t parameter_size)
	{
		auto pBody = body;
		auto header_expression = header.get_header_expression();
		std::size_t* target_param_idx = new std::size_t();
		try {
			auto param = header.get_argument(target_parameter, parameter_size);

			while ((pBody.get())->type() != TokenType::variable || (pBody.get())->type() != TokenType::variable)
			{
				auto target_variable_operator = (pBody.get())->transform(pBody, target_parameter, target_param_idx);
				auto another_operator_idx = (pBody.get())->get_required_parameter_count() - 1 - *target_param_idx;
				auto res = reverse_operation(pBody, another_operator_idx, header_expression);
				header_expression = res;
				pBody = target_variable_operator;
			}
		}
		catch (std::exception e)
		{
			return nullptr;
		}
		return header_expression;
		//for (auto i = body.get(); i != (body.get())->end(); ++i)
		//{
		//	*i;
		//}
	}

	//void clear_variables(); With the map referencing approach this method is not necessary anymore because if we need to reuse the expression
	//with different arguments, we just reassign them with init_variables

private:
	Header<T> header;
	std::shared_ptr<IToken<T>> body;

    std::vector<IToken<T>*> allowed_tokens {new Variable<T>(), new OperatorAssign<T>(), new Number<T>(), new Boolean<T>(), new UnaryPlus<T>(),
    new UnaryMinus<T>(), new BinaryPlus<T>(), new BinaryMinus<T>(), new OperatorMul<T>(), new OperatorDiv<T>(),
    new OperatorPow<T>(), new SqrtFunction<T>(), new SqrtnFunction<T>(), new SinFunction<T>(), new AsinFunction<T>(),
    new CosFunction<T>(), new AcosFunction<T>(), new TanFunction<T>(), new AtanFunction<T>(), new Log10Function<T>(),
    new LnFunction<T>(), new LogFunction<T>(), new J0Function<T>(), new J1Function<T>(), new JnFunction<T>(),
    new Y0Function<T>(), new Y1Function<T>(), new YnFunction<T>(), new MinFunction<T>(), new MaxFunction<T>(),
    /*new OperatorAssign<T>(),*/ new OperatorGreater<T>(), new OperatorLess<T>(), new OperatorGreaterEqual<T>(),
    new OperatorLessEqual<T>(), new OperatorEqual<T>(), new OperatorNotEqual<T>(), new Semicolon<T>()};

//let's define token as a word.
//One operator: = + - * / ^ ( ) ,
//Or: something that only consists of digits and one comma
//Or: something that starts with a letter and proceeds with letters or digits

token_string_entity parse_token(std::string& expression)
{
	auto pStart = skipSpaces(expression.c_str(), expression.length());
    auto new_length = expression.length() - (pStart - expression.c_str());
    std::string text(pStart, new_length);
    auto text_end = -1;
    std::size_t i = 0;
    while(i < allowed_tokens.size())
    {
        text_end = filter_by_regex(text, allowed_tokens[i]->get_regex());
        ++i;
        if (text_end != -1)
            break;
    }
    
    if (text_end == -1)
        throw std::logic_error("Parse token: Unexpected token");

    return token_string_entity(pStart, pStart + text_end);
}
std::shared_ptr<IToken<T>> reverse_operation(std::shared_ptr<IToken<T>> operation,
		std::size_t reverse_param_index, std::shared_ptr<IToken<T>> header)
{
		std::shared_ptr<IToken<T>> reverse_param = (operation.get())->get_param(reverse_param_index);
		TokenType type = (operation.get())->type();
		std::vector<std::shared_ptr<IToken<T>>> operands;

		operands.push_back(reverse_param);
		operands.push_back(header);

		auto new_operator = (operation.get())->get_reverse_operator(operands, reverse_param_index);
		return new_operator;
	}

	std::list<std::shared_ptr<IToken<T>>> lexBody(const char* expr, std::size_t length)
	{
		char* begPtr = (char*)expr;
		std::size_t cbRest = length;
        std::string expression(expr, cbRest);
		TokenStorage<T> tokens;
		IToken<T> *pLastToken = nullptr;
		std::stack <std::pair<std::shared_ptr<Function<T>>, std::size_t>> funcStack;
		int last_type_id = -1;
		std::list<std::shared_ptr<IToken<T>>> formula;

		while (expression.length() > 0)
		{
			auto tkn = parse_token(expression);
			if (tkn == "+")
			{
				if (last_type_id == -1 || IsOperatorTokenTypeId(TokenType(last_type_id))) //unary form
					last_type_id = int(tokens.push_token(UnaryPlus<T>())->type());
				else //binary form
					last_type_id = int(tokens.push_token(BinaryPlus<T>())->type());
			}
			else if (tkn == "-")
			{
				if (last_type_id == -1 || IsOperatorTokenTypeId(TokenType(last_type_id))) //unary form
					last_type_id = int(tokens.push_token(UnaryMinus<T>())->type());
				else //binary form
					last_type_id = int(tokens.push_token(BinaryMinus<T>())->type());
			}
			else if (tkn == "*")
				last_type_id = int(tokens.push_token(OperatorMul<T>())->type());
			else if (tkn == "/")
				last_type_id = int(tokens.push_token(OperatorDiv<T>())->type());
			else if (tkn == "^")
				last_type_id = int(tokens.push_token(OperatorPow<T>())->type());
			else if (tkn == ",")
			{
				tokens.comma_parameter_replacement();

				if (funcStack.top().first.get()->type() == TokenType::maxFunction ||
					funcStack.top().first.get()->type() == TokenType::minFunction)
					funcStack.top().second += 1;
			}
			else if (isdigit(*tkn.begin()))
			{
				char* conversion_end;
				static_assert(std::is_same<T, double>::value, "The following line is only applicable to double");
				auto value = std::strtod(tkn.begin(), (char**)&conversion_end);
				if (conversion_end != tkn.end())
					std::logic_error("Invalid syntax");
				last_type_id = int(tokens.push_token(Number<T>(value))->type());
			}
			else if (isalpha(*tkn.begin()))
			{
				if (tkn == "sqrt")
				{
					last_type_id = int(tokens.push_token(SqrtFunction<T>())->type());
					funcStack.push(std::make_pair(std::make_shared<SqrtFunction<T>>(), 1));
				}
				else if (tkn == "sqrtn")
				{
					last_type_id = int(tokens.push_token(SqrtnFunction<T>())->type());
					funcStack.push(std::make_pair(std::make_shared<SqrtnFunction<T>>(), 2));
				}
				else if (tkn == "sin")
				{
					last_type_id = int(tokens.push_token(SinFunction<T>())->type());
					funcStack.push(std::make_pair(std::make_shared<SinFunction<T>>(), 1));
				}
				else if (tkn == "cos")
				{
					last_type_id = int(tokens.push_token(CosFunction<T>())->type());
					funcStack.push(std::make_pair(std::make_shared<CosFunction<T>>(), 1));
				}
				else if (tkn == "tan")
				{
					last_type_id = int(tokens.push_token(TanFunction<T>())->type());
					funcStack.push(std::make_pair(std::make_shared<TanFunction<T>>(), 1));
				}
				else if (tkn == "asin")
				{
					last_type_id = int(tokens.push_token(AsinFunction<T>())->type());
					funcStack.push(std::make_pair(std::make_shared<AsinFunction<T>>(), 1));
				}
				else if (tkn == "acos")
				{
					last_type_id = int(tokens.push_token(AcosFunction<T>())->type());
					funcStack.push(std::make_pair(std::make_shared<AcosFunction<T>>(), 1));
				}
				else if (tkn == "atan")
				{
					last_type_id = int(tokens.push_token(AtanFunction<T>())->type());
					funcStack.push(std::make_pair(std::make_shared<AtanFunction<T>>(), 1));
				}
				else if (tkn == "log10")
				{
					last_type_id = int(tokens.push_token(Log10Function<T>())->type());
					funcStack.push(std::make_pair(std::make_shared<Log10Function<T>>(), 1));
				}
				else if (tkn == "ln")
				{
					last_type_id = int(tokens.push_token(LnFunction<T>())->type());
					funcStack.push(std::make_pair(std::make_shared<LnFunction<T>>(), 1));
				}
				else if (tkn == "log")
				{
					last_type_id = int(tokens.push_token(LogFunction<T>())->type());
					funcStack.push(std::make_pair(std::make_shared<LogFunction<T>>(), 2));
				}
				else if (tkn == "j0")
				{
					last_type_id = int(tokens.push_token(J0Function<T>())->type());
					funcStack.push(std::make_pair(std::make_shared<J0Function<T>>(), 1));
				}
				else if (tkn == "j1")
				{
					last_type_id = int(tokens.push_token(J1Function<T>())->type());
					funcStack.push(std::make_pair(std::make_shared<J1Function<T>>(), 1));
				}
				else if (tkn == "jn")
				{
					last_type_id = int(tokens.push_token(JnFunction<T>())->type());
					funcStack.push(std::make_pair(std::make_shared<JnFunction<T>>(), 2));
				}
				else if (tkn == "y0")
				{
					last_type_id = int(tokens.push_token(Y0Function<T>())->type());
					funcStack.push(std::make_pair(std::make_shared<Y0Function<T>>(), 1));
				}
				else if (tkn == "y1")
				{
					last_type_id = int(tokens.push_token(Y1Function<T>())->type());
					funcStack.push(std::make_pair(std::make_shared<Y1Function<T>>(), 1));
				}
				else if (tkn == "yn")
				{
					last_type_id = int(tokens.push_token(YnFunction<T>())->type());
					funcStack.push(std::make_pair(std::make_shared<YnFunction<T>>(), 2));
				}
				else if (tkn == "max")
				{
					last_type_id = int(tokens.push_token(MaxFunction<T>())->type());
					funcStack.push(std::make_pair(std::make_shared<MaxFunction<T>>(), 0));
				}
				else if (tkn == "min")
				{
					last_type_id = int(tokens.push_token(MinFunction<T>())->type());
					funcStack.push(std::make_pair(std::make_shared<MinFunction<T>>(), 0));
				}
				else  
                {
					last_type_id = int(tokens.push_token(Variable<T>(tkn.begin(), tkn.size()))->type());
			    }
            }
			else if (tkn == ")")
			{
				tokens.pop_bracket();
				if (!funcStack.empty()) {
					switch (funcStack.top().first.get()->type())
					{
					case TokenType::maxFunction:
						static_cast<MaxFunction<T>&>(*tokens.get_top_operation()) = MaxFunction<T>(funcStack.top().second + 1);
						break;
					case TokenType::minFunction:
						static_cast<MinFunction<T>&>(*tokens.get_top_operation()) = MinFunction<T>(funcStack.top().second + 1);
						break;
					}
					funcStack.pop();
				}
			}
			else if (tkn == "(")
			{
				last_type_id = int(tokens.push_token(Bracket<T>())->type());
			}
			else if (iscompare(*tkn.begin()))
            {
                if (*(tkn.begin() + 1) == '-') //assign symbol <-
				{
					last_type_id = int(tokens.push_token(OperatorAssign<T>())->type());
					//funcStack.push(std::make_pair(std::make_shared<OperatorAssign<T>>(), 2));
                }
                else if (tkn == "<") 
				{
					last_type_id = int(tokens.push_token(OperatorLess<T>())->type());
					//funcStack.push(std::make_pair(std::make_shared<OperatorLess<T>>(), 2));
                }
                else if (tkn == ">") 
				{
					last_type_id = int(tokens.push_token(OperatorGreater<T>())->type());
					//funcStack.push(std::make_pair(std::make_shared<OperatorGreater<T>>(), 2));
                }
                else if (tkn == ">=") 
				{
					last_type_id = int(tokens.push_token(OperatorGreaterEqual<T>())->type());
					//funcStack.push(std::make_pair(std::make_shared<OperatorGreaterEqual<T>>(), 2));
                }
                else if (tkn == "<=") 
				{
					last_type_id = int(tokens.push_token(OperatorLessEqual<T>())->type());
					//funcStack.push(std::make_pair(std::make_shared<OperatorLessEqual<T>>(), 2));
                }
                else if (tkn == "==") 
				{
					last_type_id = int(tokens.push_token(OperatorEqual<T>())->type());
					//funcStack.push(std::make_pair(std::make_shared<OperatorEqual<T>>(), 2));
                }
                else if (tkn == "!=") 
				{
					last_type_id = int(tokens.push_token(OperatorNotEqual<T>())->type());
					//funcStack.push(std::make_pair(std::make_shared<OperatorNotEqual<T>>(), 2));
                }
            }
            else if(*tkn.begin() == ';')
            {
                last_type_id = int(tokens.push_token(Semicolon<T>())->type());
                //funcStack.push(std::make_pair(std::make_shared<Semicolon<T>>(), 0));
            }
            else
				throw std::logic_error("Unexpected token");
			//cbRest -= tkn.end() - begPtr;

            std::string tkn_string(tkn.begin(), tkn.size());
            auto tkn_endpos = expression.find(tkn_string);
            expression = expression.substr(tkn_endpos + tkn.size());
            //cbRest -= tkn.size(); 
			//begPtr = (char*)(begPtr + tkn.size());
		}

		//body = std::move(tokens).finalize();
		formula = std::move(tokens).finalize();
		return formula;
	}
};

template <class K>
typename std::list<std::shared_ptr<IToken<K>>>::iterator simplify(std::list<std::shared_ptr<IToken<K>>>& body, typename std::list<std::shared_ptr<IToken<K>>>::iterator elem)
{
	try
	{
		bool isComputable = false;
		auto paramsCount = elem->get()->get_required_parameter_count();
		auto param_it = elem;
		for (auto i = paramsCount; i > 0; --i)
		{
			--param_it;
			((*elem).get())->push_argument(*param_it); //here std::move must be
			param_it = body.erase(param_it);
		}
		if (elem->get()->is_ready())
			*elem = elem->get()->simplify();
		return ++elem;
	}
	catch (std::exception e)
	{
		throw std::invalid_argument("ERROR!");
	}
}

template <class T>
auto simplify_body(std::list<std::shared_ptr<IToken<T>>>&& body)
{
	auto it = body.begin();
	while (body.size() > 1)
		it = simplify(body, it);
	//auto val = it;
	return body.front();
	//When everything goes right, you are left with only one element within the list - the root of the tree.
}

template <class T>
T compute(const std::list<std::shared_ptr<IToken<T>>>& body)
{
	assert(body.size() == 1);
	return body.front()();
}

template <class T>
Mathexpr<T>::Mathexpr(const char* sMathExpr, std::size_t cbMathExpr)
{
	//const char* endptr;
	//header = Header<T>(sMathExpr, cbMathExpr, (char**)&endptr);
	//++endptr;
	//lexBody<T>(endptr, cbMathExpr - (endptr - sMathExpr));
	//auto formula = std::move(lexBody(endptr, cbMathExpr - (endptr - sMathExpr)));
	auto formula = std::move(lexBody(sMathExpr, cbMathExpr));
	//this->body = simplify_body(std::move(formula));
    this->body = std::move(formula.front());
}

#endif // !PARSER_H
