#include "ParameterStorage.hpp"
#include "Lexic.hpp"
#include "OperatorProperty.hpp"
#include <functional>
#include <iostream>
#include <type_traits>
#include <cmath>
#include <string>

int main() 
{
    dynamic_parameter_storage<int, int, double> d;
    d[std::placeholders::_1] = 1;
    d[std::placeholders::_2] = 12;
    d[std::placeholders::_3] = 5.0;
    ArityWrapper<int, int> arity(d.get_params(), [](int a) -> int
            {return std::sin(a)+1;});
    std::cout << arity.compute() << "\n";

    Function f("sin", arity, 1);
    const std::string str = "x <- y + sin(z) / log(10, 55)";

    lex(str);
    for (auto i : lexems)
    {
        std::cout << i.name << "\n";
    }
    //ArityWrapper<int, int, int> arity(d, [](int a, int b) -> int {return a + b;});
    return 0;
}
