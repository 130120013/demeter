#include <cstring>

#include <curlpp/Exception.hpp>
#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <exception>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>

int main() 
{
    try 
    {
        //cURLpp::Easy easyhandle;
        //easyhandle.setOpt(cURLpp::Options::Url("http://example.com/"));
        //easyhandle.perform();

        curlpp::Cleanup cleanup;
        curlpp::Easy request;

        request.setOpt(curlpp::options::Url(
            std::string("http://localhost:3030/AcousticsMethods/sparql")));
        //  request.setOpt(curlpp::options::Verbose(true));

        std::list<std::string> header = 
         {
            "Accept: application/sparql-results+xml",
            "Access-Control-Allow-Origin: http://localhost:3030",
            "Access-Control-Allow-Credentials: true",
            "Access-Control-Expose-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Credentials",
            "Content-Type: application/x-www-form-urlencoded"
         }; 
        request.setOpt(new curlpp::options::HttpHeader(header)); 

        std::string query = "query=PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\
            PREFIX owl: <http://www.w3.org/2002/07/owl#>\
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\
            PREFIX ac: <http://www.semanticweb.org/alinachusova/ontologies/2022/1/AcousticsMethods#>\
            SELECT distinct ?lang_term ?term_sign WHERE {\
            ?lang_term owl:equivalentClass ?restriction.\
            ?restriction owl:onProperty ac:Term_name .\
            ?restriction owl:hasValue ?term_sign.\
            }";
        request.setOpt(new curlpp::options::PostFields(query)); 
        request.setOpt(new curlpp::options::WriteStream(&std::cout));

        request.perform();
    }
    catch(cURLpp::RuntimeError& e)
    {
        std::cout << "Exception: " << e.what() << "\n";
    }
    catch(cURLpp::LogicError& e)
    {
        std::cout << "Exception: " << e.what() << "\n";
    }
    return 0;
}
